<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<%@include file="./elements/head.jsp"%>
<body>
	<div class="page form-page">
		<!-- Main Navbar-->
		<%@include file="./elements/header.jsp"%>

		<div class="page-content d-flex align-items-stretch">

			<!-- Side Navbar -->
			<%@include file="./elements/sidebar.jsp"%>

			<div class="content-inner">
				<!-- Page Header-->
				<header class="page-header">
					<div class="container-fluid">
						<h2 class="no-margin-bottom">Create User</h2>
					</div>
				</header>

				<!-- Forms Section-->
				<section class="forms">
					<div class="container-fluid">
						<div class="row">


							<!-- Form Elements -->
							<div class="col-lg-12">
								<div class="card">
									<div class="card-close"></div>
									<div class="card-header d-flex align-items-center">
										<h3 class="h4">User Details</h3>
									</div>
									<div class="card-body">
										<form class="form-horizontal" id="testform" method="post">

											<div class="row">
	
												<div class="col-sm-9">
												<div>&nbsp</div>

													<div class="form-group-material">
														<input id="employeeName" required class="input-material col-sm-4 typeahead"  autocomplete="off" type="text" maxlength="10">
														<label for="employeeName" class="label-material" >Employee Name</label>
													</div>

													
													<input type="hidden" name="createdUser" id="createdUser" value="${fullName}">
													
																											
													<div class="form-group-material">
														<input id="userId" type="text" name="userId" required class="input-material col-sm-4" maxlength="10"> <label for="userId"
															class="label-material">User Id</label>
													</div>
													
													<div class="form-group-material">
														<input id="contactNo" type="text" name="contactNo" required class="input-material col-sm-4" maxlength="10"> <label for="contactNo"
															class="label-material">Contact No</label>
													</div>
													
													<div class="form-group-material">
														<input id="email" type="text" name="email" required class="input-material col-sm-4" maxlength="10"> <label for="email"
															class="label-material">Email</label>
													</div>
													
													
													
													
												
												</div>
											
											<div>&nbsp</div>
											<div>&nbsp</div>
											<div>&nbsp</div>
											<div>&nbsp</div>
											<div>&nbsp</div>
											
											</div>
											
                                           <input id="submit_handle" type="submit" style="display: none">

											<div class="line"></div>
											<div class="form-group row">
												<div class="col-sm-9">
												
													<button type="submit" id="createUser" class="btn btn-primary form-group custom-size">Create</button>

													<button type="reset" class="btn btn-secondary form-group custom-size">Cancel</button>

												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<%@include file="./elements/alerts.jsp"%>
	<!-- Page Footer-->
	<footer class="main-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<p>XCoders &copy; 2019-2020</p>
				</div>
				<div class="col-sm-6 text-right">
					<p>
						Design by <a href="https://www.dialog.lk/" class="external">XCoders</a>
					</p>
					<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
				</div>
			</div>
		</div>
	</footer>
			</div>
		</div>
		
		
	</div>



	<!-- Javascript files-->	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>	
	<!-- <script src="//code.jquery.com/jquery-1.12.4.js"></script> -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="js/jquery.plugin.min.js"></script>
    <script src="js/jquery.datepick.js"></script>
	<script src="js/tether.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"> </script>
	<script src="js/jquery.validate.min.js"></script>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script> 
	<script src="js/charts-home.js"></script>-->
	<script src="js/front.js"></script>
    <script src="js/bootstrap3-typeahead.js"></script> 
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Cookies.js/0.3.1/cookies.js"></script>
    <script src="js/ApplicationJs/navibar.js"></script>   
	<script src="js/ApplicationJs/CreateUser.js"></script>

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
	<!---->
	<script>
	

	 
	 
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
    
    <script>
		$(function() {
			
			
			
			
			$('#inlineDatepicker').datepick({
				
			});
		});

		
		
		
		</script>


</body>

</html>