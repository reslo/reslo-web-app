<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<%@include file="./elements/head.jsp"%>
<body>
	<div class="page form-page">
		<!-- Main Navbar-->
		<%@include file="./elements/header.jsp"%>

		<div class="page-content d-flex align-items-stretch">

			<!-- Side Navbar -->
			<%@include file="./elements/sidebar.jsp"%>

			<div class="content-inner">
				<!-- Page Header-->
				<header class="page-header">
					<div class="container-fluid">
						<h2 class="no-margin-bottom">Search All Employee Report</h2>
					</div>
				</header>

				<!-- Forms Section-->
				<section class="forms">
					<div class="container-fluid">
						<div class="row">


							<!-- Form Elements -->
							<div class="col-lg-12">
								<div class="card">
									<div class="card-close"></div>
									<div class="card-header d-flex align-items-center">
										<h3 class="h4">Employee Details</h3>
									</div>
									<div class="card-body">
										<form class="form-horizontal" method="post" action="./Project">

											<div class="row">

												<div class="col-sm-6">
													<div>&nbsp</div>
													
													<input type="hidden" name="createdUser" id="createdUser" value="${fullName}">
                                                   
													
													<div>&nbsp</div>
													<label class="col-xs-3 control-label">From  :</label>
													<div class="form-group row">
														<div class="input-group input-append date col-sm-5 select"
															id="dateRangePicker">
															<input type="text" class="form-control" id="startDate" name="startDate" style="z-index: 0;"/> 
															<span class="input-group-addon add-on">
															  <i class="fa fa-calendar"></i>
															 
															</span>
														</div>
														<input id="toggle-two" type="checkbox" data-toggle="toggle" data-on="Disabled" data-off="Enabled">
														
													</div>
													
													
													<div>&nbsp</div>
													<label for="account" class="label-material">Employee Role</label>
													
													<div id="userRole" class="form-group row">
														
														<div class="col-sm-5 select">
															<select id="userRoleChange" name="account" class="form-control"
																style="height: 40px;">

																<option>All Employee</option>
																<option>Dev</option>
																<option>QA</option>
																<option>BA</option>
																<option>Management</option>
															</select>
														</div>

													</div>
													
													
													
													
													<div>&nbsp</div>
													<div>&nbsp</div>
													<div>&nbsp</div>




												</div>
											</div>	


											<div class="form-group row">
												<div class="col-sm-4 offset-sm-0">
													<button type="button" id= "previewReport" class="btn btn-primary custom-size">Preview</button>

													<button type="reset" id= "cancelReport"class="btn btn-secondary custom-size">Cancel</button>

												</div>
											</div>
											
											<div class="line"></div>

									<table id="employeeList" class="table table-striped table-bordered" style="display:none;" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								            	<th>Employee Name</th>
								                <th>Project Name<br>& CR Name</th>
								                <th>CR No</th>
								                <th>Status</th>
								                <th>Project Start<br>Date</th>
								                <th>Project End<br>Date</th>
								                <th>Project Actual<br>Start Date</th>
								                <th>Project Actual<br>End Date</th>
								                <th>Project<br>Progress %</th>
								                <th>Allocation</th>
								            </tr>
								        </thead>
								        <tfoot>
								            <tr>
								                <th>Employee Name</th>
								                <th>Project Name<br>& CR Name</th>
								                <th>CR No</th>
								                <th>Status</th>
								                <th>Project Start<br>Date</th>
								                <th>Project End<br>Date</th>
								                <th>Project Actual<br>Start Date</th>
								                <th>Project Actual<br>End Date</th>
								                <th>Project<br>Progress %</th>
								                <th>Allocation</th>
								            </tr>
								        </tfoot>
								        <tbody>
								           
								        </tbody>
								    </table>

											<div class="form-group row">
												<div class="col-sm-4 offset-sm-0">
													<button type="button" id="downloadReport"
														class="btn btn-primary custom-size" style="display:none;">Download</button>


												</div>
											</div>


										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<%@include file="./elements/alerts.jsp"%>
	<!-- Page Footer-->
	<footer class="main-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<p>XCoders &copy; 2019-2020</p>
				</div>
				<div class="col-sm-6 text-right">
					<p>
						Design by <a href="https://www.dialog.lk/" class="external">XCoders</a>
					</p>
					<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
				</div>
			</div>
		</div>
	</footer>
			</div>
		</div>
	</div>



	<!-- Javascript files-->

	<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<!-- 	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->

    <script src="js/jquery.plugin.min.js"></script>
	<script src="js/jquery.datepick.js"></script>
	<script src="js/moment.min.js"></script>
	<script src="js/tether.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	
	<script src="js/front.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Cookies.js/0.3.1/cookies.js"></script>	
 	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
 	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
	<!-- <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>  -->
    <script src="js/ApplicationJs/navibar.js"></script>	
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	<script src="js/ApplicationJs/AllEmpReport.js"></script>


	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
	<!---->
	<script>
		

		(function(b, o, i, l, e, r) {
			b.GoogleAnalyticsObject = l;
			b[l] || (b[l] = function() {
				(b[l].q = b[l].q || []).push(arguments)
			});
			b[l].l = +new Date;
			e = o.createElement(i);
			r = o.getElementsByTagName(i)[0];
			e.src = '//www.google-analytics.com/analytics.js';
			r.parentNode.insertBefore(e, r)
		}(window, document, 'script', 'ga'));
		ga('create', 'UA-XXXXX-X');
		ga('send', 'pageview');
		
		$('#dateRangePicker').datepicker({
			format : 'yyyy/mm/dd',
			startDate : '2010/01/01',
			autoclose: true,
			daysOfWeekDisabled : [ 0, 6 ],
			endDate : '2020/12/30'
		}).on('changeDate', function(e) {
			var startDate = new Date(e.date.valueOf());
			startDate.setDate(startDate.getDate(new Date(e.date.valueOf())));
			$('#dateRangePicker1').datepicker('setStartDate',startDate)
			// Revalidate the date field
			// $('#dateRangeForm').formValidation('revalidateField', 'date');
		});

		$('#dateRangePicker1').datepicker({
			format : 'yyyy/mm/dd',
			startDate : '2010/01/01',
			autoclose: true,
			daysOfWeekDisabled : [ 0, 6 ],
			endDate : '2020/12/30'
		}).on('changeDate', function(e) {
			// Revalidate the date field
			// $('#dateRangeForm').formValidation('revalidateField', 'date');
		});
	</script>
	<script>
		$(function() {
		 $('#inlineDatepicker').datepick({
				
			}); 
		});

		
	</script>

</body>

</html>