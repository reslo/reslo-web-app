<style>
.modal.custom .modal-dialog {
    top: 101px;
    left: 53px;
} 
</style>


<div class="modal custom fade" id="newProjects" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">New Projects</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div id='new-modal-body' class="modal-body">
				<table id="newProjectList"
					class="table table-striped table-bordered" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>Project Name & CR Name</th>
							<th>Cr No</th>

						</tr>
					</thead>
					<tbody id='newProjectBody'>
					
					</tbody>

				</table>
			
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div class="modal custom fade" id="onGoingProjects" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">On Going Projects</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div id='onGoing-modal-body' class="modal-body">
				<table id="onGoingProjectList"
					class="table table-striped table-bordered" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>Project Name & CR Name</th>
							<th>Cr No</th>

						</tr>
					</thead>
					<tbody id='onGoingProjectBody'>
					
					</tbody>

				</table>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div class="modal custom fade" id="closeProjects" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Close Projects</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div id='close-modal-body' class="modal-body">
				<table id="closeProjectList"
					class="table table-striped table-bordered" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>Project Name & CR Name</th>
							<th>Cr No</th>

						</tr>
					</thead>
					<tbody id='closeProjectBody'>
					
					</tbody>

				</table>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div class="modal custom fade" id="holdProjects" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Hold Projects</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div id='hold-modal-body' class="modal-body">
				<table id="holdProjectList"
					class="table table-striped table-bordered" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>Project Name & CR Name</th>
							<th>Cr No</th>

						</tr>
					</thead>
					<tbody id='holdProjectBody'>
					
					</tbody>

				</table>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div class="modal custom fade" id="idleEmp" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Idle Employees</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div id='idleEmp-modal-body' class="modal-body">
				<table id="idleEmpList"
					class="table table-striped table-bordered" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>Employee Name</th>
							<th>User Role</th>

						</tr>
					</thead>
					<tbody id='idleEmpListBody'>
					
					</tbody>

				</table>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div class="modal custom fade" id="overAllocatedEmp" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Over Allocated Employees</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div id='overAllocatedEmp-modal-body' class="modal-body">
				<table id="overAllocatedEmpList"
					class="table table-striped table-bordered" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>Employee Name</th>
							<th>User Role</th>
							<th>Total Allocation</th>

						</tr>
					</thead>
					<tbody id='overAllocatedEmpListBody'>
					
					</tbody>

				</table>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>


<div class="modal custom fade" id="underAllocatedEmp" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Under Allocated Employees</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div id='underAllocatedEmp-modal-body' class="modal-body">
				<table id="underAllocatedEmpList"
					class="table table-striped table-bordered" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>Employee Name</th>
							<th>User Role</th>
							<th>Total Allocation</th>

						</tr>
					</thead>
					<tbody id='underAllocatedEmpListBody'>
					
					</tbody>

				</table>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div class="modal custom fade" id="BookingEmp" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Booking Employees</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div id='BookingEmp-modal-body' class="modal-body">
				<table id="BookingEmpList"
					class="table table-striped table-bordered" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>Employee Name</th>
							<th>User Role</th>
							<th>Booking</th>

						</tr>
					</thead>
					<tbody id='BookingEmpListBody'>
					
					</tbody>

				</table>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

