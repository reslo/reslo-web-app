<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Add Resource</h2>
		</div>
	</header>

<!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                           
               
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4"></h3>
                    </div>
                    <div class="card-body">
                      <form class="form-horizontal">
                        
                        <div class="row">
                          <label class="col-sm-3 form-control-label">Resource Details</label>
                          <div class="col-sm-9">
                            <div class="form-group-material">
                              <input id="EmployeeNo" type="text" name="EmployeeNo" required class="input-material">
                              <label for="EmployeeNo" class="label-material">Employee No</label>
                            </div>
                            
                            <div class="form-group-material">
                              <input id="employeeName" type="text" name="employeeName" required class="input-material">
                              <label for="employeeName" class="label-material">Employee Name</label>
                            </div>
                            
                            <div class="form-group-material">
                              <input id="ContactNo" type="text" name="ContactNo" required class="input-material" maxlength="10" pattern="[0-9]{10}">
                              <label for="ContactNo" class="label-material">Contact No</label>
                            </div>
                            
                            <div class="form-group-material">
                              <input id="register-email" type="email" name="registerEmail" required class="input-material">
                              <label for="register-email" class="label-material">Email Address      </label>
                            </div>
                            <div class="form-group-material">
                              <input id="register-passowrd" type="password" name="registerPassword" required class="input-material">
                              <label for="register-passowrd" class="label-material">password        </label>
                            </div>
                          </div>
                        </div>
                        

                        <div class="line"></div>
                        <div class="form-group row">
                          <div class="col-sm-4 offset-sm-3">
                          	<button type="submit" class="btn btn-primary">Save</button>
                          	
                            <button type="submit" class="btn btn-secondary">Cancel</button>
                            
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

	

	<!-- Page Footer-->
	<footer class="main-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<p>Dialog Software &copy; 2017-2019</p>
				</div>
				<div class="col-sm-6 text-right">
					<p>
						Design by <a href="https://www.dialog.lk/"
							class="external">Dialog.lk</a>
					</p>
					<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
				</div>
			</div>
		</div>
	</footer>
</div>