<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<!-- Side Navbar -->
	<nav class="side-navbar">
		<!-- Sidebar Header-->
		<div class="sidebar-header d-flex align-items-center">
			<div class="avatar">
						<c:choose>
							<c:when test="${image=='true'}">
                          	<img width="63px" height="auto" class="nav-user-photo" style="border-radius: 50%" src="data:image/jpg;base64,<c:out value='${userImage}'/>"/>
                            </c:when>
							<c:otherwise>
                            <img width="63px" height="auto" class="nav-user-photo" style="border-radius: 50%" src="img/defaultUser.png" alt="Default Image" />
							</c:otherwise>
						</c:choose> 
			</div>
			<div class="title">
				<h1 class="h4">${fullName}</h1>
				<p style="font-size: 12px;">${title}</p>
			</div>
		</div>
		<!-- Sidebar Navidation Menus-->
		<span class="heading">Main</span>
		<ul class="list-unstyled" id="list-unstyled">
			<!-- <li class="menu-item"><a href="./home"><i class="icon-home"></i>Home</a></li> -->
			<li class="menu-item"><a href="./newHome"><i class="icon-home"></i>Home</a></li>
			<li class="menu-item"><a href="./CreateUser"><i class="fa fa-user-plus"></i>Create User</a></li>
			<li class="menu-item has-sub"><a href="#ManageResource" aria-expanded="false"
				data-toggle="collapse"> <i class="fa fa-user-o"></i>Manage
					Resource </a>
				<ul id="ManageResource" class="collapse list-unstyled">
					<li><a href="./AddResource">Add Resource</a></li>
					<li><a href="./RemoveResource">Update Resource</a></li>

				</ul>
			</li>
			<li class="has-sub"><a href="#ManageProject" aria-expanded="false" data-toggle="collapse">
					<i class="fa fa-window-maximize"></i>Manage
					Project
			</a>
				<ul id="ManageProject" class="collapse list-unstyled">
					<li><a href="./AddProject">Add Project</a></li>
					<li><a href="./UpdateProject">Update Project</a></li>

				</ul></li>
			<li><a href="./ResourceAllocation"> <i class="fa fa-id-badge"></i>Resource
					Booking
			</a></li>
			<!-- <li><a href="./ResourceBooking"> <i class="fa fa-address-book-o"></i>Resource
					Booking
			</a></li> -->
			<li><a href="#charts" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-bar-chart"></i>Summary
					Reports
			</a>
			<ul id="charts" class="collapse list-unstyled">
					<li class="menu-item"><a href="#EmployeeSummary" aria-expanded="false"
						data-toggle="collapse"> <i class="fa fa-user-o"></i>Employee Summary</a>

						<ul id="EmployeeSummary" class="collapse list-unstyled">
							<li><a href="./AllEmpReport">Seach All Employees</a></li>
							<li><a href="./IdleEmpReport">Idle Employees</a></li>
							<li><a href="./AllocatedEmpReport">Allocated Employees</a></li>
							<li><a href="./EmpWiseReport">Employee Wise</a></li>
							<li><a href="./WorloadSummaryReport">Resource Workload</a></li>
						</ul></li>


					<li><a href="#ProjectSummary" aria-expanded="false"
						data-toggle="collapse"> <i class="icon-interface-windows"></i>Project Summary</a>
						<ul id="ProjectSummary" class="collapse list-unstyled">
							<li><a href="./AllProjectReport">All Projects</a></li>
							<li><a href="./ProjectWiseReport">Project Wise</a></li>
							
						</ul></li>
					<li><a href="#BookingSummary" aria-expanded="false"
						data-toggle="collapse"> <i class="icon-padnote"></i>Booking Summary</a>
						<ul id="BookingSummary" class="collapse list-unstyled">
							<li><a href="./BookingEmpSummaryReport">Employee Wise</a></li>
							<li><a href="./BookingProjectSummaryReport">Project Wise</a></li>
							
						</ul></li>	
				</ul></li>
			
		</ul>
		<span class="heading">Extras</span>
		<ul class="list-unstyled">
			<!-- <li><a href="#QAPortal"> <i class="fa fa-street-view"></i>QA Portal
			</a></li> -->
			<!-- <li><a href="./DialogHolidays"> <i class="fa fa-calendar-times-o"></i>Dialog Holidays
			</a></li>
			<li><a href="./GanttChart"> <i class="fa fa-calendar-times-o"></i>Gantt Chart
			</a></li>
			<li><a href="./AddResource2"> <i class="fa fa-street-view"></i>Test
			</a></li>
			<li><a href="./AllocationChart"> <i class="fa fa-calendar-times-o"></i>Allocation Chart
			</a></li> -->
		
		</ul>
		 
		 <div>&nbsp</div>
		 <div>&nbsp</div>
		 <div>&nbsp</div>
		 
		<div id="inlineDatepicker" style="padding: 10px;"></div>	
		
	</nav>
	