<style>
.modal.custom .modal-dialog {
    top: 50px;
} 
</style>

<div class="modal custom fade" id="modelError" role="dialog">
	<div class="modal-dialog">
		<div class="alert alert-danger alert-dismissable">
			<a href="#" class="close" data-dismiss="modal" aria-label="close">�</a>
			<strong>Error!</strong> <br>
			<br>
			<p><div id="errorMsg"></div></p>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal custom fade" id="modelSuccess" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="alert alert-success alert-dismissable">
			<a href="#" class="close" data-dismiss="modal" aria-label="close">�</a>
			<strong>Success!</strong> <br>
			<br>
			<p><div id="successMsg"></div></p>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal custom fade" id="modelWarning" role="dialog">
	<div class="modal-dialog">
		<div class="alert alert-warning alert-dismissable">
			<a href="#" class="close" data-dismiss="modal" aria-label="close">�</a>
			<strong>Warning!</strong> <br>
			<br>
			<p><div id="warningMsg"></div></p>
		</div>
	</div>
</div>

  <div class="modal custom fade" id="deleteConfirmBox" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmBox" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                      <h4 class="modal-title" id="deleteConfirmBox">Delete Confirmation</h4>
                  <a href="#" class="close" data-dismiss="modal" aria-label="close">�</a>
              
                </div>
            
                <div class="modal-body">
                    <p id="">Are you sure you want to Delete ?</p>
                    <p class="debug-url"></p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger btn-ok" data-dismiss="modal" id="deleteConfirm">Proceed</button>
                </div>
            </div>
        </div>
    </div>

  <div class="modal custom fade" id="updateConfirmBox" tabindex="-1" role="dialog" aria-labelledby="updateConfirmBox" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                      <h4 class="modal-title" id="updateConfirmBox">Update Confirmation</h4>
                  <a href="#" class="close" data-dismiss="modal" aria-label="close">�</a>
              
                </div>
            
                <div class="modal-body">
                    <p id="">Are you sure you want to Update ?</p>
                    <p class="debug-url"></p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger btn-ok" data-dismiss="modal" id="updateConfirm">Proceed</button>
                </div>
            </div>
        </div>
    </div>

