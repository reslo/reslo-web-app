
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

	<%@include file="./elements/head.jsp"%>
	<style>
      #gantt-chart{
      	width: 100%; 
      	height: 305px;
      }
    </style>
<body>
	<div class="page home-page">
		<!-- Main Navbar-->
		<%@include file="./elements/header.jsp"%>
		
		<div class="page-content d-flex align-items-stretch">
		
		<!-- Side Navbar -->
		<%@include file="./elements/sidebar.jsp"%>

		<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Welcome to Reslo</h2>
		</div>
	</header>
	<!-- Dashboard Counts Section-->
	<section class="dashboard-counts no-padding-bottom">
		<div class="container-fluid">
			<div class="row bg-white has-shadow">
				<!-- Item -->
				<div class="col-xl-3 col-sm-6">
					<div class="item d-flex align-items-center" data-toggle="modal" data-target="#idleEmp">
						<div class="icon bg-violet">
							<i class="icon-user"></i>
						</div>
						<div data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Idle Employees for Today"
										class="red-tooltip title">
							<span>Idle<br>Employees
							</span>
							<div class="progress">
								<div role="progressbar" style="width: 25%; height: 4px;"
									aria-valuenow="5" aria-valuemin="0" aria-valuemax="100"
									class="progress-bar bg-violet"></div>
							</div>
						</div>
						<div  id='idleCount' class="number">
							
						</div>
					</div>
					
				</div>
				<!-- Item -->
				<div class="col-xl-3 col-sm-6">
					<div class="item d-flex align-items-center" data-toggle="modal" data-target="#overAllocatedEmp">
						<div class="icon bg-red">
							<i class="icon-padnote"></i>
						</div>
							<div data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Allocations Over 100% for Today"
										class="red-tooltip title">
										<span>Over<br>Allocated
										</span>
										<div class="progress">
											<div role="progressbar" style="width: 25%; height: 4px;"
												aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
												class="progress-bar bg-red"></div>
										</div>
									</div>
									<div id='overAllocatedCout' class="number">
							
						</div>
					</div>
				</div>
				<!-- Item -->
				<div class="col-xl-3 col-sm-6">
					<div class="item d-flex align-items-center" data-toggle="modal" data-target="#underAllocatedEmp">
						<div class="icon bg-green">
							<i class="icon-bill"></i>
						</div>
						<div data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Allocation Under 100% for Today" class="red-tooltip title">
							<span>Under<br>Allocated
							</span>
							<div class="progress">
								<div role="progressbar" style="width: 25%; height: 4px;"
									aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
									class="progress-bar bg-green"></div>
							</div>
						</div>
						<div id='underAllocatedCout' class="number">
							
						</div>
					</div>
				</div>
				
				<div class="col-xl-3 col-sm-6">
					<div class="item d-flex align-items-center" data-toggle="modal" data-target="#BookingEmp">
						<div class="icon bg-orange">
							<i class="icon-check"></i>
						</div>
						<div data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Resources Booked for the Next Month"
										class="red-tooltip title">
							<span>Future<br>Allocations
							</span>
							<div class="progress">
								<div role="progressbar" style="width: 25%; height: 4px;"
									aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
									class="progress-bar bg-orange"></div>
							</div>
						</div>
						<div id='BookingEmpCount' class="number">
						
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- Dashboard Header Section    -->
	<section class="dashboard-header">
		<div class="container-fluid">
			<div class="row">
				<!-- Statistics -->
				<div class="statistics col-lg-3 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" data-toggle="modal" data-target="#newProjects">
						<div class="icon bg-blue">
							<i class="icon-check"></i>
						</div>
						<div id="new" class="text">
							
						</div>
						
				  </div>
					
					
					<div
						class="statistic d-flex align-items-center bg-white has-shadow" data-toggle="modal" data-target="#onGoingProjects">
						<div class="icon bg-orange">
							<i class="fa fa-paper-plane-o"></i>
						</div>
						<div id="inProgress" class="text">
							
						</div>
					</div>
					<div
						class="statistic d-flex align-items-center bg-white has-shadow" data-toggle="modal" data-target="#holdProjects">
						<div class="icon bg-green">
							<i class="fa fa-calendar-o"></i>
						</div>
						<div id="onHold" class="text">
							
						</div>
					</div>
					<div
						class="statistic d-flex align-items-center bg-white has-shadow" data-toggle="modal" data-target="#closeProjects">
						<div class="icon bg-red">
							<i class="fa fa-tasks"></i>
						</div>
						<div id="close" class="text">
							
						</div>
					</div>
					
					
					
				</div>
				<!-- gantt Chart  -->			
				<div class="chart col-9" style="padding-left: 0px;">
					<div class="bg-white d-flex align-items-center justify-content-center has-shadow">
						<div id="gantt-chart"></div>
					</div>
				</div>				
			</div>
		</div>
	</section>
	
	<!-- Dashboard Header Section    -->
	<section class="projects no-padding-top">
		<div class="container-fluid">
			<div class="row">
				<!-- Statistics -->
				<div class="chart col-lg-12 col-12">
				  <div class="bg-white d-flex align-items-center justify-content-center has-shadow">
									<div class="chart col-5">

										<div class="form-group row">

											<div class="col-sm-5 select" style="margin: 0 auto; margin-left: 48%; padding-left: 0px;">
												<div>&nbsp</div>
												<select id="projectName" name="projectName" class="form-control"
													style="height: 40px;" >
													
												</select>
												
												
										        <div>&nbsp</div>
												<span id="pie-chart-id" style="width:  400px; margin-left: -30px;"></span>
											</div>



										</div>
										<div id="pie-chart-container">
											<canvas id="pie-chart" width="800" height="450"></canvas>
										</div>
										<div>&nbsp</div>
										<div>&nbsp</div>
										
									</div>
									<div class="chart col-2">
										<div>&nbsp</div>
										<div>&nbsp</div>
										
										
										<div class="i-checks form-group-material">
											<input id="Dev" checked="checked" type="radio" value="Dev"
												name="group1" class="radio-template"> <label
												for="Dev">Dev</label>
										</div>
										<div class="i-checks form-group-material">
											<input id="QA" type="radio" value="QA"
												name="group1" class="radio-template"> <label
												for="QA">QA</label>
										</div>
										<div class="i-checks form-group-material">
											<input id="BA" type="radio" value="BA" type="radio"
												name="group1" class="radio-template"> <label
												for="BA">BA</label>
										</div>
										<div class="i-checks form-group-material">
											<input id="Management" type="radio"
												value="Management" name="group1" class="radio-template">
											<label for="Management">Management</label>
										</div>
									</div>

									<div class="chart col-5">
										<div id="allocationTable">
											<div class="row form-group">
												<div class="col-12">
													<table id="allocationDetailList"
														class="table table-striped table-bordered" cellspacing="0"
														width="100%">
														<thead>
															<tr>
																<th>Employee ID</th>
																<th>Employee Name</th>
																<th>Allocation</th>

															</tr>
														</thead>

													</table>

												</div>
												<div class="col"></div>
											</div>

											<div>&nbsp</div>

										</div>
									</div>
								</div>	
				</div>
			</div>
		</div>
	</section>
	
	
	 <!-- Projects Section-->
         <!--  <section class="projects no-padding-top">
            <div class="container-fluid">
              Project
              <div class="project">
                <div class="row bg-white has-shadow">
                  <div class="left-col col-lg-6 d-flex align-items-center justify-content-between">
                    <div class="project-title d-flex align-items-center">
                      <div class="image has-shadow"><img src="img/project-1.jpg" alt="..." class="img-fluid"></div>
                      <div class="text">
                        <h3 class="h4">Project Title</h3><small>Lorem Ipsum Dolor</small>
                      </div>
                    </div>
                    <div class="project-date"><span class="hidden-sm-down">Today at 4:24 AM</span></div>
                  </div>
                  <div class="right-col col-lg-6 d-flex align-items-center">
                    <div class="time"><i class="fa fa-clock-o"></i>12:00 PM </div>
                    <div class="comments"><i class="fa fa-comment-o"></i>20</div>
                    <div class="project-progress">
                      <div class="progress">
                        <div role="progressbar" style="width: 45%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            
             
            </div>
          </section> -->
          
	<%@include file="./elements/homePagePopUps.jsp"%>
	<!-- Page Footer-->
	<footer class="main-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<p>XCoders &copy; 2019-2020</p>
				</div>
				<div class="col-sm-6 text-right">
					<p>
						Design by <a href="https://www.dialog.lk/" class="external">XCoders</a>
					</p>
					<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
				</div>
			</div>
		</div>
	</footer>
</div>

		</div>
	</div>
	<!-- Javascript files-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/jquery.plugin.min.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/jquery.datepick.js"></script>
	<script src="js/tether.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"> </script>
	<script src="js/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<!-- <script src="js/charts-home.js"></script> -->
	<script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-base.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-ui.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-exports.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-gantt.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-data-adapter.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
    <script type="text/javascript" src="js/ApplicationJs/homeGanttChart.js"></script>
    <script src="js/ApplicationJs/navibar.js"></script>	
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Cookies.js/0.3.1/cookies.js"></script>
	<script src="js/front.js"></script>
	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
	<!---->
	<script>
	

	 
	 
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
    
    	<script>
		$(function() {
			$('#inlineDatepicker').datepick({
				
			});
		});

		

	</script>
	<script>
	$(document).ready(function(){
  
	    $("div").tooltip();	    
	});
	
	</script>
</body>
</html>
