<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<%@include file="./elements/head.jsp"%>
<body>
	<div class="page form-page">
		<!-- Main Navbar-->
		<%@include file="./elements/header.jsp"%>

		<div class="page-content d-flex align-items-stretch">

			<!-- Side Navbar -->
			<%@include file="./elements/sidebar.jsp"%>

			<div class="content-inner">
				<!-- Page Header-->
				<header class="page-header">
					<div class="container-fluid">
						<h2 class="no-margin-bottom">Resource Booking</h2>
					</div>
				</header>

				<!-- Forms Section-->
				<section class="forms">
					<div class="container-fluid">
						<div class="row">


							<!-- Form Elements -->
							<div class="col-lg-12">
								<div class="card">
									<div class="card-close"></div>
									<div class="card-header d-flex align-items-center">
										<h3 class="h4">Booking Details</h3>
									</div>
									<div class="card-body">
										<form class="form-horizontal" method="post" action="./manageAllocation">
                                             
											 <div>&nbsp</div>
											 
                                           <div class="row">
                                             
												<div class="col">
												    <div class="form-group-material">
														<input id="projectName" type="text" name="projectName" required class="input-material letters col-sm-6 typeahead" autocomplete="off"
															maxlength="25"> <label for="projectName"
															class="label-material">Project Name & CR Name</label>
													</div>
												</div>
												
												<input type="hidden" name="createdUser" id="createdUser" value="Amel">
												
												<div class="col">
												   <div class="form-group-material">
														<input id="crNo" type="text" name="crNo" required
															class="input-material col-sm-6 typeahead" autocomplete="off" maxlength="30"> <label
															for="crNo" class="label-material">CR No</label>
													</div>
											</div>
											</div>
												
											
											
											
											<div class="row">
											
														<div class="col-2 form-group"><label id="projectStartDateLBL" class="label-material">Plan Start Date</label></div>
														<div class="col-2 form-group"><label id="projectEndDateLBL" class="label-material">Plan End Date</label></div>
														<div class="col-2 form-group"><label id="projectActualEndDateLBL" class="label-material">Actual End Date</label></div>
											
										
											
											</div>
											<div class="row">
											
														<div class="col-2 form-group">
														     <input id="projectStartDate" class="label-material" style="border: none">
														</div>
														<div class="col-2 form-group">
														     <input id="projectEndDate" class="label-material" style="border: none">
														</div>
														<div class="col-2 form-group">
														     <input id="projectActualEndDate" class="label-material" style="border: none">
														</div>
											
										
											</div>
											<div>&nbsp</div>
											<div>&nbsp</div>
											
											<div class="row">
											
											<div class="col-4">
													<div class="form-group-material">
														<input id="employeeName" type="text" name="employeeName" required class="input-material letters col-sm-9 typeahead" autocomplete="off"
															maxlength="25"> <label for="employeeName"
															class="label-material">Employee Name</label>
													</div>
													
													<input type="hidden" name="empId" id="empId">
													
											</div>		
											<div class="col-8">	
											<button type="submit" id= "checkCurrentAllocation" class="btn btn-secondary form-group" style="margin-left: -50px">Check Current Allocation</button>
											</div>
											
										</div>
											<div>&nbsp</div>
											<div>&nbsp</div>
											
											<div id="allocationTable">
												<div class="row form-group">
													<div class="col-12">
														<table id="allocationList" class="table table-striped table-bordered" cellspacing="0"
															width="100%">
															<thead>
																<tr>
																	<th>Project Name & CR Name</th>
																	<th>CR No</th>
																	<th>Status</th>
																	<th>Start Date</th>
																	<th>End date</th>
																	<th>Actual End Date</th>
																	<th>Allocation</th>
																	<th>Actions</th>
																</tr>
															</thead>
															<tfoot>
																<tr>
																	<th>Project Name & CR Name</th>
																	<th>CR No</th>
																	<th>Status</th>
																	<th>Start Date</th>
																	<th>End date</th>
																	<th>Actual End Date</th>
																	<th>Allocation</th>
																	<th>Actions</th>
																</tr>
															</tfoot>
															

														</table>
	
													</div>
													<div class="col"></div>
												</div>
	
												<div>&nbsp</div>
												
											</div>
											

											<div class="line"></div>
											<div class="row">
												<div class="col">
													<label class="col-xs-3 control-label">Booking Start Date</label>
													<div class="form-group row">

														<div class="form-group input-group input-append date col-sm-6 select"
															id="dateRangePicker">
															<input type="text" class="form-group form-control" id="allocationStartDate"
																name="startDate" /> <span
																class="input-group-addon add-on"> <i
																class="fa fa-calendar"></i>

															</span>
														</div>
													</div>
												</div>
												<div class="col">
													<label class="col-xs-3 control-label">Booking End Date</label>
													<div class="form-group row">
														<div class="form-group input-group input-append date col-sm-6 select"
															id="dateRangePicker1" name="dateRangePicker1">
															<input type="text" class="form-group form-control" id="allocationEndDate"
																name="endDate" /> <span
																class="input-group-addon add-on"> <i
																class="fa fa-calendar"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
											<div>&nbsp</div>

											<div class="row">
												<div class="col-6">
													<div class="form-group-material">
														<input id="noOfDays" type="text" name="noOfDays" required
															class="input-material letters col-sm-6"
															autocomplete="off" maxlength="3"> <label
															for="noOfDays" class="label-material">No of days</label>
													</div>
												</div>
												<div class="col-6">
													<div class="form-group-material">
														<input id="allocation" type="text" name="allocation"
															required class="input-material letters col-sm-6"
															autocomplete="off" maxlength="3"> <label
															for="allocation" class="label-material">Booking Allocation
															%</label>
													</div>
												</div>
											</div>



											<div>&nbsp</div>
											
											
											


											<div class="line"></div>
											<div class="form-group row">
												<div class="col-sm-4 offset-sm-0">
												
													<button type="button" id= "addAllocation" class="btn btn-primary">Add</button>
													
													<button type="button" id= "updateAllocation" class="btn btn-primary">Update</button>
													
													<button type="button" id= "deleteAllocation" class="btn btn-primary">Delete</button>
                                                    
													<button type="reset" id= "cancelAllocation" class="btn btn-secondary">Cancel</button>

												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<%@include file="./elements/alerts.jsp"%>
	<!-- Page Footer-->
	<footer class="main-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<p>XCoders &copy; 2019-2020</p>
				</div>
				<div class="col-sm-6 text-right">
					<p>
						Design by <a href="https://www.dialog.lk/" class="external">XCoders</a>
					</p>
					<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
				</div>
			</div>
		</div>
	</footer>
			</div>
		</div>
	</div>



	<!-- Javascript files-->
	<!-- script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  -->	
<!-- 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
	<script src="//code.jquery.com/jquery-1.12.4.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
	<script src="js/jquery.plugin.min.js"></script>
	<script src="js/jquery.datepick.js"></script>
	<script src="js/moment.min.js"></script>
	<script src="js/tether.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<script src="js/charts-home.js"></script> -->
	<script src="js/front.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Cookies.js/0.3.1/cookies.js"></script>		
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
    <script src="js/ApplicationJs/navibar.js"></script>	
	<script src="js/ApplicationJs/ManageAllocation.js"></script>
	<script src="js/bootstrap3-typeahead.js"></script> 
	


	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
	<!---->
	<script>
 	$('#dateRangePicker').datepicker({
		format : 'dd/mm/yyyy',
		startDate : '01/01/2010',
		endDate : '30/12/2020'
	}).on('changeDate', function(e) {
		// Revalidate the date field
		// $('#dateRangeForm').formValidation('revalidateField', 'date');
	});

	$('#dateRangePicker1').datepicker({
		format : 'dd/mm/yyyy',
		startDate : '01/01/2010',
		endDate : '30/12/2020'
	}).on('changeDate', function(e) {
		// Revalidate the date field
		// $('#dateRangeForm').formValidation('revalidateField', 'date');
	}); 
 			(function(b, o, i, l, e, r) {
			b.GoogleAnalyticsObject = l;
			b[l] || (b[l] = function() {
				(b[l].q = b[l].q || []).push(arguments)
			});
			b[l].l = +new Date;
			e = o.createElement(i);
			r = o.getElementsByTagName(i)[0];
			e.src = '//www.google-analytics.com/analytics.js';
			r.parentNode.insertBefore(e, r)
		}(window, document, 'script', 'ga'));
		ga('create', 'UA-XXXXX-X');
		ga('send', 'pageview');
	</script>
	<script>
		$(function() {
			$('#inlineDatepicker').datepick({
				
				
			});
		});

	</script>

</body>

</html>