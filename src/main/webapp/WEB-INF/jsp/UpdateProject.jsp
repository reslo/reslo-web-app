<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<%@include file="./elements/head.jsp"%>
<body>
	<div class="page form-page">
		<!-- Main Navbar-->
		<%@include file="./elements/header.jsp"%>

		<div class="page-content d-flex align-items-stretch">

			<!-- Side Navbar -->
			<%@include file="./elements/sidebar.jsp"%>

			<div class="content-inner">
				<!-- Page Header-->
				<header class="page-header">
					<div class="container-fluid">
						<h2 class="no-margin-bottom">Update Project</h2>
					</div>
				</header>

				<!-- Forms Section-->
				<section class="forms">
					<div class="container-fluid">
						<div class="row">


							<!-- Form Elements -->
							<div class="col-lg-12">
								<div class="card">
									<div class="card-close"></div>
									<div class="card-header d-flex align-items-center">
										<h3 class="h4">Project Details</h3>
									</div>
									<div class="card-body">
										<form class="form-horizontal" method="post" action="./updateProject">

											<div class="row">

												<div class="col-sm-6">
													<div>&nbsp</div>
													<div>&nbsp</div>


													<div class="form-group-material">
														<input id="projectName" type="text" name="projectName" required class="input-material letters col-sm-6 typeahead" autocomplete="off"
															maxlength="25"> <label for="projectName"
															class="label-material">Project Name & CR Name</label>
													</div>
                                                  <input type="hidden" name="createdUser" id="createdUser" value="${fullName}">
                                                   
													<label class="col-xs-3 control-label">Plan Start Date</label>
													<div class="form-group row">
														<div class="input-group input-append date col-sm-5 select"
															id="dateRangePicker">
															<input type="text" class="form-control" id="startDate" name="startDate" style="z-index: 0;"/> 
															<span class="input-group-addon add-on">
															  <i class="fa fa-calendar"></i>
															 
															</span>
														</div>
													</div>
													
													<label class="col-xs-3 control-label">Actual Start Date</label>
													<div class="form-group row">
														<div class="input-group input-append date col-sm-5 select"
															id="dateRangePicker2">
															<input type="text" class="form-control" id="actualStartDate" name="actualStartDate" style="z-index: 0;"/> 
															<span class="input-group-addon add-on">
															<i class="fa fa-calendar"></i>
															</span>
														</div>
													</div>

													<label for="account" class="label-material">Status</label>
													<div class="i-checks form-group-material">
														
														<input id="newProjectRadio" type="radio" value="New Project"
															name="group1" class="radio-template"> <label
															for="assignedAllocation">New Project</label>
														<div>&nbsp</div>
															
														<input id="onGoingProjectRadio" type="radio" value="On going Project"
															name="group1" class="radio-template" style="margin-left: 15px;"> <label
															for="assignedAllocation">On going Project</label>
													</div>
													
													
													
													<div class="form-group row">

														<div id="statusChange" class="col-sm-5 select">
															<select id="status" name="status" class="form-control"
																style="height: 40px;">

																<option>Select a Status</option>
																<option value="InProgress">InProgress</option>
																<option value="OnHold">OnHold</option>
																<option value="Close">Close</option>

															</select>
														</div>

													</div>
													
													<div class="form-group-material">
														<input id="projectProgress" type="text" name="projectProgress" required
															class="input-material col-sm-6" maxlength="3"> <label
															for="projectProgress" class="label-material">Project Progress %</label>
													</div>
													
													<div class="form-group-material">
														<input id="updateReason" type="text" name="updateReason" required
															class="input-material col-sm-6" maxlength="30"> <label
															for="updateReason" class="label-material">Update Reason</label>
													</div>
													
													




												</div>
												<div class="col-sm-6">

													<div>&nbsp</div>
													<div>&nbsp</div>

 


													<div class="form-group-material">
														<input id="crNo" type="text" name="crNo" required
															class="input-material col-sm-6 typeahead" autocomplete="off" maxlength="30"> <label
															for="crNo" class="label-material">CR No</label>
													</div>

													<label class="col-xs-3 control-label">Plan End Date</label>
													<div class="form-group row">
														<div class="input-group input-append date col-sm-5 select"
															id="dateRangePicker1">
															<input type="text" class="form-control" id="endDate" name="endDate" style="z-index: 0;"/> 
															<span class="input-group-addon add-on">
															<i class="fa fa-calendar"></i>
															</span>
														</div>
													</div>
													
													<label class="col-xs-3 control-label">Actual End Date</label>
													<div class="form-group row">
														<div class="input-group input-append date col-sm-5 select"
															id="dateRangePicker3">
															<input type="text" class="form-control" id="actualEndDate" name="actualEndDate" style="z-index: 0;"/> 
															<span class="input-group-addon add-on">
															<i class="fa fa-calendar"></i>
															</span>
														</div>
													</div>
													
													

													<div>&nbsp</div>
													<div>&nbsp</div>
													
													
												</div>

									 	   
											
											
											</div>
											
											<div class="line"></div>
											<div class="form-group row">
												<div class="col-sm-4 offset-sm-0">
													<button type="button" id= "updateProject" class="btn btn-primary custom-size">Update</button>

													<button type="reset" id= "cancelupdateProject" class="btn btn-secondary custom-size">Cancel</button>

												</div>
											</div>
											
											<div class="line"></div>

												<table id="projectList" class="table table-striped table-bordered" cellspacing="0" width="100%">
											        <thead>
											            <tr>
											                <th>Project Name & CR Name</th>
											                <th>CR No</th>
											                <th>Status</th>
											                <th>Start Date</th>
											                <th>End date</th>
											                <th>Actual Start<br>Date</th>
								                            <th>Actual End<br>Date</th>
								                            <th>Project<br>Progress %</th>
											                <th>Reason</th>
											            </tr>
											        </thead>
											        <tfoot>
											            <tr>
											                <th>Project Name & CR Name</th>
											                <th>CR No</th>
											                <th>Status</th>
											                <th>Start Date</th>
											                <th>End date</th>
											                <th>Actual Start<br>Date</th>
								                            <th>Actual End<br>Date</th>
								                            <th>Project<br>Progress %</th>
											                <th>Reason</th>
											            </tr>
											        </tfoot>
											        <tbody>
											           
											        </tbody>
											    </table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<%@include file="./elements/alerts.jsp"%>
	<!-- Page Footer-->
	<footer class="main-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<p>XCoders &copy; 2019-2020</p>
				</div>
				<div class="col-sm-6 text-right">
					<p>
						Design by <a href="https://www.dialog.lk/" class="external">XCoders</a>
					</p>
					<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
				</div>
			</div>
		</div>
	</footer>
			</div>
		</div>
	</div>



	<!-- Javascript files-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>	
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
	 
	<script src="js/jquery.plugin.min.js"></script>
	<script src="js/jquery.datepick.js"></script>
	<script src="js/moment.min.js"></script>
	<script src="js/tether.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<script src="js/charts-home.js"></script> -->
	<script src="js/front.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Cookies.js/0.3.1/cookies.js"></script>		
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
    <script src="js/ApplicationJs/navibar.js"></script>	
	<script src="js/ApplicationJs/UpdateProject.js"></script>
	<script src="js/bootstrap3-typeahead.js"></script> 
	


	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
	<!---->
	<script>
		
	
	
	
	

		(function(b, o, i, l, e, r) {
			b.GoogleAnalyticsObject = l;
			b[l] || (b[l] = function() {
				(b[l].q = b[l].q || []).push(arguments)
			});
			b[l].l = +new Date;
			e = o.createElement(i);
			r = o.getElementsByTagName(i)[0];
			e.src = '//www.google-analytics.com/analytics.js';
			r.parentNode.insertBefore(e, r)
		}(window, document, 'script', 'ga'));
		ga('create', 'UA-XXXXX-X');
		ga('send', 'pageview');
	</script>
	<script>
		$(function() {
			$('#inlineDatepicker').datepick({
				
				
			});
		});

		
	</script>

</body>

</html>