<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<%@include file="./elements/head.jsp"%>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-base.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-ui.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-exports.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-gantt.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.0.1/js/anychart-data-adapter.min.js"></script>
    <link rel="stylesheet" href="https://cdn.anychart.com/releases/8.0.1/css/anychart-ui.min.css" />
    <link rel="stylesheet" href="https://cdn.anychart.com/releases/8.0.1/fonts/css/anychart-font.min.css" />
    <style>
      html, body, #container {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #gantt-chart{
      	height: 500px;
      }
    </style>
<body>
	<div class="page form-page">
		<!-- Main Navbar-->
		<%@include file="./elements/header.jsp"%>

		<div class="page-content d-flex align-items-stretch">

			<!-- Side Navbar -->
			<%@include file="./elements/sidebar.jsp"%>

			<div class="content-inner">
				<!-- Page Header-->
				<header class="page-header">
					<div class="container-fluid">
						<h2 class="no-margin-bottom">Gantt Chart Demo</h2>
					</div>
				</header>

				<!-- Forms Section-->
				<section class="forms">
					<div class="container-fluid">
						<div class="row">


							<!-- Form Elements -->
							<div class="col-lg-12">
								<div class="card">
									
										<label class="col-xs-3 control-label">Start Date</label>
										<div class="form-group row">
											<div class="input-group input-append date col-sm-5 select"
												id="dateRangePicker">
												<input type="text" class="form-control" id="startDate"
													name="startDate" /> <span class="input-group-addon add-on">
													<i class="fa fa-calendar"></i>

												</span>
											</div>
										
									</div>
									
										<label class="col-xs-3 control-label">End Date</label>
										<div class="form-group row">
											<div class="input-group input-append date col-sm-5 select"
												id="dateRangePicker1">
												<input type="text" class="form-control" id="endDate"
													name="endDate" /> <span class="input-group-addon add-on">
													<i class="fa fa-calendar"></i>

												</span>
											</div>
										
									</div>
									<div class="form-group row">
												<div class="col-sm-4 offset-sm-0">
													<button type="button" id= "getAllocationGanttChart" class="btn btn-primary">Save</button>

													<button type="reset" id= "cancelGanttChart"class="btn btn-secondary">Cancel</button>

												</div>
											</div>

									<div id="gantt-chart"></div>
									<!-- <div id="gantt-chart" style="width: 500px; height: 400px;"></div> -->
								</div>
							</div>
						</div>
					</div>
				</section>
				<%@include file="./elements/alerts.jsp"%>
	<!-- Page Footer-->
	<footer class="main-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<p>XCoders &copy; 2019-2020</p>
				</div>
				<div class="col-sm-6 text-right">
					<p>
						Design by <a href="https://www.dialog.lk/" class="external">XCoders</a>
					</p>
					<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
				</div>
			</div>
		</div>
	</footer>
			</div>
		</div>
	</div>



	<!-- Javascript files-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>	
	<!--  script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <script src="js/jquery.plugin.min.js"></script>
    <script src="js/jquery.datepick.js"></script>
	<script src="js/tether.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"> </script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
	<!--   script
		src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<script src="js/charts-home.js"></script> -->
	<script src="js/front.js"></script>
	<script src="js/bootstrap3-typeahead.js"></script> 
	<script type="text/javascript" src="js/ApplicationJs/AllocationChart.js"></script>
	<!-- <script src="js/gantt.js"></script> -->


	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
	<!---->
	<script>
	
	$('#dateRangePicker').datepicker({
		format : 'yyyy/mm/dd',
		startDate : '2010/01/01',
		autoclose: true,
		endDate : '2020/12/30'
	}).on('changeDate', function(e) {
		// Revalidate the date field
		// $('#dateRangeForm').formValidation('revalidateField', 'date');
	});

	$('#dateRangePicker1').datepicker({
		format : 'yyyy/mm/dd',
		startDate : '2010/01/01',
		autoclose: true,
		endDate : '2020/12/30'
	}).on('changeDate', function(e) {
		// Revalidate the date field
		// $('#dateRangeForm').formValidation('revalidateField', 'date');
	});
	 
		 
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
    
    <script>
		$(function() {
			$('#inlineDatepicker').datepick({
				
			});
		});
		
		function call_event_handler(value) {
			   //console.log(value);
			   
			   //clear the place holder values
			   $("#employeeNo").siblings('.label-material').addClass('active');
			   $("#contactNo").siblings('.label-material').addClass('active');
			   $("#empRole").siblings('.label-material').addClass('active');
			   
			   //set to text field input
			   $("#employeeNo").val(value.empId);
			   $("#contactNo").val(value.contactNo);
			   $("#empRole").val(value.userRole);
			   $("#employeeName").val($("#employeeName").val());
			  /*  $("#employeeNo").prop('disabled', true); */
			   $("#employeeNo").focus(function(){
				    this.blur();
				});
			   
			   
			   $("#employeeName").keyup(function() {

				    if (!this.value) {
				    	$("#employeeNo").val("");
				    	$("#employeeNo").siblings('.label-material').removeClass('active');
						$("#contactNo").val("");
						$("#contactNo").siblings('.label-material').removeClass('active');
						$("#empRole").val("");
						$("#empRole").siblings('.label-material').removeClass('active');
				    }

				});
			   
			}

		
	</script>



</body>

</html>