anychart.onDocumentReady(function(){
	
	    var rawData = [
	      {
	        "name": "Activities",
	        "actualStart": Date.UTC(2007, 0, 25),
	        "actualEnd": Date.UTC(2007, 2, 14),
	        "children": [
	          {
	            "name": "Draft plan",
	            "actualStart": Date.UTC(2007, 0, 25),
	            "actualEnd": Date.UTC(2007, 1, 3)
	          },
	          {
	            "name": "Board meeting",
	            "actualStart": Date.UTC(2007, 1, 4),
	            "actualEnd": Date.UTC(2007, 1, 4)
	          },
	          {
	            "name": "Research option",
	            "actualStart": Date.UTC(2007, 1, 4),
	            "actualEnd": Date.UTC(2007, 1, 24)
	          },
	          {
	            "name": "Final plan",
	            "actualStart": Date.UTC(2007, 1, 24),
	            "actualEnd": Date.UTC(2007, 2, 14)
	          }
	        ]
	      }];
	
	  // tree data settings
	  var treeData = anychart.data.tree(rawData, "as-tree");
	
	  // chart type
	  chart = anychart.ganttProject();
		
	  // set chart data
	  chart.data(treeData);

    // chart container
    chart.container('gantt-chart');

	  // initiate drawing
	  chart.draw();
	  
	  // show all items 
	  chart.fitAll();
	
	});