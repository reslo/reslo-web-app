var startDate = null;
var endDate = null;

//loadGanttChart(data);

anychart.onDocumentReady(function() {
    // The data used in this sample can be obtained from the CDN
    // https://cdn.anychart.com/samples/gantt-charts/human-resource-chart/data.js
    anychart.data.loadJsonFile('test.json', function(data) {
      // create data tree
      var treeData = anychart.data.tree(data, 'as-table');

      // create resource gantt chart
      var chart = anychart.ganttResource();

      // set data for the chart
      chart.data(treeData);

      chart.rowSelectedFill('#D4DFE8')
        .rowHoverFill('#EAEFF3')
        // set start splitter position settings
        .splitterPosition(150);


      // get chart data grid link to set column settings
      var dataGrid = chart.dataGrid();

      // set first column settings
      dataGrid.column(0)
        .title('#')
        .width(30)
        .cellTextSettings({
          hAlign: 'center'
        });

      // set second column settings
      dataGrid.column(1)
        .title('Employee Name')
        .width(120);

      // set container id for the chart
      chart.container('gantt-chart');

      // initiate chart drawing
      chart.draw();

      // zoom chart to specified date
      chart.zoomTo(1171036800000, 1176908400000);
    });
  });

$('#getAllocationGanttChart').click(
		function(e) {
			
			var startDate = new Date($("#startDate").val());
			var endDate = new Date($("#endDate").val());
			
			loadProjects(startDate,endDate);
			
			return false;
		});

//add bold and italic text settings to all parent items
function labelTextSettingsFormatter(label, dataItem) {
  if (dataItem.numChildren()) {
    label.fontWeight('bold').fontStyle('italic');
  }
}



/*Employee Column
function progressValueColumnTextFormatter(item) {
	  var field = item.get('progressValue');
	  return field;
	}

Project Name Column
function nameColumnTextFormatter(item) {
	  var field = item.get('name');
	  return field;
	}

Allocation Column
function rowHeightColumnTextFormatter(item) {
	  var field = item.get('allocation');
	  return field;
	}*/

/*
Employee Column
function progressValueColumnTextFormatter(item) {
	  var field = item.get('progressValue');
	  return field;
	}

Project Name Column
function nameColumnTextFormatter(item) {
	  var field = item.get('name');
	  return field;
	}

Allocation Column
function rowHeightColumnTextFormatter(item) {
	  var field = item.get('rowHeight');
	  return field;
	}

*/

//do pretty formatting for passed date unit
function formatDate(dateUnit) {
  if (dateUnit < 10) dateUnit = '0' + dateUnit;
  return dateUnit + '';
}


function  loadProjects(startDate,endDate) {           
	ajaxGetCall({
		startDate : startDate,
		endDate : endDate
	}, "/Reslo/getAllocationGanttChartData", function(data){
			loadGanttChart(); 
		//alert(data);
		//console.log(data);
    	});
}

function ajaxGetCall(data, url,callback){
	
	$.ajax({
		  type: "GET",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}
