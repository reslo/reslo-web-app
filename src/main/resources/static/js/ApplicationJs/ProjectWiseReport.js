var dataSet = [ [ "", "", "", ""] ];
var startDate = null;
var result = [];
var projectName = null;
var crNo = null;
var userRole = null;

$( document ).ready(function() {
	
	getAllProject();
	
	$("#allocationTabel").hide();
	$("#printReport").hide();
	$("#chartReport").hide();
	$("#depCount").hide();
	$("#pie-chart-div").hide();
	
	
	$('#projectName').change(function(e) {
		projectName =  $("#projectName").val();
		
		$('#crNo').val("");
		$("#allocationTabel").hide();
		$("#printReport").hide();
		$("#chartReport").hide();
		$("#depCount").hide();
		$("#pie-chart-div").hide();
		$("#userRoleChange").val("All Employee");
		
    });
	
	$('#previewReport').click(
			function(e) {
			
			if ($("#crNo").val() != "" && $("#crNo").val() != null)
             {
				
			  crNo =  $("#crNo").val();
			  userRole =  $("#userRoleChange").val();
			  $(".dataTables_wrapper").show();
		      loadDepCount(projectName,crNo);
			  getProjectWiseData(projectName,crNo,userRole);	
			  
			  $("#allocationTabel").show();
			  $("#printReport").show();
			  $("#chartReport").show();
			  $("#depCount").show();
			 
            }
			return false;
	});
	
	$('#cancelReport').click(
			function(e) {
			$(".dataTables_wrapper").hide();
			$("#crNo").val("");	
			$("#userRoleChange").val("All Employee");
			$("#allocationTabel").hide();
			$("#printReport").hide();
			$("#chartReport").hide();
			$("#depCount").hide();
			$("#pie-chart-div").hide();
				
				
				return false;
	});
	
	$('#chartReport').click(
		function(e) {
				
			$('#pie-chart').remove();
			$('#pie-chart-container').append('<canvas id="pie-chart" width="800" height="450"></canvas>');
			$("#pie-chart-id").text(projectName+" Project Summary");
			loadChart(projectName,crNo,userRole);
			$("#pie-chart-div").show();
		return false;
	});
	

	
	
	$('#printReport').click(
			function(e) {
				
			
			downloadGetProjectWiseData(projectName,crNo,userRole);	

		 	return false;
	});
	
	
	
	$('#crNo').typeahead({
		source : function(query, process) {
			filterName = 'crNo';
			return $.get('/Reslo/getProjectsByCrNo', {
				name : query,
				projectName : projectName
			}, function(data) {
				//data = $.parseJSON(data);
				return process(data);
			});
		}
	});

	
	
});




function  loadDepCount(projectName,crNo) {           
	ajaxGetCall({
		
	}, "/Reslo/getDeptCountByProject?projectName="+projectName+"&crNo="+crNo, function(data){
			
		custodianBankSearchBean = data;
		
		

			$.each(custodianBankSearchBean,
							function() {
								
								if (custodianBankSearchBean[0]['devCount'] != null) {
									$("#devCount").val(custodianBankSearchBean[0]['devCount']);
									
								}

								if (custodianBankSearchBean[0]['qaCount'] != null) {
									$("#qaCount").val(custodianBankSearchBean[0]['qaCount']);
									
								}
								
								if (custodianBankSearchBean[0]['baCount'] != null) {
									$("#baCount").val(custodianBankSearchBean[0]['baCount']);
									
								}

								if (custodianBankSearchBean[0]['managementCount'] != null) {
									$("#managementCount").val(custodianBankSearchBean[0]['managementCount']);
									
								}
							 });
    	});
}

function  downloadGetProjectWiseData(projectName,crNo,userRole) {           

  window.location ="/Reslo/downloadGetProjectWiseData?projectName="+projectName+"&crNo="+crNo+"&userRole="+userRole;
			

}

function getProjectWiseData(projectName,crNo,userRole) {
	ajaxGetCall(
			{},
			"/Reslo/getProjectWiseData?projectName="+projectName+"&crNo="+crNo+"&userRole="+userRole,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#allocationList').dataTable();
				oTable.fnClearTable();

				custodianBankSearchBean = data;

				$.each(custodianBankSearchBean,
								function(i) {
									var  employeeId = employeeName = userRole = allocation = 'N/A';
									count++;


									if (custodianBankSearchBean[i]['employee']['empId'] != null) {
										employeeId = custodianBankSearchBean[i]['employee']['empId'];
									}
									
									if (custodianBankSearchBean[i]['employee']['empName'] != null) {
										employeeName = custodianBankSearchBean[i]['employee']['empName'];
									}
									
									if (custodianBankSearchBean[i]['employee']['userRole'] != null) {
										userRole = custodianBankSearchBean[i]['employee']['userRole'];
									}
									
									if (custodianBankSearchBean[i]['allocation'] != null) {
										allocation = custodianBankSearchBean[i]['allocation'];
									}
									
									oTable.fnAddData([ employeeId, employeeName, userRole, allocation ]);

								});

			
			});
}

function loadChart(projectName,crNo,userRole) {
	ajaxGetCall(
			{},
			"/Reslo/getProjectWiseData?projectName="+projectName+"&crNo="+crNo+"&userRole="+userRole,
			function(data) {

					//custodianBankSearchBean = data;
					loadPieChart(data);

				
			
			});
}


function call_event_handler(value) {
	
	   project = value;
	  
	   //console.log(value);
	   
	   //clear the place holder values
	   $("#crNo").siblings('.label-material').addClass('active');
	   
	   
	   if($("#crNo").val()!=null && $("#crNo").val()!='')
	   {
		   $("#crNo").val(project.crNo); 
	   }
	   
}

function formatDate(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}


function loadPieChart(data) {
	
	var nameArr = [];
	var allocationArr = [];
	
	$.each(data, function( index, value ) {
		  nameArr.push(value.employee.empName);
		  allocationArr.push(value.allocation);
	});
	
	
	
	new Chart(document.getElementById("pie-chart"), {
	    type: 'pie',
	    data: {
	      labels: nameArr,		      
	      datasets: [{
	        label: "Population (millions)",
	        backgroundColor: ["#796AEE","#89ffdf","#C9D631","#ffc36d","#ff7676","#c66798","#54e69d","#C9D631","#e2edd0","#C5B5F3","#ffd9ba","#C9D631","#89ffdf","#c66798","#8baae8","#76f76a","#f7f300","#17c4be","#b55a8f","#e8b4d2","#fcb5c0"],
	       // backgroundColor: ["#796AEE","#ff7676","#54e69d","#ffc36d","#3e95cd","#91F3F5","#91F5CC","#C9D631","#e2edd0","#C5B5F3","#ffd9ba","#C9D631","#89ffdf","#c66798","#8baae8","#76f76a","#f7f300","#17c4be","#b55a8f","#e8b4d2","#fcb5c0"],
	       // backgroundColor: ["#3e95cd", "#C5B5F3","#91CEF5","#91F3F5","#91F5CC","#9AF591", "#C9D631","#e2edd0", "#C5B5F3","#E4B823"],
	        data: allocationArr
	      }]
	    },
	    options: {
	    	legend: {position: 'left'}	     
	    }
	});
}




function getAllProject() {
	ajaxGetCall(
			{},
			"/Reslo/getAllProjectsByName",
			function(data) {
				
			//set data to the drop down
			 for (i = 0; i < data.length; i++) {
					  
			 var name = '<option>' + data[i]['project']['projectName'] + '</option>'
			 $("#projectName").append(name);
			}
			projectName =  $("#projectName").val();

			});	

				
}



function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		/* $body.removeClass("loading"); */
	});
}


