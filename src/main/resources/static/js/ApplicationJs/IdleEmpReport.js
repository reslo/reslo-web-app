var dataSet = [ [ "", "", "", "", "", "", "", "", ""] ];
var userRole = null;
var startDate = null;
var endDate = null;
var firstDay = null;
var lastDay = null;
var dateArray = null;
var holidayArray = null;
var StartTime = null;	
var EndTime = null;	
var employeeName = null;

$( document ).ready(function() {
	
	$("#employeeList").hide();
	$("#printReport").hide();
	$("#bookingChart").hide();
	loadHolidays();
	
	
	$('#previewReport').click(
			function(e) {
			
			startDate = formatDates(new Date($("#startDate").val()));
			endDate = formatDates(new Date($("#endDate").val()));
			userRole = $("#userRoleChange").val();
			
			if (startDate != "NaN/NaN/NaN" && endDate != "NaN/NaN/NaN")
             {
				$('#gantt-chart').remove();
				$('#gantt').append('<div id="gantt-chart"></div>');
			  loadIdleEmpData(startDate,endDate,userRole);
			 
		     }
				
			return false;
	});
	
	$('#cancelReport').click(
			function(e) {
			$("#userRoleChange").val("All Employee");	
			$("#employeeList").hide();
			$("#startDate").val("");
			$("#endDate").val("");
			$("#bookingChart").hide();
			
				
			return false;
	});
	
 
	
});

function formatDates(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}


function setDateStartTime(startDate)
{
	startDate.setHours(0);
	startDate.setMinutes(0);
	startDate.setSeconds(0);
	 
	return startDate;
}

function setDateEndTime(endDate)
{
	endDate.setHours(23);
	endDate.setMinutes(59);
	endDate.setSeconds(59);
	 
	return endDate;
}

function formatTimeStamp(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	return d ;
}


function loadGanttChart(data) {

	var test = null;
	start = formatDates(firstDay).split('/');
	end = formatDates(lastDay).split('/');
	  
    var treeData = anychart.data.tree(data, 'as-table');

    anychart.format.outputTimezone(-330);
    
    // create resource gantt chart
    var chart = anychart.ganttResource();

    // set data for the chart
    chart.data(treeData);
   
    chart.rowSelectedFill('#D4DFE8')
      .rowHoverFill('#EAEFF3')
      // set start splitter position settings
      .splitterPosition(150);


    // get chart data grid link to set column settings
    var dataGrid = chart.dataGrid();

    // set first column settings
    dataGrid.column(0)
      .title('#')
      .width(30)
      .cellTextSettings({
        hAlign: 'center'
      });

    // set second column settings
    dataGrid.column(1)
      .title('Employee Name')
      .width(120);

    // set container id for the chart
    chart.container('gantt-chart');

    // initiate chart drawing
    chart.draw();

    // zoom chart to specified date
   // test = anychart.column;
    //test.tooltip(false);
    chart.getTimeline().tooltip(false);
    
    var start1 = parseInt(start[0]);
    var start2 = parseInt(start[1]);
    var start3 = parseInt(start[2]);
    var end1 = parseInt(end[0]);
    var end2 = parseInt(end[1]);
    var end3 = parseInt(end[2]);
    // zoom chart to specified date
  chart.zoomTo(Date.UTC(start1, start2, start3), Date.UTC(end1, end2, end3));
   
}

  

//do pretty formatting for passed date unit
function formatDate(dateUnit) {
  if (dateUnit < 10) dateUnit = '0' + dateUnit;
  return dateUnit + '';
}

function loadHolidays() {

	ajaxGetCall({}, "/Reslo/getAllHolidays", function(data) {
		list = new Array();
		$.each(data, function(index, value) {
			addHoliday = new Date(value.date);
			list.push(addHoliday);
		});
		holidayArray = list;
	});
}

Date.prototype.addDays = function(days) {
	var date = new Date(this.valueOf())
	date.setDate(date.getDate() + days);
	return date;
}

function getDates(startDate, stopDate) {
	dateArray = new Array();
	var currentDate = startDate;
	while (currentDate <= stopDate) {
		dateArray.push(new Date(currentDate));
		currentDate = currentDate.addDays(1);
	}
	return dateArray;
}

function filterWeekends(dateArray) {
	// $.each(dateArray, function( index, value ) {
	for (i = 0; i < dateArray.length; i++) {
		if (dateArray[i] != undefined
				&& (dateArray[i].toString().includes('Sat') || dateArray[i]
						.toString().includes('Sun'))) {
			dateArray.splice(i, 1);
			i--;
		}
	}
	return dateArray;
}

function filterHolidays(dateArray) {
	// holidayArray
	for (i = 0; i < dateArray.length; i++) {
		index = holidayArray.toString().indexOf(dateArray[i].toString());
		if (index > -1) {
			dateArray.splice(i, 1);
			i--;
		}
	}
	return dateArray;
}

function loadIdleEmpData(startDate,endDate,userRole) {
	ajaxGetCall(
			{},
			"/Reslo/getIdleData?startDate="+startDate+"&endDate="+endDate+"&userRole="+userRole,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#employeeList').dataTable();
				oTable.fnClearTable();

				
				//var date = new Date();
				firstDay = new Date($("#startDate").val());
				lastDay = new Date($("#endDate").val());
				
				dateArray = getDates(firstDay, lastDay);
				dateArray = filterWeekends(dateArray);
				dateArray = filterHolidays(dateArray);
				
				var aDate=null;
				var list =  [];
			
				
				$.each(data, function( indexO, objValue ) {	
					var list = null;
					
					
					list =  $.extend(true, [], dateArray);
					
					if(objValue.allocationDateList!=null)
						{
					 $.each(objValue.allocationDateList, function( indexD, dateValue ) {
						 
						 aDate = formatTimeStamp(dateValue);
			
						 for(var i =0;i<=list.length-1; i++){
						// for(var i = list.length-1; i--;){
							if(list[i].getTime() == aDate.getTime()){
								
								list.splice(i, 1);
							}
						}
					});
					 
					 objValue.allocationDateList= [];
					 
					 $.each(list, function( index, date ) {
						 
						objValue.allocationDateList.push(date); 
					 });
				}
					 else
						{
						 objValue.allocationDateList = [];
						 $.each(dateArray, function( index, date ) {
						
						 objValue.allocationDateList.push(date); 
							 
						});
					}
				});
				var emplist = [];	
				var idCounter = 1;
		$.each(data, function( indexO, objValue ) {	 
			
			employeeName = objValue.employeeName;
			
			
			var periodCounter = 1;
			
			 var jsonObj = {
				      "id": +idCounter,
				      "name":employeeName,
				      "periods":[]
				    };
			 
		   $.each(objValue.allocationDateList, function( indexD, dateValue ) {
					 
					 StartTime = null;
					 EndTime = null;
					 var End = new Date(dateValue);
					
					 StartTime =  setDateStartTime(dateValue);
					
					 EndTime =  setDateEndTime(End);					 
						 
			        var period = {}
			        period.id = +idCounter+"_"+periodCounter; 
			        period.start = +( moment(StartTime).unix() )*1000;
			        period.end = +( moment(EndTime).unix() )*1000;
			        period.stroke = "#B8AA96";
			        jsonObj.periods.push(period);
			        
			        var fill = {}
			        fill.angle = 90; 
			        fill.keys = [{ "color": "#7ec1f5"}];
			        
			        period.fill = fill;
				     
			        periodCounter = periodCounter+1;
					 
					
				  });
		  
		   
		   if(jsonObj.periods.length!=0)
			   {
			   emplist.push(jsonObj);
			   }
				 
				idCounter =  idCounter+1;
				 
			});
		  if(emplist.length!=0)
			  {
			  $("#bookingChart").show();
			   loadGanttChart(emplist);
			  }
		  
		 
			
	});
}



function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		/* $body.removeClass("loading"); */
	});
}


