var startDate = null;
var endDate = null;
var first = null;
var last = null;
var chart = null;
var start = null;
var end = null;
var startIdleDate = null;
var endIdleDate = null;
var pName = [];
var result = [];
var dataSet = [ [ "", "", ""] ];
var projectName = null;
var userRole = null;
var newCount = 0 ;
var inProgressCount = 0;
var onHoldCount = 0;
var closeCount = 0 ;
var employeeName = null;
var employeeRole = null;
var holidayArray = null;
var dateArray = null;


$( document ).ready(function() {
	
	var date = new Date();
	loadHolidays();
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	start = formatDate( firstDay.getFullYear()+'/'+firstDay.getMonth()+'/'+firstDay.getDate() ).split('/');
	end = formatDate(lastDay.getFullYear()+'/'+lastDay.getMonth()+'/'+lastDay.getDate() ).split('/');
    
	var select = document.getElementById("projectName");
	select.options.length = 0;
	
	
	
	$("#projectName").on('change', function() 
			{
			   projectName = $("#projectName").val();
			   
			   loadSelectedProjectData(projectName,userRole);	
			   
			   $('#pie-chart').remove();
			   $('#pie-chart-container').append('<canvas id="pie-chart" width="800" height="450"></canvas>');
			   
			   loadSelectedProject(projectName);
			});
	
	$('input[name=group1]').change(function(){
		 userRole = $("input:radio[name='group1']:checked").val();
		 loadSelectedProjectData(projectName,userRole);
	});
	
	loadProjects(firstDay,lastDay);
	
	startIdleDate = formatDates(date);
	endIdleDate	  = formatDates(date);
	
	loadIdleEmpData(startIdleDate,endIdleDate);
	
	
});

function loadPieChart(data) {
	
	var nameArr = [];
	var allocationArr = [];
	
	$.each(data, function( index, value ) {
		  nameArr.push(value.employee.empName);
		  allocationArr.push(value.allocation);
	});
	
	
	
	new Chart(document.getElementById("pie-chart"), {
	    type: 'pie',
	    data: {
	      labels: nameArr,		      
	      datasets: [{
	        label: "Population (millions)",
	        backgroundColor: ["#796AEE","#89ffdf","#C9D631","#ffc36d","#ff7676","#c66798","#54e69d","#C9D631","#e2edd0","#C5B5F3","#ffd9ba","#C9D631","#89ffdf","#c66798","#8baae8","#76f76a","#f7f300","#17c4be","#b55a8f","#e8b4d2","#fcb5c0"],
	       // backgroundColor: ["#796AEE","#ff7676","#54e69d","#ffc36d","#3e95cd","#91F3F5","#91F5CC","#C9D631","#e2edd0","#C5B5F3","#ffd9ba","#C9D631","#89ffdf","#c66798","#8baae8","#76f76a","#f7f300","#17c4be","#b55a8f","#e8b4d2","#fcb5c0"],
	       // backgroundColor: ["#3e95cd", "#C5B5F3","#91CEF5","#91F3F5","#91F5CC","#9AF591", "#C9D631","#e2edd0", "#C5B5F3","#E4B823"],
	        data: allocationArr
	      }]
	    },
	    options: {
	    	legend: {position: 'left'}	     
	    }
	});
}
function loadGanttChart(data) {
	 
	  //anychart.onDocumentReady(function() {

	 
	  // The data used in this sample can be obtained from the CDN
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  //anychart.data.loadJsonFile('test.json', function(data) {
	
		
	    // create data tree
	    var treeData = anychart.data.tree(data, 'as-table');
	    
	    anychart.format.outputTimezone(-330);

	    // create project gantt chart
	    chart = anychart.ganttProject();
	    
	    

	    // set data for the chart
	    chart.data(treeData);

	    // set start splitter position settings
	    chart.splitterPosition(363);

	    // get chart data grid link to set column settings
	    var dataGrid = chart.dataGrid();
	    
	    // set first column settings
	    dataGrid.column(0).cellTextSettings({
	      hAlign: 'center'
	    })
	    .width(30);

	    // set second column settings
	    dataGrid.column(1)
	      
	      .width(130)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .title('Project Name & CR Name')
	      .format(nameColumnTextFormatter);
 
	    // set third column settings
/*	   dataGrid.column(2)
	      .title('CR No')
	      .width(118)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(crNoColumnTextFormatter);	 */   
	    
	    dataGrid.column(2)
	      .title('Project CR Start Date')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(thirdColumnTextFormatter);
	    
	    // set fourth column settings
	    dataGrid.column(3)
	      .title('Project CR End Date')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(fourthColumnTextFormatter);

	    // set container id for the chart
	    chart.container('gantt-chart');

	    // initiate chart drawing
	    chart.draw();
	    
	   // chart.fitAll();
	    var start1 = parseInt(start[0]);
	    var start2 = parseInt(start[1])+1;
	    var start3 = parseInt(start[2]);
	    var end1 = parseInt(end[0]);
	    var end2 = parseInt(end[1])+1;
	    var end3 = parseInt(end[2]);
	    // zoom chart to specified date
	  chart.zoomTo(Date.UTC(start1, start2, start3), Date.UTC(end1, end2, end3));
	 // });
	//});
	  unique(pName);
	  
	  //set data to the drop down
	  for (i = 0; i < result.length; i++) {
		    var data = '<option>' + result[i] + '</option>'
		    $("#projectName").append(data);
		}
	  
	
	   projectName = $("#projectName").val();
	   
	   userRole = $("input:radio[name='group1']:checked").val();
	   
	   
	   loadSelectedProjectData(projectName,userRole);
	   
	   loadSelectedProject(projectName);
	  
} 

function setDateStartTime(startDate)
{
	startDate.setHours(0);
	startDate.setMinutes(0);
	startDate.setSeconds(0);
	 
	return startDate;
}

function setDateEndTime(endDate)
{
	endDate.setHours(23);
	endDate.setMinutes(59);
	endDate.setSeconds(59);
	 
	return endDate;
}

function formatTimeStamp(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	return d ;
}

function formatDate(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}





//add bold and italic text settings to all parent items
function labelTextSettingsFormatter(label, dataItem) {
  if (dataItem.numChildren()) {
    label.fontWeight('bold').fontStyle('italic');
  }
}


//do pretty formatting for dates in third column
function thirdColumnTextFormatter(item) {
  var field = item.get('actualStart');
  var field1 = item.get('baselineStart');
  var field2 = item.get('actualStartTrue');
  var field3 = item.get('baselineStartTrue');
  

  if(field==field1)
	  {
	// format base line text
	  if (field2) {
	    var baselineStart = new Date(field2);
	    
	    return baselineStart.getFullYear() + '/' + formatDate(baselineStart.getMonth() + 1) + '/' +
	      formatDate(baselineStart.getDate());
	  } else {

	    return '';
	  }
	  
	  }
  else
	  {
	// format base line text
	  if (field3) {
	    var baselineStart = new Date(field3);
	    
	    return baselineStart.getFullYear() + '/' + formatDate(baselineStart.getMonth() + 1) + '/' +
	      formatDate(baselineStart.getDate());
	  } else {

	    return '';
	  }
	  }
  
}

function fourthColumnTextFormatter(item) {
	  var field0 = item.get('baselineStart');  
	  var field = item.get('actualEnd');
	  var field1 = item.get('baselineEnd');
	  var field2 = item.get('actualEndTrue');
	  var field3 = item.get('baselineEndTrue');
	  
	  if(field1==field0 || field==field1)
	  {
	// format base line text
	  if (field2) {
	    var baselineStart = new Date(field2);
	    
	    return baselineStart.getFullYear() + '/' + formatDate(baselineStart.getMonth() + 1) + '/' +
	      formatDate(baselineStart.getDate());
	  } else {

	    return '';
	  }
	  
	  }
  else
	  {
	// format base line text
	  if (field3) {
	    var baselineStart = new Date(field3);
	    
	    return baselineStart.getFullYear() + '/' + formatDate(baselineStart.getMonth() + 1) + '/' +
	      formatDate(baselineStart.getDate());
	  } else {

	    return '';
	  }
	  }
	  
	 
	}

//do pretty formatting for dates in fourth column
function nameColumnTextFormatter(item) {
  
 var field = item.get('id');
  
  pName.push(item.get('id'));
 
  return field;
}


function unique(pName) {
    
    $.each(pName, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

//do pretty formatting for dates in fourth column
function crNoColumnTextFormatter(item) {
  var field = item.get('crNo');
  return field;
}


//do pretty formatting for passed date unit
function formatDate(dateUnit) {
  if (dateUnit < 10) dateUnit = '0' + dateUnit;
  return dateUnit + '';
}

function loadSelectedProjectData(projectName,userRole) {
	ajaxGetCall(
			{},
			"/Reslo/getAllocationByProject?projectName="+projectName+"&userRole="+userRole,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#allocationDetailList').dataTable();
				oTable.fnClearTable();

				custodianBankSearchBean = data;

				$
						.each(
								custodianBankSearchBean,
								function(i) {
									var empId = empName = allocation ='N/A';
									count++;

									if (custodianBankSearchBean[i]['employee']['empId'] != null) {
										empId = custodianBankSearchBean[i]['employee']['empId'];
									}

									if (custodianBankSearchBean[i]['employee']['empName'] != null) {
										empName = custodianBankSearchBean[i]['employee']['empName'];
									}

									if (custodianBankSearchBean[i]['allocation'] != 'N/A') {
										allocation = custodianBankSearchBean[i]['allocation'];
									}

									oTable.fnAddData([ empId, empName, allocation ]);

								});
				

				
			});
}


function loadSelectedProject(projectName) {
	ajaxGetCall(
			{},
			"/Reslo/getAllocationBySelectedProject?projectName="+projectName,
			function(data) {
				if(data.length==0)
					{
					$("#pie-chart-id").text("No Records Found for "+projectName);
					
					}
				else
					{
					$("#pie-chart-id").text(projectName+" Project Summary");
					}
				loadPieChart(data);
			});
}


function  loadProjects(startDate,endDate) {           
	ajaxGetCall({
		startDate : startDate,
		endDate : endDate
	}, "/Reslo/getGanttChartData", function(data){
			loadGanttChart(data.chartList);
			
			var status = new Object();
			
			status.New = [];
			status.InProgress = [];
			status.OnHold = [];
			status.Close = [];
			 
			$.each( data.chartList, function( key, value ) {
				 var projectName = value.id;
				 
				status[value.status].push(value);
				
				 
				});
			
			if(status.New.length == 0)
			{
				
				$("#new-modal-body").empty();
				$("#new-modal-body").append('<p>No Records Found !</p>');
			}
			else
			{
			for (i = 0; i < status.New.length; i++){
				$("#newProjectBody").append('<tr><td>'+status.New[i].id+'</td><td>'+status.New[i].crNo+'</td></tr>');
							
				}
			}
			
			if(status.InProgress.length == 0)
			{
				$("#onGoing-modal-body").empty();
				$("#onGoing-modal-body").append('<p>No Records Found !</p>');
			}
			else
			{
			for (i = 0; i < status.InProgress.length; i++){
				$("#onGoingProjectBody").append('<tr><td>'+status.InProgress[i].id+'</td><td>'+status.InProgress[i].crNo+'</td></tr>');
							
			}
			}
			
			if(status.OnHold.length == 0)
			{
				$("#hold-modal-body" ).empty();
				$("#hold-modal-body").append('<p>No Records Found !</p>');
			}
			else
			{
				
			for (i = 0; i < status.OnHold.length; i++){
				$("#holdProjectBody").append('<tr><td>'+status.OnHold[i].id+'</td><td>'+status.OnHold[i].crNo+'</td></tr>');
							
			}
			}
			
			if(status.Close.length == 0)
			{
				
				$("#close-modal-body" ).empty();
				$("#close-modal-body").append('<p>No Records Found !</p>');
			}
			else
			{
			for (i = 0; i < status.Close.length; i++){
				$("#closeProjectBody").append('<tr><td>'+status.Close[i].id+'</td><td>'+status.Close[i].crNo+'</td></tr>');
							
			}
			}
			
			
			
			inProgressCount=data.inProgressCount;
			onHoldCount=data.onHoldCount;
			closeCount=data.closeCount;
			newCount=data.newProjectCount;
			$("#inProgress").append('<strong>'+inProgressCount+'</strong><br><small>On Going Projects</small>');
			$("#onHold").append('<strong>'+onHoldCount+'</strong><br><small>Hold Projects</small>');
			$("#close").append('<strong>'+closeCount+'</strong><br><small>Close Projects</small>');
			$("#new").append('<strong>'+newCount+'</strong><br><small>New Projects</small>');
			
			projectName = $("#projectName").val();
			$("#pie-chart-id").text(projectName+" Project Summary"); 
			
			
    	});
}

function getDates(startDate, stopDate) {
	dateArray = new Array();
	var currentDate = startDate;
	while (currentDate <= stopDate) {
		dateArray.push(new Date(currentDate));
		currentDate = currentDate.addDays(1);
	}
	return dateArray;
}

function filterWeekends(dateArray) {
	// $.each(dateArray, function( index, value ) {
	for (i = 0; i < dateArray.length; i++) {
		if (dateArray[i] != undefined
				&& (dateArray[i].toString().includes('Sat') || dateArray[i]
						.toString().includes('Sun'))) {
			dateArray.splice(i, 1);
			i--;
		}
	}
	return dateArray;
}

function loadHolidays() {

	ajaxGetCall({}, "/Reslo/getAllHolidays", function(data) {
		list = new Array();
		$.each(data, function(index, value) {
			addHoliday = new Date(value.date);
			list.push(addHoliday);
		});
		holidayArray = list;
	});
}

function filterHolidays(dateArray) {
	// holidayArray
	for (i = 0; i < dateArray.length; i++) {
		index = holidayArray.toString().indexOf(dateArray[i].toString());
		if (index > -1) {
			dateArray.splice(i, 1);
			i--;
		}
	}
	return dateArray;
}

function formatDates(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

Date.prototype.addDays = function(days) {
	var date = new Date(this.valueOf())
	date.setDate(date.getDate() + days);
	return date;
}


function loadIdleEmpData(startDate,endDate) {
	ajaxGetCall(
			{},
			"/Reslo/getIdleEmpforHomePage?startDate="+startDate+"&endDate="+endDate,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var accBalance = null;

				//var date = new Date();
				firstDay = new Date(startDate);
				lastDay = new Date(endDate);
				
				dateArray = getDates(firstDay, lastDay);
				dateArray = filterWeekends(dateArray);
				dateArray = filterHolidays(dateArray);
				
				var aDate=null;
				var list =  [];
			
				
				$.each(data, function( indexO, objValue ) {	
					var list = null;
					
					list =  $.extend(true, [], dateArray);
					
					if(objValue.allocationDateList!=null)
						{
					 $.each(objValue.allocationDateList, function( indexD, dateValue ) {
						 
						 aDate = formatTimeStamp(dateValue);
			
						 for(var i =0;i<=list.length-1; i++){
						 	 
							if(list[i].getTime() == aDate.getTime()){
								
								list.splice(i, 1);
							}
						}
					});
					 
					 objValue.allocationDateList= [];
					 
					 $.each(list, function( index, date ) {
						 
						objValue.allocationDateList.push(date); 
					 });
				}
					 else
						{
						 objValue.allocationDateList = [];
						 $.each(dateArray, function( index, date ) {
						
						 objValue.allocationDateList.push(date); 
							 
						});
					}
				});
				var emplist = [];	
				var idCounter = 1;
		$.each(data, function( indexO, objValue ) {	 
			
			employeeName = objValue.employeeName;
			employeeRole = objValue.employeeRole;
			
			var periodCounter = 1;
			
			 var jsonObj = {
				      "id": +idCounter,
				      "name":employeeName,
				      "periods":[]
				    };
			 
		   $.each(objValue.allocationDateList, function( indexD, dateValue ) {
					 
					 StartTime = null;
					 EndTime = null;
					 var End = new Date(dateValue);
					
					 StartTime =  setDateStartTime(dateValue);
					
					 EndTime =  setDateEndTime(End);					 
						 
			        var period = {}
			        period.id = +idCounter+"_"+periodCounter; 
			        period.start = +( moment(StartTime).unix() )*1000;
			        period.end = +( moment(EndTime).unix() )*1000;
			        period.stroke = employeeRole;
			        jsonObj.periods.push(period);
			        
			        var fill = {}
			        fill.angle = 90; 
			        fill.keys = [{ "color": "#7ec1f5"}];
			        
			        period.fill = fill;
				     
			        periodCounter = periodCounter+1;
					 
					
				  });
		   if(jsonObj.periods.length!=0)
		   {
		   emplist.push(jsonObj);
		   }
			 
			idCounter =  idCounter+1;
			 
		});
	  
		$("#idleCount").append('<strong>'+emplist.length+'</strong>');
		
		if(emplist.length == 0)
		{
			
			$("#idleEmp-modal-body").empty();
			$("#idleEmp-modal-body").append('<p>No Records Found !</p>');
		}
		else
		{
		for (i = 0; i < emplist.length; i++){
			$("#idleEmpListBody").append('<tr><td>'+emplist[i].name+'</td><td>'+emplist[i].periods[0].stroke+'</td></tr>');
						
			}
		}
					 
			
	});
	getworkloadSummaryforHomePage(startIdleDate,endIdleDate);
}


function getworkloadSummaryforHomePage(startDate,endDate) {
	ajaxGetCall(
			{},
			"/Reslo/getworkloadSummaryforHomePage?startDate="+startDate+"&endDate="+endDate,
			function(data) {
				var underAllocatedCout = 0 ;
				var overAllocatedCout = 0 ;
				
				$.each(data, function( index, obj ) {
				
					if(obj.allocation<=100)
						{
						
						$("#underAllocatedEmpListBody").append('<tr><td>'+obj.name+'</td><td>'+obj.status+'</td><td>'+obj.allocation+'</td></tr>');
						underAllocatedCout = underAllocatedCout+1;	
						}
					else
					{
						overAllocatedCout++;
						$("#overAllocatedEmpListBody").append('<tr><td>'+obj.name+'</td><td>'+obj.status+'</td><td>'+obj.allocation+'</td></tr>');
					}
				});
				
				$("#underAllocatedCout").append('<strong>'+underAllocatedCout+'</strong>');
				$("#overAllocatedCout").append('<strong>'+overAllocatedCout+'</strong>');
				
				if(underAllocatedCout == 0)
				{
					
					$("#underAllocatedEmp-modal-body").empty();
					$("#underAllocatedEmp-modal-body").append('<p>No Records Found !</p>');
					
				}
				
				if(overAllocatedCout == 0)
				{
					
					$("#overAllocatedEmp-modal-body").empty();
					$("#overAllocatedEmp-modal-body").append('<p>No Records Found !</p>');
					
				}
				
				var date = new Date(), y = date.getFullYear(), m = date.getMonth();
				first = new Date(y, m+1, 1);
				last = new Date(y, m+1 + 1, 0);
				getBookingEmpforHomePage(first,last);
			
			});
}


function getBookingEmpforHomePage(first,last) {
	ajaxGetCall(
			{first : first,
			 last : last},
			"/Reslo/getBookingEmployeesforHomePage",
			function(data) {
				
				var bookedEmpCout = 0 ;
				
				$.each(data, function( index, obj ) {
				
				$("#BookingEmpListBody").append('<tr><td>'+obj.name+'</td><td>'+obj.crNo+'</td><td>'+obj.allocation+'</td></tr>');
				bookedEmpCout = bookedEmpCout+1;	
					
				});
				
				if(bookedEmpCout == 0)
				{
					
					$("#BookingEmp-modal-body").empty();
					$("#BookingEmp-modal-body").append('<p>No Records Found !</p>');
					$("#BookingEmpCount").append('<strong>'+bookedEmpCout+'</strong>');
				}
				else
				{
					$("#BookingEmpCount").append('<strong>'+bookedEmpCout+'</strong>');
				}
				
				
			
			});
}

function ajaxGetCall(data, url,callback){
	
	$.ajax({
		  type: "GET",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}
