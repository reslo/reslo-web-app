var dataSet = [ [ "", "", "", "", "", "", "", "", ""] ];
var employeeName = null;
var startDate = null;
var endDate = null;
var filterName = 'empName';
var totalAlloaction = null;


$( document ).ready(function() {
	
	$("#employeeList").hide();
	$("#printReport").hide();
	$("#chartReport").hide();
	$("#empWiseChart").hide();
	
	
	$('#previewReport').click(
			function(e) {
			
			employeeName = $("#employeeName").val();
			startDate = formatDates(new Date($("#startDate").val()));
			endDate = formatDates(new Date($("#endDate").val()));
			
			
			if (startDate != "" && endDate != ""  && employeeName != "")
             {

			  loadEmployeeWiseData(startDate,endDate,employeeName);
			  $(".dataTables_wrapper").show();
			  $("#employeeList").show();
			  $("#printReport").show();
			  $("#chartReport").show();
		     }
				
				
				return false;
	});
	
	$('#cancelReport').click(
			function(e) {
			$(".dataTables_wrapper").hide();
			$("#employeeName").val("");
			$("#startDate").val("");
			$("#endDate").val("");
			$("#employeeList").hide();
			$("#printReport").hide();
			$("#chartReport").hide();
				
				
			return false;
	});
	
	$('#chartReport').click(
			function(e) {
				$('#gantt-chart').remove();
				$('#gantt').append('<div id="gantt-chart"></div>');
				$("#empWiseChart").show();
				loadChart(startDate,endDate,employeeName);
				
				
				
	    return false;
	});
	
	$('#printReport').click(
			function(e) {
				
		downloadEmpWiseData(startDate,endDate,employeeName,totalAlloaction);
				
	    return false;
	});
	

	 $('input.typeahead').typeahead({
			source : function(query, process) {
				return $.get('/Reslo/getEmployeeByName', {
					name : query
				}, function(data) {
					//data = $.parseJSON(data);
					return process(data);
				});
			}
		});
	 
	    $('#employeeList').DataTable( {
	        "footerCallback": function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	 
	            // Total over all pages
	            total = api
	                .column( 8 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = api
	                .column( 8, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Update footer
	            $( api.column( 8 ).footer() ).html(pageTotal +'%');
	            totalAlloaction =pageTotal;
	        }
	    } );
	
});

function formatDates(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

function loadGanttChart(data) {
	  //anychart.onDocumentReady(function() {
	
	  var date = new Date();
	  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	
	
	  start = formatDates(firstDay).split('/');
	  end = formatDates(lastDay).split('/');
	
	  // The data used in this sample can be obtained from the CDN
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  //anychart.data.loadJsonFile('test.json', function(data) {
		
	    // create data tree
	    var treeData = anychart.data.tree(data, 'as-table');
	    
	    anychart.format.outputTimezone(-330);

	    // create project gantt chart
	    chart = anychart.ganttProject();
	    
	    

	    // set data for the chart
	    chart.data(treeData);

	    // set start splitter position settings
	    chart.splitterPosition(515);

	    // get chart data grid link to set column settings
	    var dataGrid = chart.dataGrid();
	    
	    // set first column settings
	    dataGrid.column(0).cellTextSettings({
	      hAlign: 'center'
	    })
	    .width(30);

	    // set second column settings
	    dataGrid.column(1)
	      .width(150)
	      .title('Project Name & CR Name')
	      .cellTextSettingsOverrider(labelTextSettingsFormatter);

	    dataGrid.column(2)
	      
	      .width(130)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .title('CR No')
	      .format(crNoColumnTextFormatter);
	    
	    // set third column settings
	    dataGrid.column(3)
	      .title('Plan Start')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(thirdColumnTextFormatter);

	    // set fourth column settings
	    dataGrid.column(4)
	      .title('Actual End')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(fourthColumnTextFormatter);
	    

	    // set container id for the chart
	    chart.container('gantt-chart');

	    // initiate chart drawing
	    chart.draw();
	    
	   // chart.fitAll();
	    var start1 = parseInt(start[0]);
	    var start2 = parseInt(start[1]);
	    var start3 = parseInt(start[2]);
	    var end1 = parseInt(end[0]);
	    var end2 = parseInt(end[1]);
	    var end3 = parseInt(end[2]);
	    // zoom chart to specified date
	  chart.zoomTo(Date.UTC(start1, start2, start3), Date.UTC(end1, end2, end3));
	 // });
	//});
	  
} 

//add bold and italic text settings to all parent items
function labelTextSettingsFormatter(label, dataItem) {
  if (dataItem.numChildren()) {
    label.fontWeight('bold').fontStyle('italic');
  }
}

//do pretty formatting for dates in third column
function thirdColumnTextFormatter(item) {
  var field = item.get('actualStart');
  var field1 = item.get('baselineStart');
  var field2 = item.get('actualStartTrue');
  var field3 = item.get('baselineStartTrue');
  

  if(field==field1)
	  {
	// format base line text
	  if (field2) {
	    var baselineStart = new Date(field2);
	    
	    return formatDate(baselineStart.getDate()) + '/' + formatDate(baselineStart.getMonth() + 1) + '/' +baselineStart.getFullYear();
	  } else {

	    return '';
	  }
	  
	  }
  else
	  {
	// format base line text
	  if (field3) {
	    var baselineStart = new Date(field3);
	    
	    return formatDate(baselineStart.getDate()) + '/' + formatDate(baselineStart.getMonth() + 1) + '/' +baselineStart.getFullYear();
	  } else {

	    return '';
	  }
	  }
  
}

function fourthColumnTextFormatter(item) {
	  var field0 = item.get('baselineStart');  
	  var field = item.get('actualEnd');
	  var field1 = item.get('baselineEnd');
	  var field2 = item.get('actualEndTrue');
	  var field3 = item.get('baselineEndTrue');
	  
	  if(field1==field0 || field==field1)
	  {
	// format base line text
	  if (field2) {
	    var baselineStart = new Date(field2);
	    
	    return formatDate(baselineStart.getDate()) + '/' + formatDate(baselineStart.getMonth() + 1) + '/' +baselineStart.getFullYear();
	  } else {

	    return '';
	  }
	  
	  }
  else
	  {
	// format base line text
	  if (field3) {
	    var baselineStart = new Date(field3);
	    
	    return formatDate(baselineStart.getDate()) + '/' + formatDate(baselineStart.getMonth() + 1) + '/' +baselineStart.getFullYear();
	  } else {

	    return '';
	  }
	  }
	  
	 
	} 
	 


//do pretty formatting for dates in fourth column
function nameColumnTextFormatter(item) {
  
 var field = item.get('id');
  
  pName.push(item.get('id'));
 
  return field;
}


function unique(pName) {
    
    $.each(pName, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

//do pretty formatting for dates in fourth column
function crNoColumnTextFormatter(item) {
  var field = item.get('crNo');
  return field;
}


//do pretty formatting for passed date unit
function formatDate(dateUnit) {
  if (dateUnit < 10) dateUnit = '0' + dateUnit;
  return dateUnit + '';
}


function call_event_handler(value) {

	
	if (value && value.empName) {
		employee = value;
		empId=employee.empId;
	}

	console.log(value);


}




function loadEmployeeWiseData(startDate,endDate,employeeName) {
	ajaxGetCall(
			{},"/Reslo/getEmployeeWiseData?startDate="+startDate+"&endDate="+endDate+"&employeeName="+employeeName,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#employeeList').dataTable();
				oTable.fnClearTable();

				custodianBankSearchBean = data;

				$.each(custodianBankSearchBean,
								function(i) {
									var projectName = crNo = projectStatus = startDate = endDate = actualStartDate = actualEndDate = projectProgress = allocation = 'N/A';
									count++;

								

									if (custodianBankSearchBean[i]['id'] != null) {
										projectName = custodianBankSearchBean[i]['id'];
									}

									if (custodianBankSearchBean[i]['crNo'] != null) {
										crNo = custodianBankSearchBean[i]['crNo'];
									}
									
									if (custodianBankSearchBean[i]['status'] != null) {
										projectStatus = custodianBankSearchBean[i]['status'];
									}

								
									if (custodianBankSearchBean[i]['actualStartTrue'] != null) {
										startDate = formatDates(custodianBankSearchBean[i]['actualStartTrue']);
									}
									
									if (custodianBankSearchBean[i]['actualEndTrue'] != null) {
										endDate = formatDates(custodianBankSearchBean[i]['actualEndTrue']);
									}
									
									if (custodianBankSearchBean[i]['baselineStartTrue'] != null) {
										actualStartDate = formatDates(custodianBankSearchBean[i]['baselineStartTrue']);
									}
									
									if (custodianBankSearchBean[i]['baselineEndTrue'] != null) {
										actualEndDate = formatDates(custodianBankSearchBean[i]['baselineEndTrue']);
									}

									if (custodianBankSearchBean[i]['progressValue'] != "null%") {
										projectProgress = custodianBankSearchBean[i]['progressValue'];
									}
									
									if (custodianBankSearchBean[i]['allocation'] != null) {
										allocation = custodianBankSearchBean[i]['allocation'];
									}

									
									oTable.fnAddData([projectName, crNo, projectStatus, startDate, endDate, actualStartDate, actualEndDate, projectProgress, allocation ]);
									
									var totalRecords =$('#employeeList').DataTable().page.info().recordsTotal;
									if(totalRecords === 0)
										{
										$("#printReport").hide();
										}
									else
										{
										$("#printReport").show();
										
										}
									
								});

			
			});
}

function  loadTotal() {  

} 

function  loadChart(startDate,endDate,employeeName) {           
	ajaxGetCall({}, "/Reslo/getEmployeeWiseData?startDate="+startDate+"&endDate="+endDate+"&employeeName="+employeeName, function(data){
			loadGanttChart(data);
			
	}			
)}

function  downloadEmpWiseData(startDate,endDate,employeeName,totalAlloaction) {  
	
	window.location = "/Reslo/downloadEmpWiseData?startDate="+startDate+"&endDate="+endDate+"&employeeName="+employeeName+"&totalAlloaction="+totalAlloaction;


}

function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		
	});
}


