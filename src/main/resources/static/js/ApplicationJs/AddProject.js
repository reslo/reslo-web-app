var projectName = null;
var crNo = null;
var startDate = null;
var endDate = null;
var status = null;
var createdUser = null;
var Start = null;
var End = null;
var actualEnd = null;
var projectProgress =null;

$( document ).ready(function() {


    
	$('#statusChange').hide();
	$('#newProjectRadio').prop('checked', true); 
	 loadData();
	 
		 $('#addProject').click(
			function(e) {
				
				projectName = $("#projectName").val();
				crNo = $("#crNo").val();
				startDate = $("#startDate").val();
				endDate = $("#endDate").val();
				createdUser = $("#createdUser").val();
				
				if ($("#onGoingProjectRadio").prop("checked")) {
					status = $("#status").val();
				}
				else{
					status = "New";
				}
				
				createProject(projectName, crNo, startDate, endDate, createdUser, status);
				
				
				$("#projectName").val("");
				$("#projectName").siblings('.label-material').removeClass('active');
				$("#startDate").val("");
				$("#startDate").siblings('.label-material').removeClass('active');
				$('#statusChange').hide();
				$("#crNo").val("");
				$("#crNo").siblings('.label-material').removeClass('active');
				$("#endDate").val("");
				$("#endDate").siblings('.label-material').removeClass('active');
				$('#onGoingProjectRadio').prop('checked', false);
				$('#newProjectRadio').prop('checked', true);
				
				return false;
			});
	 
});

$('#cancelProject').click(
		function(e) {
			
			$("#projectName").val("");
			$("#projectName").siblings('.label-material').removeClass('active');
			$("#startDate").val("");
			$("#startDate").siblings('.label-material').removeClass('active');
			$('#statusChange').hide();
			$("#crNo").val("");
			$("#crNo").siblings('.label-material').removeClass('active');
			$("#endDate").val("");
			$("#endDate").siblings('.label-material').removeClass('active');
			$('#onGoingProjectRadio').prop('checked', false);
			$('#newProjectRadio').prop('checked', true);
		});

$('#onGoingProjectRadio').click(
		function() {
			$('#statusChange').show();
		});

$('#newProjectRadio').click(
		function() {
			$('#statusChange').hide();
		});

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}


function  loadData() {           
	ajaxGetCall({}, "/Reslo/getAllProjects", function(data){
                   
    				//if(data.errorCode == 0){   
                        var count = 0;
                        var appName = "N/A";
                        var name ="N/A";
                        var accBalance = null;
                      
                        //get the DataTable
                        var oTable = $('#projectList').dataTable(); 
                        oTable.fnClearTable();
                        
                        custodianBankSearchBean = data;

                        $.each(custodianBankSearchBean, function(i) {
                        	 var projectName = actualEndDate = actualStartDate =name = reason = projectProgress= projectStatus = startDate = endDate = 'N/A';
                             count++;
                             
                             if(custodianBankSearchBean[i]['projectName']!= null)
                             { projectName = custodianBankSearchBean[i]['projectName'];}
                             
                             if(custodianBankSearchBean[i]['crNo']!= null)
                             { crNo = custodianBankSearchBean[i]['crNo'];}
                             
                             if(custodianBankSearchBean[i]['projectStatus']!= 'N/A')
                             { 
                           	 projectStatus = custodianBankSearchBean[i]['projectStatus'];
                             }
                             
                             
                             
                             if(projectStatus == "New")
                       	  {
                       	   projectStatus = 'New Project';
                       	  }
                             
                             if(custodianBankSearchBean[i]['startDate']!= 'N/A')
                             {   
                           	  Start = formatDate(custodianBankSearchBean[i]['startDate']);
                           	  startDate = Start;}
                             
                             if(custodianBankSearchBean[i]['endDate']!= 'N/A')
                             { 
                           	  End = formatDate(custodianBankSearchBean[i]['endDate']);
                           	  endDate = End;}
                             
                             if(custodianBankSearchBean[i]['actualStartDate']!= null)
                             { 
                           	  actualStart = formatDate(custodianBankSearchBean[i]['actualStartDate']);
                           	  actualStartDate = actualStart;}
                             else{
                           	  actualStartDate = 'N/A';
                             }
                             
                             if(custodianBankSearchBean[i]['actualEndDate']!= null)
                             { 
                           	  actualEnd = formatDate(custodianBankSearchBean[i]['actualEndDate']);
                           	  actualEndDate = actualEnd;}
                             else{
                           	  actualEndDate = 'N/A';
                             }
                             
                             if(custodianBankSearchBean[i]['projectProgress']!= null)
                             { 
                           	  projectProgress = custodianBankSearchBean[i]['projectProgress'];
                             }
                             
                             if(custodianBankSearchBean[i]['reason']!= null)
                             { reason = custodianBankSearchBean[i]['reason'];}
                                                     
                             oTable.fnAddData([projectName,crNo,projectStatus,startDate,endDate,actualStartDate,actualEndDate,projectProgress,reason]);
                                   
                           });

                   /* }
        else{
               $("#errorMsg").text("Sorry. Requested data not available.");
                     $('#modelError').modal();
                     setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
        }*/
             });
}
 


function createProject(projectName, crNo, startDate,endDate, createdUser,status) {
	ajaxCall({
		projectName : projectName,
		crNo : crNo,
		startDate : startDate,
		endDate : endDate,
		createdUser : createdUser,
		status : status,
	}, "/Reslo/Project", function(data) {
		if (data == 1) {
			
			 loadData();
			
			$("#successMsg").text("Project Successfully Added !");
       	    $('#modelSuccess').modal();
       	    setTimeout(function() { $('#modelSuccess').modal('hide'); }, 3000);
			
		} else {
		
			  loadData();
			 
			  $("#errorMsg").text("Project Added Failed !");
	          $('#modelError').modal();
	          setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
		}
	});
}


function ajaxCall(data, url,callback){
	
	$.ajax({
		  type: "POST",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}

function ajaxGetCall(data, url,callback){
	
	$.ajax({
		  type: "GET",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}


