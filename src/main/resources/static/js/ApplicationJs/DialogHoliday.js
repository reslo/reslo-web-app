var getDate = null;
var description = null;
var table = null;
var resultData = null;
var holidayDate = null;

var dataSet = [
    [ "", "" ]];

$( document ).ready(function() {

    
	loadData();
	
	table = $('#holidayList').DataTable( {
        data: dataSet,
        columns: [
            { title: "Date" },
            { title: "Discription"},
            {title: "Action"}            
        ],
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "<button>Remove</button>"
        } ]
    } );
	 
	table.clear().draw();
	
	$('#holidayList tbody').on( 'click', 'button', function () {
        var data = table.row( $(this).parents('tr') ).data();
        table
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    } );
	 
	
	 
});


function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}

$("#dateRangePicker").keyup(function() {

	getDate=null;

});

$('#dateRangePicker')
.datepicker({
    format: 'dd/mm/yyyy',
    startDate: '01/01/2010',
    endDate: '30/12/2020'
})
.on('changeDate', function(e) {
	
	 getDate = formatDate(e.date);
	 
    // Revalidate the date field
   // $('#dateRangeForm').formValidation('revalidateField', 'date');
});

$('#addHoliday').click(
		function(e) {
			description = $("#description").val();
			
				if(getDate == null && description == 'Select a Holoday Status')
				{
					alert("EMPTY");
				}
				else if(getDate == null)
				{
					alert("EMPTY");
				}
				
				else if(description == 'Select a Holoday Status')
				{
					alert("EMPTY");
				}
				
			
			
			else{
				if ($('#holidayList td:contains("' + getDate + '")').length)
				{
			    alert("FALSE");
		        } else {

			      $('#holidayList').dataTable().fnAddData([ getDate, description ]);
			      $("#holidayDate").val("");
			      $("#description").val('Select a Holoday Status');
			      
			      
		         }
			}
		});
$('#submitHolidays').click(function(e) {
	var holidayList = [];
	resultData = $('#holidayList').dataTable().fnGetData();
	
	$.each(resultData, function( index, value ) {
		var datarr = value[0].split("/");
		var dateEntry = {date:new Date(datarr[0]+"/"+datarr[1]+"/"+datarr[2]), desc:value[1]};
		holidayList.push(dateEntry);
	});
	
	addHoliday(holidayList);
});

function  loadData() {           
	ajaxGetCall({}, "/Reslo/getAllHolidays", function(data){
                   
    				//if(data.errorCode == 0){   
                        var count = 0;
                        var appName = "N/A";
                        var name ="N/A";
                        var accBalance = null;
                      
                        //get the DataTable
                        var oTable = $('#holidayList').dataTable(); 
                        oTable.fnClearTable();
                        
                        custodianBankSearchBean = data;

                        $.each(custodianBankSearchBean, function(i) {
                                  var date = description = 'N/A';
                                  count++;
                                  
                                  if(custodianBankSearchBean[i]['date']!= null)
                                  {    holidayDate = formatDate(custodianBankSearchBean[i]['date']);
                                       date = holidayDate;
                                  }
                                  
                                  if(custodianBankSearchBean[i]['description']!= null)
                                  { description = custodianBankSearchBean[i]['description'];}
                                  
                                                          
                                  oTable.fnAddData([date,description]);
                                   
                           });

                   /* }
        else{
               $("#errorMsg").text("Sorry. Requested data not available.");
                     $('#modelError').modal();
                     setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
        }*/
             });
}

function addHoliday(resultData) {
	ajaxCall(resultData, "/Reslo/DialogHolidays", function(data) {
		if (data == 1) {
			
			 $("#successMsg").text("Holidays Successfully Added !");
        	 $('#modelSuccess').modal();
        	 setTimeout(function() { $('#modelSuccess').modal('hide'); }, 3000);
			
		} else {
			
			  $("#errorMsg").text("Holiday Added Failed !");
	          $('#modelError').modal();
	          setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
		}
	});
}


function ajaxGetCall(data, url,callback){
	
	$.ajax({
		  type: "GET",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}

function ajaxCall(data, url,callback){
	
	$.ajax({
		  type: "POST",
		  dataType: 'json',
		  data: JSON.stringify(data),
		  contentType: 'application/json; charset=utf-8',
		  url: url	  
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass('loading');
	  	});
}