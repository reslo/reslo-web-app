var userId = null;
var createdBy = null;
var email = null;
var mobile = null;
var userName = null;
var filterName = 'fullName';

function submitform() {

    $('#submit_handle').click();
}


$( document ).ready(function() {
	
  
	
	 $('#createUser').click(function(e) { 			        
    	
		// submitform();
		 
		 userId=$("#userId").val();
    	 createdBy=$("#createdUser").val();	 
    	 email=$("#email").val(); 
    	 mobile = $("#contactNo").val(); 
    	 userName = $("#employeeName").val();    
    	 //e.preventDefault();
    	 if(userId != "")
    	{
    	 createUser(userId,createdBy,email,mobile,userName);
    	 $("#userId").val("");
    	 $("#userId").siblings('.label-material').removeClass('active');
    	 $("#email").val(""); 
    	 $("#email").siblings('.label-material').removeClass('active');
    	 $("#contactNo").val(""); 
    	 $("#contactNo").siblings('.label-material').removeClass('active');
    	 $("#employeeName").val(""); 
    	 $("#employeeName").siblings('.label-material').removeClass('active');
	    }
    	
    	 
      return false;   
});
	
	
	
	$('#employeeName').typeahead({
		source : function(query, process) {
			return $.get('/Reslo/getUserByName', {
				name : query
			}, function(data) {
				//data = $.parseJSON(data);
				return process(data);
			});
		}
	});


	
	
	
});

function call_event_handler(value) {
	   //console.log(value);
	   
	   //clear the place holder values
	   $("#contactNo").siblings('.label-material').addClass('active');
	   $("#email").siblings('.label-material').addClass('active');
	   $("#userId").siblings('.label-material').addClass('active');
	   
	   //set to text field input
	   $("#contactNo").val(value.mobileNumber);
	   $("#email").val(value.email);
	   $("#userId").val(value.id);
	   $("#userName").val($("#employeeName").val());
	   
	   
	   $("#employeeName").keyup(function() {

		    if (!this.value) {
		    	$("#contactNo").val("");
		    	$("#contactNo").siblings('.label-material').removeClass('active');
				$("#email").val("");
				$("#email").siblings('.label-material').removeClass('active');
				$("#userId").val("");
				$("#userId").siblings('.label-material').removeClass('active');
		    }

		});
	   
	}

function createUser(userId,createdBy,email,mobile,userName) {
	ajaxCall({
		userId : userId,
		createdBy : createdBy,
		email : email,
		mobile : mobile,
		userName : userName,
	}, "/Reslo/userCreation", function(data) {
		if (data == 1) {
		
			$("#successMsg").text("Aplication User Successfully Adeded !");
         	$('#modelSuccess').modal();
       	    setTimeout(function() { $('#modelSuccess').modal('hide'); }, 3000);
			
		} else {
			
        	  $("#errorMsg").text(data.errorMessage);
	          $('#modelError').modal();
	          setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
		}
	});
}

function ajaxCall(data, url,callback){
	
	$.ajax({
		  type: "POST",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}