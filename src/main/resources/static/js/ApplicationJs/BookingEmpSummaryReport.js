var dataSet = [ [ "", "", "", "", ""] ];
var filterName = 'empName';
var startDate = null;
var start = null;
var empId = null;
var empName = null;
var employee = null;
var diffDays = null;

$( document ).ready(function() {
	
	
	$("#printReport").hide();
	$("#chartReport").hide();
	$("#bookingChart").hide();
	$("#projectTabel").hide();
	
	
	
	$('#previewReport').click(
			function(e) {
			empId = employee.empId;
			start = $('#startDate').datepicker({ dateFormat: 'yyyy/mm/dd' }).val();
			empName = $("#employeeName").val();
			
			
			if (startDate != "")
             {

			  getAllProjects(empId,start);
			  $(".dataTables_wrapper").show();
			  $("#projectTabel").show();
			  $("#printReport").show();
			  $("#chartReport").show();
		     }
							
			return false;
	});
	


	$('#employeeName').change(function(e) {
		
		var input = document.getElementById('employeeName');

		input.onkeydown = function() {
		    var key = event.keyCode || event.charCode;

		    if( key == 8 || key == 46 )
		    $("#projectTabel").hide();
			$("#printReport").hide();
			$("#chartReport").hide();
			$("#bookingChart").hide();
			
			
		};
    });


	
	
	$('#cancelReport').click(
			function(e) {
			$("#employeeName").val("");		
			$("#startDate").val("");
			$("#projectTabel").hide();
			$("#printReport").hide();
			$("#chartReport").hide();
			$("#bookingChart").hide();
			$(".dataTables_wrapper").hide();	
			$('#dateRangePicker').datepicker({
				startDate : '+' + diffDays + 'd',
				format : 'yyyy/mm/dd',
				autoclose: true,
				daysOfWeekDisabled : [ 0, 6 ]
			}).on('changeDate',function(e) {
				startDate = new Date(e.date.valueOf());	
				return false;
	});
			});		
			
	
	$('#chartReport').click(
			function(e) {
				$("#bookingChart").show();
				$('#gantt-chart').remove();
				$('#gantt').append('<div id="gantt-chart"></div>');
				
				loadProjects(empId,start);
				
				
	    return false;
	});
	
	$('#printReport').click(
			function(e) {
				
				
		downloadGetBookingEmployeeSummary(empId,empName,startDate);
				
				
	    return false;
	});

	 $('input.typeahead').typeahead({
			source : function(query, process) {
				return $.get('/Reslo/getEmployeeByName', {
					name : query
				}, function(data) {
					//data = $.parseJSON(data);
					return process(data);
				});
			}
		});
			
});


function call_event_handler(value) {

	
	if (value && value.empName) {
		employee = value;
		empId=employee.empId;
	}

	console.log(value);

	

}
			var dateField = $('#dateRangePicker');
			var yourDate = new Date();
			var date2 = new Date(yourDate.getFullYear(), yourDate.getMonth() + 1, 1);
			var timeDiff = Math.abs(date2.getTime() - yourDate.getTime());
			diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

		

		$('#dateRangePicker').datepicker({
			startDate : '+' + diffDays + 'd',
			format : 'yyyy/mm/dd',
			autoclose: true,
			daysOfWeekDisabled : [ 0, 6 ]
		}).on('changeDate',function(e) {
			startDate = new Date(e.date.valueOf());
			
		
});





function formatDates(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

function loadGanttChart(data) {
	  //anychart.onDocumentReady(function() {
	
	  var date = new Date();
	  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	
	
	  start = formatDates(firstDay).split('/');
	  end = formatDates(lastDay).split('/');
	
	  // The data used in this sample can be obtained from the CDN
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  //anychart.data.loadJsonFile('test.json', function(data) {
		
	    // create data tree
	    var treeData = anychart.data.tree(data, 'as-table');
	    
	    anychart.format.outputTimezone(-330);

	    // create project gantt chart
	    chart = anychart.ganttProject();
	    
	    

	    // set data for the chart
	    chart.data(treeData);

	    // set start splitter position settings
	    chart.splitterPosition(515);

	    // get chart data grid link to set column settings
	    var dataGrid = chart.dataGrid();
	    
	    // set first column settings
	    dataGrid.column(0).cellTextSettings({
	      hAlign: 'center'
	    })
	    .width(30);

	    // set second column settings
	    dataGrid.column(1)
	      .width(150)
	      .title('Project Name & CR Name')
	      .cellTextSettingsOverrider(labelTextSettingsFormatter);

	    dataGrid.column(2)
	      
	      .width(130)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .title('CR No')
	      .format(crNoColumnTextFormatter);
	    
	    // set third column settings
	    dataGrid.column(3)
	      .title('Plan Start')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(thirdColumnTextFormatter);

	    // set fourth column settings
	    dataGrid.column(4)
	      .title('Actual End')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(fourthColumnTextFormatter);
	    

	    // set container id for the chart
	    chart.container('gantt-chart');

	    // initiate chart drawing
	    chart.draw();
	    
	   // chart.fitAll();
	    var start1 = parseInt(start[0]);
	    var start2 = parseInt(start[1]);
	    var start3 = parseInt(start[2]);
	    var end1 = parseInt(end[0]);
	    var end2 = parseInt(end[1]);
	    var end3 = parseInt(end[2]);
	    // zoom chart to specified date
	  chart.zoomTo(Date.UTC(start1, start2, start3), Date.UTC(end1, end2, end3));
	 // });
	//});
	  
} 

//add bold and italic text settings to all parent items
function labelTextSettingsFormatter(label, dataItem) {
  if (dataItem.numChildren()) {
    label.fontWeight('bold').fontStyle('italic');
  }
}

//do pretty formatting for dates in third column
function thirdColumnTextFormatter(item) {
  
  var field1 = item.get('actualStart');
  
  if(field1!= null)
	  {
	
	    var actualStart = new Date(field1);
	    
	    return formatDate(actualStart);
	  } else {

	    return '';
	  }
	  
 }
  
	  
  


function fourthColumnTextFormatter(item) {
	  
	  var field1 = item.get('actualEnd');
	  
	  
	  if (field1 != null) {
	    var actualEnd = new Date(field1);
	    
	    return formatDate(actualEnd);
	  } else {

	    return '';
	  }
	  
  }
  
	 


//do pretty formatting for dates in fourth column
function nameColumnTextFormatter(item) {
  
 var field = item.get('id');
  
  pName.push(item.get('id'));
 
  return field;
}


function unique(pName) {
    
    $.each(pName, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

//do pretty formatting for dates in fourth column
function crNoColumnTextFormatter(item) {
  var field = item.get('crNo');
  return field;
}


//do pretty formatting for passed date unit
function formatDate(dateUnit) {
  if (dateUnit < 10) dateUnit = '0' + dateUnit;
  return dateUnit + '';
}

function formatDate(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

function getAllProjects(empId,start) {
	ajaxGetCall(
			{empId : empId,
				start : start},
			"/Reslo/getBookingEmployeeSummary?id=" + empId+"&startDate="+start,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#projectList').dataTable();
				oTable.fnClearTable();

				custodianBankSearchBean = data;

				$.each(custodianBankSearchBean,
								function(i) {
									var  projectName = crNo =  bookingStartDate = bookingEndDate =  booking = 'N/A';
									count++;


									if (custodianBankSearchBean[i]['id'] != null) {
										projectName = custodianBankSearchBean[i]['id'];
									}

									if (custodianBankSearchBean[i]['crNo'] != null) {
										crNo = custodianBankSearchBean[i]['crNo'];
									}

								
									if (custodianBankSearchBean[i]['actualStart'] != 'N/A') {
										Start = formatDate(custodianBankSearchBean[i]['actualStart']);
										bookingStartDate = Start;
									}

									if (custodianBankSearchBean[i]['actualEnd'] != 'N/A') {
										End = formatDate(custodianBankSearchBean[i]['actualEnd']);
										bookingEndDate = End;
									}

									if (custodianBankSearchBean[i]['allocation'] != null) {
										booking = custodianBankSearchBean[i]['allocation'];
									}
									
									
									oTable.fnAddData([ projectName, crNo, bookingStartDate, bookingEndDate, booking]);

								});

				var totalRecords =$('#projectList').DataTable().page.info().recordsTotal;
				if(totalRecords === 0)
					{
					$("#printReport").hide();
					$("#chartReport").hide();
					
					}
				else
					{
					$("#printReport").show();
					$("#chartReport").show();
					}
				
		
			
			});
}

function  loadProjects(empId,startDate) {           
	ajaxGetCall({
		empId : empId,
		startDate : startDate		
	}, "/Reslo/getBookingEmployeeSummary?id=" + empId+"&startDate="+startDate, function(data){
			loadGanttChart(data);
			
		});
}

function downloadGetBookingEmployeeSummary(empId,empName,startDate) {
	
	window.location = "/Reslo/downloadGetBookingEmployeeSummary?id="+empId+"&empName="+empName+"&date="+startDate;
			

}


function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		/* $body.removeClass("loading"); */
	});
}


