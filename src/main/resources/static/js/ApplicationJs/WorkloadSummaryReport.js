var dataSet = [ [ "", "", "", ""] ];
var userRole = null;
var startDate = null;
var endDate = null;
var workloadType = null;
var oTable = null;

$( document ).ready(function() {
	
	$('#underAllocations').prop('checked', true);
	$("#employeeList").hide();
	$("#printReport").hide();
	
	
	
	$('#previewReport').click(
			function(e) {
			
			startDate = formatDate(new Date($("#startDate").val()));
			endDate = formatDate(new Date($("#endDate").val()));
			userRole = $("#userRoleChange").val();
			
			if($('#underAllocations').is(':checked'))
				{
				workloadType = "UnderAllocation";
				}
			else
				{
				workloadType = "OverAllocation";
				}
			
			if (startDate != "" && endDate != "" && userRole != "")
             {

				workloadSummaryData(startDate,endDate,userRole,workloadType);
			  $(".dataTables_wrapper").show();
			  $("#employeeList").show();
			  
			 
		     }
				
				
				return false;
	});
	
	$('#cancelReport').click(
			function(e) {
			
			$("#startDate").val("");
			$("#endDate").val("");
			$('#underAllocations').prop('checked', true);
			$("#employeeList").hide();
			$(".dataTables_wrapper").hide();
			$("#printReport").hide();
			
			return false;
	});

	$('#printReport').click(
			function(e) {
			
				downloadWorkloadSummaryData(startDate,endDate,userRole,workloadType);
			
			return false;
	});

  
	
});



function formatDate(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

function workloadSummaryData(startDate,endDate,userRole,workloadType) {
	ajaxGetCall(
			{},
			"/Reslo/getworkloadSummaryData?startDate="+startDate+"&endDate="+endDate+"&userRole="+userRole+"&workloadType="+workloadType,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				oTable = $('#employeeList').dataTable();
				oTable.fnClearTable();
				
				custodianBankSearchBean = data;
				
		

				$.each(custodianBankSearchBean,
								function(i) {
									var employeeId = employeeName = userRole = totAllocation = 'N/A';
						
									count++;

									if (custodianBankSearchBean[i]['id'] != 'N/A') {
										employeeId = custodianBankSearchBean[i]['id'];
									}
									
									if (custodianBankSearchBean[i]['name'] != 'N/A') {
										employeeName = custodianBankSearchBean[i]['name'];
									}
									
									if (custodianBankSearchBean[i]['status'] != 'N/A') {
										userRole = custodianBankSearchBean[i]['status'];
									}

									
									if (custodianBankSearchBean[i]['allocation'] != 0) {
										totAllocation = custodianBankSearchBean[i]['allocation'];
									}
									
									
									oTable.fnAddData([ employeeId, employeeName, userRole, totAllocation]);

								});
				var totalRecords =$('#employeeList').DataTable().page.info().recordsTotal;
				if(totalRecords === 0)
					{
					$("#printReport").hide();
					}
				else
					{
					$("#printReport").show();
					}
			
			});
}



function  downloadWorkloadSummaryData(startDate,endDate,userRole,workloadType) {  

	window.location = "/Reslo/downloadWorkloadSummaryData?startDate="+startDate+"&endDate="+endDate+"&userRole="+userRole+"&workloadType="+workloadType;
	

}

function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		/* $body.removeClass("loading"); */
	});
}


