var startDate = null;
var endDate = null;
var chart = null;
var start = null;
var end = null;


//loadGanttChart(data);


function loadGanttChart(data) {
	  //anychart.onDocumentReady(function() {
	
	
	  // The data used in this sample can be obtained from the CDN
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  //anychart.data.loadJsonFile('test.json', function(data) {
		
	    // create data tree
	    var treeData = anychart.data.tree(data, 'as-table');
	    
	    anychart.format.outputTimezone(-330);

	    // create project gantt chart
	    chart = anychart.ganttProject();
	    
	    

	    // set data for the chart
	    chart.data(treeData);

	    // set start splitter position settings
	    chart.splitterPosition(460);

	    // get chart data grid link to set column settings
	    var dataGrid = chart.dataGrid();
	    
	    // set first column settings
	    dataGrid.column(0).cellTextSettings({
	      hAlign: 'center'
	    });

	    // set second column settings
	    dataGrid.column(1)
	      .width(200)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter);

	    // set third column settings
	    dataGrid.column(2)
	      .title('Plan Start')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(thirdColumnTextFormatter);

	    // set fourth column settings
	    dataGrid.column(3)
	      .title('Actual End')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(fourthColumnTextFormatter);
	    

	    // set container id for the chart
	    chart.container('gantt-chart');

	    // initiate chart drawing
	    chart.draw();
	    
	   // chart.fitAll();
	    var start1 = parseInt(start[0]);
	    var start2 = parseInt(start[1]);
	    var start3 = parseInt(start[2]);
	    var end1 = parseInt(end[0]);
	    var end2 = parseInt(end[1]);
	    var end3 = parseInt(end[2]);
	    // zoom chart to specified date
	  chart.zoomTo(Date.UTC(start1, start2, start3), Date.UTC(end1, end2, end3));
	 // });
	//});
	  
} 

function formatDate(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ];
}



$('#getGanttChart').click(
		function(e) {
			
			var date = new Date();
			var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
			var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
			
			var startDate = new Date($("#startDate").val());
			var endDate = new Date($("#endDate").val());
			
			loadProjects(startDate,endDate);
			
			start = formatDate($("#startDate").val()).split('/');
			end = formatDate($("#endDate").val()).split('/');
			return false;
		});

//add bold and italic text settings to all parent items
function labelTextSettingsFormatter(label, dataItem) {
  if (dataItem.numChildren()) {
    label.fontWeight('bold').fontStyle('italic');
  }
}

//do pretty formatting for dates in third column
function thirdColumnTextFormatter(item) {
  var field = item.get('baselineStart');

  // format base line text
  if (field) {
    var baselineStart = new Date(field);
    return formatDate(baselineStart.getUTCMonth() + 1) + '/' +
      formatDate(baselineStart.getUTCDate()) + '/' + baselineStart.getUTCFullYear();
  } else {
    // format milestone text
    var actualStart = item.get('actualStart');
    var actualEnd = item.get('actualEnd');
    if ((actualStart == actualEnd) || (actualStart && !actualEnd)) {
      var start = new Date(actualStart);
      return formatDate(start.getUTCMonth() + 1) + '/' +
        formatDate(start.getUTCDate()) + '/' + start.getUTCFullYear();
    }
    return '';
  }
}

//do pretty formatting for dates in fourth column
function fourthColumnTextFormatter(item) {
  var field = item.get('baselineEnd');
  if (field) {
    var baselineEnd = new Date(field);
    return formatDate((baselineEnd.getUTCMonth() + 1)) + '/' +
      formatDate(baselineEnd.getUTCDate()) + '/' + baselineEnd.getUTCFullYear();
  } else {
    return '';
  }
}

//do pretty formatting for passed date unit
function formatDate(dateUnit) {
  if (dateUnit < 10) dateUnit = '0' + dateUnit;
  return dateUnit + '';
}


function  loadProjects(startDate,endDate) {           
	ajaxGetCall({
		startDate : startDate,
		endDate : endDate
	}, "/Reslo/getGanttChartData", function(data){
			loadGanttChart(data.chartList);  
    	});
}

function ajaxGetCall(data, url,callback){
	
	$.ajax({
		  type: "GET",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}
