var employeeNo = null;
var employeeName = null;
var contactNo = null;
var createdUser = null;
var empRole = null;


$(document).ready(function() {




	 $('#createEmployee').click(function(e) { 
		 
		 employeeNo=$("#employeeNo").val();
		 employeeName=$("#employeeName").val();	 
		 contactNo=$("#contactNo").val(); 
		 createdUser = $("#createdUser").val(); 
		 empRole = $("#empRole").val();   
		 
		if(employeeNo!="" && employeeName!= "" && contactNo!="")
		{
    	 createEmployee(employeeNo,employeeName,contactNo,createdUser,empRole);
    	 
	    $("#employeeName").val("");
    	$("#employeeName").siblings('.label-material').removeClass('active');
		$("#employeeNo").val("");
    	$("#employeeNo").siblings('.label-material').removeClass('active');
		$("#contactNo").val("");
		$("#contactNo").siblings('.label-material').removeClass('active');
		$("#empRole").val("Select an Employee Role");
		$("#empRole").siblings('.label-material').removeClass('active');
		}	
         return false;   
});
	 
    $( ".letters" ).keypress(function(e) {
        var key = e.keyCode;
        if (key >= 48 && key <= 57) {
            e.preventDefault();
        }
    });
    
   

	
});

function createEmployee(employeeNo,employeeName,contactNo,createdUser,empRole) {
	ajaxCall({
		employeeNo : employeeNo,
		employeeName : employeeName,
		contactNo : contactNo,
		createdUser : createdUser,
		empRole : empRole,
	}, "/Reslo/Resource", function(data) {
		if (data == 1) {
			
			$("#successMsg").text("Resource Successfully Added !");
       	    $('#modelSuccess').modal();
       	    setTimeout(function() { $('#modelSuccess').modal('hide'); }, 3000);	
			
		} else {
			
			  $("#errorMsg").text("Resource Added Failed !");
	          $('#modelError').modal();
	          setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
		}
	});
}

function ajaxCall(data, url,callback){
	
	$.ajax({
		  type: "POST",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}