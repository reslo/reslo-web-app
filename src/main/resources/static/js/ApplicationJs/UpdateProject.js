var projectName = null;
var crNo = null;
var startDate = null;
var endDate = null;
var actualEndDate = null;
var actualStartDate = null;
var status = null;
var projectProgress = null;
var updateReason = null;
var createdUser = null;
var filterName = 'projectName';
var project = null;
var Start = null;
var End = null;


$( document ).ready(function() {

    
	loadData();
	$('#statusChange').hide();

	$('#updateProject').click(
			function(e) {
				
			$('#updateConfirmBox').modal();	
				
				
			});
	
$('#updateConfirm').click(function(e){
		
		$('#updateConfirmBox').modal('hide');
		
		projectName = $("#projectName").val();
		crNo = $("#crNo").val();
		if($("#actualEndDate").val()==""){
			actualEndDate = null;
		}else{
			actualEndDate = $("#actualEndDate").val();
		}
		if($("#actualStartDate").val()==""){
			actualStartDate = null;
		}else{
			actualStartDate = $("#actualStartDate").val();
		}
		if($("#projectProgress").val()==""){
			projectProgress = null;
		}
		else{
		projectProgress = $("#projectProgress").val();
		}
		updateReason = $("#updateReason").val();
		if ($("#onGoingProjectRadio").prop("checked")) {
			status = $("#status").val();
		} 
		else{
			status = "New";
		}
		createdUser = $("#createdUser").val();
		
		if ($("#onGoingProjectRadio").prop("checked")) {
			status = $("#status").val();
		} 
		
		updateProject(projectName, crNo, actualStartDate, actualEndDate, updateReason, projectProgress, status, createdUser);
		
		$("#projectName").val("");
    	$("#projectName").siblings('.label-material').removeClass('active');
		$("#crNo").val("");
    	$("#crNo").siblings('.label-material').removeClass('active');
    	$("#startDate").val("");
    	$("#startDate").siblings('.label-material').removeClass('active');
    	$("#endDate").val("");
    	$("#endDate").siblings('.label-material').removeClass('active');
    	$("#status").val("");
    	$("#status").siblings('.label-material').removeClass('active');
    	$("#actualEndDate").val("");
    	$("#actualEndDate").siblings('.label-material').removeClass('active');
    	$("#actualStartDate").val("");
    	$("#actualStartDate").siblings('.label-material').removeClass('active');
    	$("#updateReason").val("");
    	$("#updateReason").siblings('.label-material').removeClass('active');
    	$('#statusChange').hide();
    	$("#projectProgress").val("");
    	$("#projectProgress").siblings('.label-material').removeClass('active');
    	$('#onGoingProjectRadio').prop('checked', false);
		$('#newProjectRadio').prop('checked', true);
		
		return false;		
				
	});
	
	$('#cancelupdateProject').click(
			function(e) {
				
				$("#projectName").val("");
		    	$("#projectName").siblings('.label-material').removeClass('active');
				$("#crNo").val("");
		    	$("#crNo").siblings('.label-material').removeClass('active');
		    	$("#startDate").val("");
		    	$("#startDate").siblings('.label-material').removeClass('active');
		    	$("#endDate").val("");
		    	$("#endDate").siblings('.label-material').removeClass('active');
		    	$("#status").val("");
		    	$("#status").siblings('.label-material').removeClass('active');
		    	$("#actualEndDate").val("");
		    	$("#actualEndDate").siblings('.label-material').removeClass('active');
		    	$("#actualStartDate").val("");
		    	$("#actualStartDate").siblings('.label-material').removeClass('active');
		    	$("#updateReason").val("");
		    	$("#updateReason").siblings('.label-material').removeClass('active');
		    	$("#projectProgress").val("");
		    	$("#projectProgress").siblings('.label-material').removeClass('active');
		    	$('#statusChange').hide();
		    	$('#onGoingProjectRadio').prop('checked', false);
				$('#newProjectRadio').prop('checked', false);
				return false;
			});
		 
		 $('#projectName').typeahead({
				source : function(query, process) {
					filterName = 'projectName';
					return $.get('/Reslo/getProjectsByName', {
						name : query						
					}, function(data) {
						//data = $.parseJSON(data);
						return process(data);
					});
				}
			});
		 
		 $('#crNo').typeahead({
				source : function(query, process) {
					filterName = 'crNo';
					return $.get('/Reslo/getProjectsByCrNo', {
						name : query,
						projectName : project.projectName
					}, function(data) {
						//data = $.parseJSON(data);
						return process(data);
					});
				}
			});
	 
});

$('#dateRangePicker')
.datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: '01/01/2010',
    daysOfWeekDisabled : [ 0, 6 ],
    endDate: '30/12/2020'
})
.on('changeDate', function(e) {
    // Revalidate the date field
   // $('#dateRangeForm').formValidation('revalidateField', 'date');
});

$('#dateRangePicker1')
.datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: '01/01/2010',
    daysOfWeekDisabled : [ 0, 6 ],
    endDate: '30/12/2020'
}).on('changeDate', function(e) {
	   
	    
    // Revalidate the date field
   // $('#dateRangeForm').formValidation('revalidateField', 'date');
});

$('#dateRangePicker2')
.datepicker({
    format: 'yyyy/mm/dd',
    autoclose: true,
    startDate: '2010/01/01',
    daysOfWeekDisabled : [ 0, 6 ],
    endDate: '2020/12/30'
})
.on('changeDate', function(e) {
	
	var startDate = new Date(e.date.valueOf());
	startDate.setDate(startDate.getDate(new Date(e.date.valueOf())));
	$('#dateRangePicker3').datepicker('setStartDate',startDate)
	    
    // Revalidate the date field
   // $('#dateRangeForm').formValidation('revalidateField', 'date');
});

$('#dateRangePicker3')
.datepicker({
    format: 'yyyy/mm/dd',
    autoclose: true,
    startDate: '2010/01/01',
    daysOfWeekDisabled : [ 0, 6 ],
    endDate: '2020/12/30'
})
.on('changeDate', function(e) {
	    
    // Revalidate the date field
   // $('#dateRangeForm').formValidation('revalidateField', 'date');
});



$('#onGoingProjectRadio').click(
		function() {
			$('#statusChange').show();
		});

$('#newProjectRadio').click(
		function() {
			$('#statusChange').hide();
		});

	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();
	
	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	
	    return [year, month, day].join('/');
	}
		
function  loadData() {           
	ajaxGetCall({}, "/Reslo/getAllProjects", function(data){
                   
    				//if(data.errorCode == 0){   
                        var count = 0;
                        var appName = "N/A";
                        var name ="N/A";
                        var accBalance = null;
                      
                        //get the DataTable
                        var oTable = $('#projectList').dataTable(); 
                        oTable.fnClearTable();
                        
                        custodianBankSearchBean = data;

                        $.each(custodianBankSearchBean, function(i) {
                                  var projectName = actualEndDate = actualStartDate =name = reason = projectProgress= projectStatus = startDate = endDate = 'N/A';
                                  count++;
                                  
                                  if(custodianBankSearchBean[i]['projectName']!= null)
                                  { projectName = custodianBankSearchBean[i]['projectName'];}
                                  
                                  if(custodianBankSearchBean[i]['crNo']!= null)
                                  { crNo = custodianBankSearchBean[i]['crNo'];}
                                  
                                  if(custodianBankSearchBean[i]['projectStatus']!= 'N/A')
                                  { 
                                	 projectStatus = custodianBankSearchBean[i]['projectStatus'];
                                  }
                                  
                                  
                                  
                                  if(projectStatus == "New")
                            	  {
                            	   projectStatus = 'New Project';
                            	  }
                                  
                                  if(custodianBankSearchBean[i]['startDate']!= 'N/A')
                                  {   
                                	  Start = formatDate(custodianBankSearchBean[i]['startDate']);
                                	  startDate = Start;}
                                  
                                  if(custodianBankSearchBean[i]['endDate']!= 'N/A')
                                  { 
                                	  End = formatDate(custodianBankSearchBean[i]['endDate']);
                                	  endDate = End;}
                                  
                                  if(custodianBankSearchBean[i]['actualStartDate']!= null)
                                  { 
                                	  actualStart = formatDate(custodianBankSearchBean[i]['actualStartDate']);
                                	  actualStartDate = actualStart;}
                                  else{
                                	  actualStartDate = 'N/A';
                                  }
                                  
                                  if(custodianBankSearchBean[i]['actualEndDate']!= null)
                                  { 
                                	  actualEnd = formatDate(custodianBankSearchBean[i]['actualEndDate']);
                                	  actualEndDate = actualEnd;}
                                  else{
                                	  actualEndDate = 'N/A';
                                  }
                                  
                                  if(custodianBankSearchBean[i]['projectProgress']!= null)
                                  { 
                                	  projectProgress = custodianBankSearchBean[i]['projectProgress'];
                                  }
                                  
                                  if(custodianBankSearchBean[i]['reason']!= null)
                                  { reason = custodianBankSearchBean[i]['reason'];}
                                                          
                                  oTable.fnAddData([projectName,crNo,projectStatus,startDate,endDate,actualStartDate,actualEndDate,projectProgress,reason]);
                                   
                           });

                   /* }
        else{
               $("#errorMsg").text("Sorry. Requested data not available.");
                     $('#modelError').modal();
                     setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
        }*/
             });
}
       function call_event_handler(value) {
	
	   project = value;
	  
	   //console.log(value);
	   
	   //clear the place holder values
	   $("#crNo").siblings('.label-material').addClass('active');
	   
	   //disable the fileds
	   $("#startDate").prop("disabled", true);
	   $("#endDate").prop("disabled", true);
	   
	   Start = formatDate(project.startDate);
	   End = formatDate(project.endDate);
	   var startDate = new Date(End);
	    
	   $('#dateRangePicker3').datepicker('setStartDate',startDate);
	   actualEndDate = formatDate(project.actualEndDate);
	   actualStartDate = formatDate(project.actualStartDate);
	   
	

	   
	   if($("#crNo").val()!=null && $("#crNo").val()!='')
	   {
	   $("#crNo").val(project.crNo);
	   $("#startDate").val(Start);
	   $("#endDate").val(End);
	   if(project.projectProgress!=null)
	{
	   $("#projectProgress").siblings('.label-material').addClass('active');
	   $("#projectProgress").val(project.projectProgress);
	   }
	   if(actualEndDate!='1970/01/01')
	   {
	   $("#actualEndDate").val(actualEndDate);
	   }
	   
	   if(actualStartDate!='1970/01/01')
	   {
	   $("#actualStartDate").val(actualStartDate);
	   }
	  
	   
	   var textToFind = project.projectStatus;
	   if(textToFind=='New')
	   	{
	   	$("#newProjectRadio").prop("checked", true)
	   	}
	   else{
	   	   $("#onGoingProjectRadio").prop("checked", true)
	   	   $('#statusChange').show();
	   	   var dd = document.getElementById('status');
	   	   for (var i = 0; i < dd.options.length; i++) {
	   	       if (dd.options[i].text === textToFind) {
	   	           dd.selectedIndex = i;
	   	           break;
	   	       }
	   	   }
	   }
	   
	   $("#updateReason").siblings('.label-material').addClass('active');
	   $("#updateReason").val(project.reason);
	   
	   
	  /* $('#status option').eq(project.projectStatus).prop('selected', true);*/
	   }
	   
	   $("#projectName").keyup(function() {

		    if (!this.value) {
		    	$("#crNo").val("");
		    	$("#crNo").siblings('.label-material').removeClass('active');
		    	$("#startDate").val("");
		    	$("#startDate").siblings('.label-material').removeClass('active');
		    	$("#endDate").val("");
		    	$("#endDate").siblings('.label-material').removeClass('active');
		    	$("#status").val("");
		    	$("#status").siblings('.label-material').removeClass('active');
		    	$("#actualEndDate").val("");
		    	$("#actualEndDate").siblings('.label-material').removeClass('active');
		    	$("#actualStartDate").val("");
		    	$("#actualStartDate").siblings('.label-material').removeClass('active');
		    	$("#updateReason").val("");
		    	$("#updateReason").siblings('.label-material').removeClass('active');
		    	$('#statusChange').hide();
		    	$('#onGoingProjectRadio').prop('checked', false);
				$('#newProjectRadio').prop('checked', false);
				$("#projectProgress").val("");
		    	$("#projectProgress").siblings('.label-material').removeClass('active');
				
		    }

		});
	   
	}


function updateProject(projectName, crNo, actualStartDate, actualEndDate, updateReason,projectProgress, status, createdUser) {
	
	var boose = {
			projectName : project.projectName,
			crNo : project.crNo,
			actualStartDate : actualStartDate,
			actualEndDate : actualEndDate,
			updateReason : updateReason,
			projectProgress : projectProgress,
			status : status,
			createdUser : createdUser,
		};
	if(actualEndDate == null){
		delete boose['actualEndDate']; // Removes json.actualEndDate from the dictionary.
	}
	
	if(actualStartDate == null){
		delete boose['actualStartDate']; // Removes json.actualStartDate from the dictionary.
	}
	
	ajaxCall(boose, "/Reslo/updateProject", function(data) {
		if (data == 2) {
			
			loadData();
			$("#successMsg").text("Project Successfully Updated !");
       	    $('#modelSuccess').modal();
       	    setTimeout(function() { $('#modelSuccess').modal('hide'); }, 3000);	

			
		} else {
			
			  $("#errorMsg").text("Project Update Failed  !");
	          $('#modelError').modal();
	          setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
		}
	});
}


function ajaxCall(data, url,callback){
	
	$.ajax({
		  type: "POST",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	//$body.removeClass("loading");
	  	});
}


function ajaxGetCall(data, url,callback){
	
	$.ajax({
		  type: "GET",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}