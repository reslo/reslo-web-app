var employeeNo = null;
var employeeName = null;
var contactNo = null;
var createdUser = null;
var empRole = null;
var filterName = 'empName';

$( document ).ready(function() {

	 $("#empRole").prop('disabled', 'disabled');
	 $('#removeEmployee').click(function(e) { 			        
		 if(employeeNo!="" && employeeName!= "" && contactNo!="" && empRole!="")
			{
		 $('#deleteConfirmBox').modal();
			}
});
	 
	 $('#deleteConfirm').click(function(e){
			
			$('#deleteConfirmBox').modal('hide');
			
			employeeNo=$("#employeeNo").val();
			createdUser = $("#createdUser").val(); 
			   
			removeEmployee(employeeNo,createdUser);
			 
		    $("#employeeName").val("");
	    	$("#employeeName").siblings('.label-material').removeClass('active');
			$("#employeeNo").val("");
	    	$("#employeeNo").siblings('.label-material').removeClass('active');
			$("#contactNo").val("");
			$("#contactNo").siblings('.label-material').removeClass('active');
			$("#empRole").val("");
			$("#empRole").siblings('.label-material').removeClass('active');
			 
	      return false;
					
		});
	 
	 $('#updateEmployee').click(
				function(e) {
					if(employeeNo!="" && employeeName!= "" && contactNo!="" && empRole!="")
					{	
				$('#updateConfirmBox').modal();
					}
				});
	 
	 $('#cancelEmployee').click(
				function(e) {
					
		    $("#employeeName").val("");
	    	$("#employeeName").siblings('.label-material').removeClass('active');
			$("#employeeNo").val("");
	    	$("#employeeNo").siblings('.label-material').removeClass('active');
			$("#contactNo").val("");
			$("#contactNo").siblings('.label-material').removeClass('active');
			$("#empRole").val("");
			$("#empRole").siblings('.label-material').removeClass('active');
					
			});
	 
	 
	 $('#updateConfirm').click(function(e){
			
			$('#updateConfirmBox').modal('hide');
			
			employeeNo = $("#employeeNo").val();
			employeeName = $("#employeeName").val();
			contactNo = $("#contactNo").val();
			empRole = $("#empRole").val();
			createdUser = $("#createdUser").val();
			
			updateResource(employeeNo,employeeName,contactNo,empRole,createdUser);
			
			$("#employeeName").val("");
	    	$("#employeeName").siblings('.label-material').removeClass('active');
			$("#employeeNo").val("");
	    	$("#employeeNo").siblings('.label-material').removeClass('active');
			$("#contactNo").val("");
			$("#contactNo").siblings('.label-material').removeClass('active');
			$("#empRole").val("");
			$("#empRole").siblings('.label-material').removeClass('active');
			
			return false;	
					
		});
	 
	 
	 $('input.typeahead').typeahead({
			source : function(query, process) {
				return $.get('/Reslo/getEmployeeByName', {
					name : query
				}, function(data) {
					//data = $.parseJSON(data);
					return process(data);
				});
			}
		});

	
});

function removeEmployee(employeeNo,createdUser) {
	ajaxCall({
		employeeNo : employeeNo,
		createdUser : createdUser,
		
	}, "/Reslo/removeResource", function(data) {
		if (data == 0) {
			 
			$("#successMsg").text("Resource Successfully Deleted !");
        	 $('#modelSuccess').modal();
        	 setTimeout(function() { $('#modelSuccess').modal('hide'); }, 3000);
			
		} else {
			
			  $("#errorMsg").text("Resource Deleted Failed  !");
	          $('#modelError').modal();
	          setTimeout(function() { $('#modelError').modal('hide'); }, 3000);

		}
	});
}

function updateResource(employeeNo,employeeName,contactNo,empRole,createdUser) {
	ajaxCall({
		employeeNo : employeeNo,
		employeeName : employeeName,
		contactNo : contactNo,
		empRole : empRole,
		createdUser : createdUser,
	}, "/Reslo/updateResource", function(data) {
		if (data == 2) {
		
			 $("#successMsg").text("Resource Successfully Updated !");
        	 $('#modelSuccess').modal();
        	 setTimeout(function() { $('#modelSuccess').modal('hide'); }, 3000);

			
		} else {
			
			  $("#errorMsg").text("Resource Update Failed  !");
	          $('#modelError').modal();
	          setTimeout(function() { $('#modelError').modal('hide'); }, 3000);

		}
	});
}

function ajaxCall(data, url,callback){
	
	$.ajax({
		  type: "POST",
		  url: url,
		  data: data
		})
		.done(function( msg ) {
		    callback(msg);
		})
		.fail(function(err) {
	    	console.log( "error : " +err);
	    	$body.removeClass("loading");
	  	});
}