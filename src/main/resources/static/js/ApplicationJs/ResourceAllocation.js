var projectName = null;
var crNo = null;
var employeeName = null;
var empId = null;
var allocationStartDate = null;
var allocationEndDate = null;
var allocationDate = null;
var noOfDays = null;
var allocation = null;
var Start = null;
var End = null;
var actualStartDate = null;
var actualEndDate = null;
var employee = null;
var project = null;
var filterName = null;
var getStartDate = null;
var dateArray = null;
var holidayArray = null;
var diffDays = null;
var dataSet = [ [ "", "", "", "", "", "", ""] ];
var custodianBankSearchBean = null;

function dateDifference(start, end) {

	// Copy date objects so don't modify originals
	var s = new Date(start);
	var e = new Date(end);

	// Set time to midday to avoid dalight saving and browser quirks
	s.setHours(12, 0, 0, 0);
	e.setHours(12, 0, 0, 0);

	// Get the difference in whole days
	var totalDays = Math.round((e - s) / 8.64e7);

	// Get the difference in whole weeks
	var wholeWeeks = totalDays / 7 | 0;

	// Estimate business days as number of whole weeks * 5
	var days = wholeWeeks * 5;

	// If not even number of weeks, calc remaining weekend days
	if (totalDays % 7) {
		s.setDate(s.getDate() + wholeWeeks * 7);

		while (s < e) {
			s.setDate(s.getDate() + 1);

			// If day isn't a Sunday or Saturday, add to business days
			if (s.getDay() != 0 && s.getDay() != 6) {
				++days;
			}
		}
	}
	return days + 1;
}

$(document).ready(
		function() {


			$('#dateRangePicker1 > .form-control').prop('disabled', true);
			$('#headingBooking').hide();
			$('#headingAllocation').show();
			$('#bookingStart').hide();
			$('#allocationStart').show();
			$('#bookingEnd').hide();
			$('#allocationEnd').show();
			$('#bookingAmount').hide();
			$('#allocationAmount').show();
			$('#projectActualStartDateLBL').hide();
			$('#projectStartDateLBL').hide();
			$('#projectEndDateLBL').hide();
			$('#projectActualEndDateLBL').hide();
			$('#allocationTable').hide();
			$("#addAllocation").prop("disabled", false);
			$("#updateAllocation").hide();
			$("#deleteAllocation").hide();
			$('#assignedAllocation').prop('checked', true);

			loadHolidays();
			
			
			$('#addAllocation').click(
					function(e) {

						projectName = $("#projectName").val();
						crNo = $("#crNo").val();
						empId = empId;
						allocation = $("#allocation").val();
						createdUser = $("#createdUser").val();
						allocationDate = dateArray;

						if ($("#assignedAllocation").prop("checked")) {
							addAllocation(projectName, crNo, empId,
									allocationDate, allocation, createdUser);
						} else {
							addBooking(projectName, crNo, empId,
									allocationDate, allocation, createdUser);
						}

						$("#projectName").val("");
						$("#projectName").siblings('.label-material')
								.removeClass('active');
						$("#crNo").val("");
						$("#crNo").siblings('.label-material').removeClass(
								'active');
						$("#projectStartDate").val("");
						$("#projectStartDate").siblings('.label-material')
								.removeClass('active');
						$("#projectEndDate").val("");
						$("#projectEndDate").siblings('.label-material')
								.removeClass('active');
						$("#projectActualStartDate").val("");
						$("#projectActualEndDate").val("");
						$("#projectActualEndDate").siblings('.label-material')
								.removeClass('active');
						$("#employeeName").val("");
						$("#employeeName").siblings('.label-material')
								.removeClass('active');
						$("#allocationTable").hide();
						$("#allocationStartDate").val("");
						$("#allocationStartDate").siblings('.label-material')
								.removeClass('active');
						$("#allocationEndDate").val("");
						$("#allocationEndDate").siblings('.label-material')
								.removeClass('active');
						$("#noOfDays").val("");
						$("#noOfDays").siblings('.label-material').removeClass(
								'active');
						$("#allocation").val("");
						$("#allocation").siblings('.label-material')
								.removeClass('active');
						$('#projectStartDateLBL').hide();
						$('#projectEndDateLBL').hide();
						$('#projectActualStartDateLBL').hide();
						$('#projectActualEndDateLBL').hide();
						$("#addAllocation").prop("disabled", false);

						return false;
					});

			$('#updateAllocation').click(function(e) {
				$('#updateConfirmBox').modal();
			});

			$('#updateConfirm').click(
					function(e) {

						$('#updateConfirmBox').modal('hide');

						projectName = $("#projectName").val();
						crNo = $("#crNo").val();
						empId = empId;
						allocation = $("#allocation").val();
						createdUser = $("#createdUser").val();
						allocationDate = dateArray;

						if ($("#assignedAllocation").prop("checked")) {
							updateAllocation(projectName, crNo, empId,
									allocationDate, allocation, createdUser);
						} else {
							updateAllocation(projectName, crNo, empId,
									allocationDate, allocation, createdUser);
						}

						$("#projectName").val("");
						$("#projectName").siblings('.label-material')
								.removeClass('active');
						$("#crNo").val("");
						$("#crNo").siblings('.label-material').removeClass(
								'active');
						$("#projectStartDate").val("");
						$("#projectStartDate").siblings('.label-material')
								.removeClass('active');
						$("#projectEndDate").val("");
						$("#projectEndDate").siblings('.label-material')
								.removeClass('active');
						$("#projectActualEndDate").val("");
						$("#projectActualStartDate").val("");
						$("#projectActualEndDate").siblings('.label-material')
								.removeClass('active');
						$("#employeeName").val("");
						$("#employeeName").siblings('.label-material')
								.removeClass('active');
						$("#allocationTable").hide();
						$("#allocationStartDate").val("");
						$("#allocationStartDate").siblings('.label-material')
								.removeClass('active');
						$("#allocationEndDate").val("");
						$("#allocationEndDate").siblings('.label-material')
								.removeClass('active');
						$("#noOfDays").val("");
						$("#noOfDays").siblings('.label-material').removeClass(
								'active');
						$("#allocation").val("");
						$("#allocation").siblings('.label-material')
								.removeClass('active');
						$('#projectStartDateLBL').hide();
						$('#projectEndDateLBL').hide();
						$('#projectActualEndDateLBL').hide()
						$('#projectActualStartDateLBL').hide();;
						$("#addAllocation").prop("disabled", false);
						$("#updateAllocation").hide();
						$("#deleteAllocation").hide();

						return false;

					});

			$('#deleteAllocation').click(function(e) {

				$('#deleteConfirmBox').modal();

			});

			$('#deleteConfirm').click(
					function(e) {

						$('#deleteConfirmBox').modal('hide');

						projectName = $("#projectName").val();
						crNo = $("#crNo").val();
						empId = empId;
						allocation = $("#allocation").val();
						createdUser = $("#createdUser").val();
						allocationDate = dateArray;

						deleteAllocation(projectName, crNo, empId, allocationDate, allocation, createdUser);

						$("#projectName").val("");
						$("#projectName").siblings('.label-material')
								.removeClass('active');
						$("#crNo").val("");
						$("#crNo").siblings('.label-material').removeClass(
								'active');
						$("#projectStartDate").val("");
						$("#projectStartDate").siblings('.label-material')
								.removeClass('active');
						$("#projectEndDate").val("");
						$("#projectEndDate").siblings('.label-material')
								.removeClass('active');
						$("#projectActualEndDate").val("");
						$("#projectActualStartDate").val("");
						$("#projectActualEndDate").siblings('.label-material')
								.removeClass('active');
						$("#employeeName").val("");
						$("#employeeName").siblings('.label-material')
								.removeClass('active');
						$("#allocationTable").hide();
						$("#allocationStartDate").val("");
						$("#allocationStartDate").siblings('.label-material')
								.removeClass('active');
						$("#allocationEndDate").val("");
						$("#allocationEndDate").siblings('.label-material')
								.removeClass('active');
						$("#noOfDays").val("");
						$("#noOfDays").siblings('.label-material').removeClass(
								'active');
						$("#allocation").val("");
						$("#allocation").siblings('.label-material')
								.removeClass('active');
						$('#projectStartDateLBL').hide();
						$('#projectEndDateLBL').hide();
						$('#projectActualEndDateLBL').hide();
						$('#projectActualStartDateLBL').hide();
						$("#addAllocation").prop("disabled", false);
						$("#updateAllocation").hide();
						$("#deleteAllocation").hide();

						return false;

					});

			$('#cancelAllocation').click(
					function(e) {
						$('#dateRangePicker1 > .form-control').prop('disabled', true);
						$('#addAllocation').show();
						$("#projectName").val("");
						$("#projectName").siblings('.label-material')
								.removeClass('active');
						$("#crNo").val("");
						$("#crNo").siblings('.label-material').removeClass(
								'active');
						$("#projectStartDate").val("");
						$("#projectStartDate").siblings('.label-material')
								.removeClass('active');
						$("#projectEndDate").val("");
						$("#projectEndDate").siblings('.label-material')
								.removeClass('active');
						$("#projectActualEndDate").val("");
						$("#projectActualEndDate").siblings('.label-material')
								.removeClass('active');
						$("#projectActualStartDate").val("");
						$("#projectActualStartDate").siblings('.label-material')
								.removeClass('active');
						$("#employeeName").val("");
						$("#employeeName").siblings('.label-material')
								.removeClass('active');
						$('#projectStartDateLBL').hide();
						$('#projectEndDateLBL').hide();
						$('#projectActualEndDateLBL').hide();
						$('#projectActualStartDateLBL').hide();
						$("#allocationTable").hide();
						$("#allocationStartDate").val("");
						$("#allocationStartDate").siblings('.label-material')
								.removeClass('active');
						$("#allocationEndDate").val("");
						$("#allocationEndDate").siblings('.label-material')
								.removeClass('active');
						$("#noOfDays").val("");
						$("#noOfDays").siblings('.label-material').removeClass(
								'active');
						$("#allocation").val("");
						$("#allocation").siblings('.label-material')
								.removeClass('active');
						$("#addAllocation").prop("disabled", false);
						$("#updateAllocation").hide();
						$("#deleteAllocation").hide();
						return false;
					});

			$('#checkCurrentAllocation').click(function(e) {
				empId = employee.empId;
				$(".dataTables_wrapper").show();
				$("#allocationTable").show();
				loadProjectData(empId);

				return false;
			});

			$('#projectName').typeahead({
				source : function(query, process) {
					filterName = 'projectName';
					return $.get('/Reslo/getProjectsByName', {
						name : query
					}, function(data) {
						// data = $.parseJSON(data);
						return process(data);
					});
				}
			});

			$('#crNo').typeahead({
				source : function(query, process) {
					filterName = 'crNo';
					return $.get('/Reslo/getProjectsByCrNo', {
						name : query,
						projectName : project.projectName
					}, function(data) {
						// data = $.parseJSON(data);
						return process(data);
					});
				}
			});

			$('#employeeName').typeahead({
				source : function(query, process) {
					filterName = 'empName';
					return $.get('/Reslo/getEmployeeByName', {
						name : query
					}, function(data) {
						// data = $.parseJSON(data);
						return process(data);
					});
				}
			});

				$('#dateRangePicker').datepicker({
				todayHighlight : true,
				startDate : '01/01/2010',
				endDate : '30/12/2020',
				format : 'dd/mm/yyyy',
				autoclose: true,
				defaultDate : new Date(),
				startDate : '-0m',
				daysOfWeekDisabled : [ 0, 6 ]}).on('changeDate',function(e) {
					
					$('#dateRangePicker1').prop('disabled', false);
					$('#dateRangePicker1 > .form-control').prop('disabled', false);
					startDate = new Date(e.date.valueOf());
					startDate.setDate(startDate.getDate(new Date(e.date.valueOf())));
					$('#dateRangePicker1').datepicker('setStartDate',startDate);
					allocationStartDate = e.date;
					getStartDate = formatDate(allocationStartDate);
					});
			
			$('#dateRangePicker1').datepicker({
				defaultDate : null,
				startDate : '-0m',
				autoclose: true,
				daysOfWeekDisabled : [ 0, 6 ],
				format : 'dd/mm/yyyy',
				startDate : '01/01/2010',
				endDate : '30/12/2020'
			}).on('changeDate', function(e) {
				allocationEndDate = e.date;
				dateArray = getDates(allocationStartDate, allocationEndDate);
				dateArray = filterWeekends(dateArray);
				dateArray = filterHolidays(dateArray);
				

				$("#noOfDays").siblings('.label-material').addClass('active');
				$("#noOfDays").val(dateArray.length);

			});



			$("#dateRangePicker").keyup(
					function() {
						$("#noOfDays").val("");
						$("#noOfDays").siblings('.label-material').removeClass(
								'active');

					});

			$("#dateRangePicker1").keyup(
					function() {

						$("#noOfDays").val("");
						$("#noOfDays").siblings('.label-material').removeClass(
								'active');

					});

			table = $('#allocationList').DataTable({
				// data: dataSet,
				columns : [ {
					title : "Project Name"
				}, {
					title : "CR No"
				}, {
					title : "Allocation Status"
				}, {
					title : "Allocation Start Date"
				}, {
					title : "Allocation End Date"
				},  {
					title : "Allocation"
				}, {
					title : "Action"
				} ],
				"columnDefs" : [ {
					"targets" : -1,
					"data" : null,
					"defaultContent" : "<button>Edit</button>"
				} ]
			});

			table.clear().draw();

			$('#allocationList tbody').on(
					'click',
					'button',
					function() {

						var data = table.row($(this).parents('tr')).data();
						var projectName = data[0];
						var crNo = data[1];
						var projectStartDate = data[3];
						var projectEndDate = data[4];
						var projectActualEndDate = data[5];

						$('#addAllocation').hide();
						$("#projectName").siblings('.label-material').addClass(
								'active');
						$("#projectName").val(projectName);
						$("#crNo").siblings('.label-material').addClass(
								'active');
						$("#crNo").val(crNo);
						$('#projectStartDateLBL').show();
						$('#projectEndDateLBL').show();
						$('#projectActualEndDateLBL').show();
						$("#projectStartDate").val("");
						$("#projectStartDate").val("");
						$("#projectStartDate").val("");
						$("#projectStartDate").val(projectStartDate);
						$("#projectEndDate").val(projectEndDate);
						$("#projectActualEndDate").val(projectActualEndDate);

						editAllocationData(projectName, crNo);

						return false;

						// table.row( $(this).parents('tr') ).remove().draw();
					});

		});

$("input[name='group1']").click(function() {
	var dateField = $('#dateRangePicker');
	var yourDate = new Date();
	var date2 = new Date(yourDate.getFullYear(), yourDate.getMonth() + 1, 1);
	var timeDiff = Math.abs(date2.getTime() - yourDate.getTime());
	diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

});

$('#assignedAllocation')
		.click(
				function() {
					$('#headingBooking').hide();
					$('#headingAllocation').show();
					$('#bookingStart').hide();
					$('#allocationStart').show();
					$('#bookingEnd').hide();
					$('#allocationEnd').show();
					$('#bookingAmount').hide();
					$('#allocationAmount').show();
					$('#dateRangePicker1 > .form-control').prop('disabled', true);
					$('#addAllocation').show();
					$("#projectName").val("");
					$("#projectName").siblings('.label-material').removeClass(
							'active');
					$("#crNo").val("");
					$("#crNo").siblings('.label-material')
							.removeClass('active');
					$("#projectStartDate").val("");
					$("#projectStartDate").siblings('.label-material')
							.removeClass('active');
					$("#projectEndDate").val("");
					$("#projectEndDate").siblings('.label-material')
							.removeClass('active');
					$("#projectActualEndDate").val("");
					$("#projectActualStartDate").val("");
					$("#projectActualEndDate").siblings('.label-material')
							.removeClass('active');
					$("#employeeName").val("");
					$("#employeeName").siblings('.label-material').removeClass(
							'active');
					$('#projectStartDateLBL').hide();
					$('#projectEndDateLBL').hide();
					$('#projectActualStartDateLBL').hide();
					$('#projectActualEndDateLBL').hide();
					$("#allocationTable").hide();
					$("#allocationStartDate").val("");
					$("#allocationStartDate").siblings('.label-material')
							.removeClass('active');
					$("#allocationEndDate").val("");
					$("#allocationEndDate").siblings('.label-material')
							.removeClass('active');
					$("#noOfDays").val("");
					$("#noOfDays").siblings('.label-material').removeClass(
							'active');
					$("#allocation").val("");
					$("#allocation").siblings('.label-material').removeClass(
							'active');
					$("#addAllocation").prop("disabled", false);
					$("#updateAllocation").hide();
					$("#deleteAllocation").hide();

					$('#dateRangePicker').datepicker('remove');
					$('#dateRangePicker').datepicker({
						todayHighlight : true,
						format : 'dd/mm/yyyy',
						defaultDate : new Date(),
						startDate : '-0m',
						daysOfWeekDisabled : [ 0, 6 ]
					});
					$('#dateRangePicker1').datepicker({
						defaultDate : null,
						startDate : '-0m',
						daysOfWeekDisabled : [ 0, 6 ],
						format : 'dd/mm/yyyy',
						startDate : '01/01/2010',
						endDate : '30/12/2020'
					});
					$('#dateRangePicker').datepicker('update');
				});

function truncateDate(date) {
	return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

$('#assignedBooking')
		.click(
				function() {
					
					$('#headingAllocation').hide();
					$('#headingBooking').show();
					$('#allocationStart').hide();
					$('#bookingStart').show();
					$('#allocationEnd').hide();
					$('#bookingEnd').show();
					$('#allocationAmount').hide();
					$('#bookingAmount').show();
					$('#dateRangePicker1 > .form-control').prop('disabled', true);
					$('#addAllocation').show();
					$("#projectName").val("");
					$("#projectName").siblings('.label-material').removeClass(
							'active');
					$("#crNo").val("");
					$("#crNo").siblings('.label-material')
							.removeClass('active');
					$("#projectStartDate").val("");
					$("#projectStartDate").siblings('.label-material')
							.removeClass('active');
					$("#projectEndDate").val("");
					$("#projectEndDate").siblings('.label-material')
							.removeClass('active');
					$("#projectActualEndDate").val("");
					$("#projectActualEndDate").siblings('.label-material')
							.removeClass('active');
					$("#employeeName").val("");
					$("#employeeName").siblings('.label-material').removeClass(
							'active');
					$('#projectStartDateLBL').hide();
					$('#projectEndDateLBL').hide();
					$('#projectActualStartDateLBL').hide();
					$('#projectActualEndDateLBL').hide();
					$("#allocationTable").hide();
					$("#allocationStartDate").val("");
					$("#allocationStartDate").siblings('.label-material')
							.removeClass('active');
					$("#allocationEndDate").val("");
					$("#allocationEndDate").siblings('.label-material')
							.removeClass('active');
					$("#noOfDays").val("");
					$("#noOfDays").siblings('.label-material').removeClass(
							'active');
					$("#allocation").val("");
					$("#allocation").siblings('.label-material').removeClass(
							'active');
					$("#addAllocation").prop("disabled", false);
					$("#updateAllocation").hide();
					$("#deleteAllocation").hide();

					$('#dateRangePicker').datepicker('remove');
					$('#dateRangePicker').datepicker({
						startDate : '+' + diffDays + 'd',
						format : 'dd/mm/yyyy',
						daysOfWeekDisabled : [ 0, 6 ]
					});
					$('#dateRangePicker1').datepicker({
						defaultDate : null,
						startDate : '-0m',
						daysOfWeekDisabled : [ 0, 6 ],
						format : 'dd/mm/yyyy',
						startDate : '01/01/2010',
						endDate : '30/12/2020'
					});
					$('#dateRangePicker').datepicker('update');
				});

function formatDate(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

function formatDate2(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('-');
}

function formatDate3(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

Date.prototype.addDays = function(days) {
	var date = new Date(this.valueOf())
	date.setDate(date.getDate() + days);
	return date;
}

function getDates(startDate, stopDate) {
	dateArray = new Array();
	var currentDate = startDate;
	while (currentDate <= stopDate) {
		dateArray.push(new Date(currentDate));
		currentDate = currentDate.addDays(1);
	}
	return dateArray;
}

function filterWeekends(dateArray) {
	// $.each(dateArray, function( index, value ) {
	for (i = 0; i < dateArray.length; i++) {
		if (dateArray[i] != undefined
				&& (dateArray[i].toString().includes('Sat') || dateArray[i]
						.toString().includes('Sun'))) {
			dateArray.splice(i, 1);
			i--;
		}
	}
	return dateArray;
}

function filterHolidays(dateArray) {
	// holidayArray
	for (i = 0; i < dateArray.length; i++) {
		index = holidayArray.toString().indexOf(dateArray[i].toString());
		if (index > -1) {
			dateArray.splice(i, 1);
			i--;
		}
	}
	return dateArray;
}

function call_event_handler(value) {

	// check object is employee
	// if not take as project
	if (value && value.projectName) {
		project = value;
	}
	if (value && value.empName) {
		employee = value;
	}

	

	// clear the place holder values
	$("#crNo").siblings('.label-material').addClass('active');

	// disable the fileds
	Start = formatDate(project.startDate);
	End = formatDate(project.endDate);
	actualEndDate = formatDate(project.actualEndDate);
	actualStartDate = formatDate(project.actualStartDate);

	if ($("#crNo").val() != null && $("#crNo").val() != ''
			&& $("#projectStartDate").val() == '') {
		$('#projectStartDateLBL').show();
		$('#projectEndDateLBL').show();
		$('#projectActualEndDateLBL').show();
		$('#projectActualStartDateLBL').show();
		$("#projectStartDate").val("");
		$("#projectStartDate").val(Start);
		$("#projectEndDate").val("");
		$("#projectEndDate").val(End);
		$("#projectActualEndDate").val("");
		
		if (actualEndDate != '1970/01/01') {
			$("#projectActualEndDate").val(actualEndDate);
		} else {
			$("#projectActualEndDate").val('    -');

		}
		if (actualStartDate != '1970/01/01') {
			$("#projectActualStartDate").val(actualStartDate);
		} else {
			$("#projectActualStartDate").val('    -');

		}

	}

	if ($("#projectStartDate").val() != ''
			&& $("#projectStartDate").val() != null) {
		$("#employeeName").siblings('.label-material').addClass('active');
		$("#employeeName").val($("#employeeName").val());
	}

	$("#projectName").keyup(
			function() {

				if (!this.value) {
					$("#crNo").val("");
					$("#crNo").siblings('.label-material')
							.removeClass('active');
					$("#projectStartDate").val("");
					$("#projectStartDate").siblings('.label-material')
							.removeClass('active');
					$("#projectEndDate").val("");
					$("#projectEndDate").siblings('.label-material')
							.removeClass('active');
					$("#projectActualEndDate").val("");
					$("#projectActualEndDate").siblings('.label-material')
							.removeClass('active');
					$("#projectActualStartDate").val("");
					$("#projectActualStartDate").siblings('.label-material')
							.removeClass('active');
					$("#allocationTable").hide();
					$("#employeeName").val("");
					$("#employeeName").siblings('.label-material').removeClass(
							'active');
					$("#allocationStartDate").val("");
					$("#allocationStartDate").siblings('.label-material')
							.removeClass('active');
					$("#allocationEndDate").val("");
					$("#allocationEndDate").siblings('.label-material')
							.removeClass('active');
					$("#noOfDays").val("");
					$("#noOfDays").siblings('.label-material').removeClass(
							'active');
					$("#allocation").val("");
					$("#allocation").siblings('.label-material').removeClass(
							'active');
					$("#employeeName").siblings('.label-material').removeClass(
							'active');
					$('#projectStartDateLBL').hide();
					$('#projectEndDateLBL').hide();
					$('#projectActualStartDateLBL').hide();
					$('#projectActualEndDateLBL').hide();
				}

			});

	$("#crNo").keyup(
			function() {

				if (!this.value) {
					$("#projectStartDate").val("");
					$("#projectStartDate").siblings('.label-material')
							.removeClass('active');
					$("#projectEndDate").val("");
					$("#projectEndDate").siblings('.label-material')
							.removeClass('active');
					$("#projectActualEndDate").val("");
					$("#projectActualEndDate").siblings('.label-material')
							.removeClass('active');
					$("#allocationTable").hide();
					$("#allocationStartDate").val("");
					$("#employeeName").val("");
					$("#employeeName").siblings('.label-material').removeClass(
							'active');
					$("#allocationStartDate").siblings('.label-material')
							.removeClass('active');
					$("#allocationEndDate").val("");
					$("#allocationEndDate").siblings('.label-material')
							.removeClass('active');
					$("#noOfDays").val("");
					$("#noOfDays").siblings('.label-material').removeClass(
							'active');
					$("#allocation").val("");
					$("#allocation").siblings('.label-material').removeClass(
							'active');
					$("#employeeName").siblings('.label-material').removeClass(
							'active');
					$('#projectStartDateLBL').hide();
					$('#projectEndDateLBL').hide();
					$('#projectActualEndDateLBL').hide();
				}

			});
	
	$("#employeeName").keyup(
			function() {

				if (!this.value) {
					
					$("#allocationTable").hide();
					$("#allocationStartDate").val("");
					$("#allocationStartDate").siblings('.label-material')
							.removeClass('active');
					$("#allocationEndDate").val("");
					$("#allocationEndDate").siblings('.label-material')
							.removeClass('active');
					$("#noOfDays").val("");
					$("#noOfDays").siblings('.label-material').removeClass(
							'active');
					$("#allocation").val("");
					$("#allocation").siblings('.label-material').removeClass(
							'active');
					$("#updateAllocation").hide();
					$("#deleteAllocation").hide();
					$("#addAllocation").show();
					
				}

			});

}

function addAllocation(projectName, crNo, empId, allocationDate, allocation,
		createdUser) {
	ajaxCall({
		projectName : projectName,
		crNo : crNo,
		empId : empId,
		allocationDate : allocationDate,
		allocation : allocation,
		createdUser : createdUser,
	}, "/Reslo/Allocation", function(data) {
		if (data == 1) {

			$("#successMsg").text("Allocation Successfully Added !");
			$('#modelSuccess').modal();
			setTimeout(function() {
				$('#modelSuccess').modal('hide');
			}, 3000);

		} else {

			$("#errorMsg").text("Allocation Added Failed !");
			$('#modelError').modal();
			setTimeout(function() {
				$('#modelError').modal('hide');
			}, 3000);
		}
	});
}

function addBooking(projectName, crNo, empId, allocationDate, allocation,
		createdUser) {
	ajaxCall({
		projectName : projectName,
		crNo : crNo,
		empId : empId,
		allocationDate : allocationDate,
		allocation : allocation,
		createdUser : createdUser,
	}, "/Reslo/Booking", function(data) {
		if (data == 1) {

			$("#successMsg").text("Booking Successfully Added !");
			$('#modelSuccess').modal();
			setTimeout(function() {
				$('#modelSuccess').modal('hide');
			}, 3000);

		} else {

			$("#errorMsg").text("Booking Added Failed !");
			$('#modelError').modal();
			setTimeout(function() {
				$('#modelError').modal('hide');
			}, 3000);
		}
	});
}

function updateAllocation(projectName, crNo, empId, allocationDate, allocation,
		createdUser) {
	ajaxCall({
		projectName : projectName,
		crNo : crNo,
		empId : empId,
		allocationDate : allocationDate,
		allocation : allocation,
		createdUser : createdUser,
	}, "/Reslo/updateAllocation", function(data) {
		if (data == 0) {
			$("#successMsg").text("Allocation Successfully Updated !");
			$('#modelSuccess').modal();
			setTimeout(function() {
				$('#modelSuccess').modal('hide');
			}, 3000);

		} else {

			$("#errorMsg").text("Allocation Update Failed  !");
			$('#modelError').modal();
			setTimeout(function() {
				$('#modelError').modal('hide');
			}, 3000);
		}
	});
}

function updateBooking(projectName, crNo, empId, allocationDate, allocation,
		createdUser) {
	ajaxCall({
		projectName : projectName,
		crNo : crNo,
		empId : empId,
		allocationDate : allocationDate,
		allocation : allocation,
		createdUser : createdUser,
	}, "/Reslo/updateBooking", function(data) {
		if (data == 0) {
			$("#successMsg").text("Booking Successfully Updated !");
			$('#modelSuccess').modal();
			setTimeout(function() {
				$('#modelSuccess').modal('hide');
			}, 3000);

		} else {

			$("#errorMsg").text("Booking Update Failed  !");
			$('#modelError').modal();
			setTimeout(function() {
				$('#modelError').modal('hide');
			}, 3000);
		}
	});
}

function deleteAllocation(projectName, crNo, empId, allocationDate, allocation,
		createdUser) {
	ajaxCall({
		projectName : projectName,
		crNo : crNo,
		empId : empId,
		allocationDate : allocationDate,
		allocation : allocation,
		createdUser : createdUser,
	}, "/Reslo/deleteAllocation", function(data) {
		if (data == 0) {
			$("#successMsg").text("Allocation Successfully Deleted !");
			$('#modelSuccess').modal();
			setTimeout(function() {
				$('#modelSuccess').modal('hide');
			}, 3000);

		} else {
			$("#errorMsg").text("Allocation Delete Failed !");
			$('#modelError').modal();
			setTimeout(function() {
				$('#modelError').modal('hide');
			}, 3000);
		}
	});
}

function loadHolidays() {
	var disableDateList = new Array();
	var disableDate = null;
	
	ajaxGetCall({}, "/Reslo/getAllHolidays", function(data) {
		list = new Array();
		$.each(data, function(index, value) {
			addHoliday = new Date(value.date);
			list.push(addHoliday);
		});
		holidayArray = list;
		

		disableDateList = holidayArray;
		
		
		

		
		
	});
}

function loadProjectData(empId) {
	ajaxGetCall(
			{},
			"/Reslo/getAllocationByEmpId?id=" + empId,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#allocationList').dataTable();
				oTable.fnClearTable();

				custodianBankSearchBean = data;

				$
						.each(
								custodianBankSearchBean,
								function(i) {
									var projectName = crNo = allocationStatus = allocationStartDate = allocationEndDate  = allocation = 'N/A';
									count++;

									if (custodianBankSearchBean[i]['project']['projectName'] != null) {
										projectName = custodianBankSearchBean[i]['project']['projectName'];
									}

									if (custodianBankSearchBean[i]['project']['crNo'] != null) {
										crNo = custodianBankSearchBean[i]['project']['crNo'];
									}

									if (custodianBankSearchBean[i]['allocationStatus'] != 'N/A') {
										allocationStatus = custodianBankSearchBean[i]['allocationStatus'];
									}

									if (custodianBankSearchBean[i]['allocationStartDate'] != 'N/A') {
										Start = formatDate(custodianBankSearchBean[i]['allocationStartDate']);
										allocationStartDate = Start;
									}

									if (custodianBankSearchBean[i]['allocationEndDate'] != 'N/A') {
										End = formatDate(custodianBankSearchBean[i]['allocationEndDate']);
										allocationEndDate = End;
									}

									if (custodianBankSearchBean[i]['allocation'] != null) {
										allocation = custodianBankSearchBean[i]['allocation'];
									}

									oTable.fnAddData([ projectName, crNo, allocationStatus, allocationStartDate, allocationEndDate, allocation ]);

								});

				/*
				 * } else{ $("#errorMsg").text("Sorry. Requested data not
				 * available."); $('#modelError').modal(); setTimeout(function() {
				 * $('#modelError').modal('hide'); }, 3000); }
				 */
			});
}

function editAllocationData(projectName, crNo) {

	var data = custodianBankSearchBean;
	var dateArray = null;

	for (var i = 0; i < data.length; i++) {
		if (data[i]['project']['projectName'] == projectName
				&& data[i]['project']['crNo'] == crNo) {

			var StartDate = formatDate2(data[i]['allocationStartDate']);
			var formatStartDate = new Date(StartDate);
			var EndDate = formatDate2(data[i]['allocationEndDate']);
			var formatEndDate = new Date(EndDate);
			var allocation = data[i]['allocation'];

			dateArray = getDates(formatStartDate, formatEndDate);
			dateArray = filterWeekends(dateArray);
			dateArray = filterHolidays(dateArray);
				
			var allocationStartDate = formatDate3(formatStartDate);
			var allocationEndDate = formatDate3(formatEndDate);

			$("#allocationStartDate").siblings('.label-material').addClass(
					'active');
			$("#allocationStartDate").val(allocationStartDate);
			$("#allocationEndDate").siblings('.label-material').addClass(
					'active');
			$("#allocationEndDate").val(allocationEndDate);
			$("#allocation").siblings('.label-material').addClass('active');
			$("#allocation").val(allocation);
			$("#noOfDays").siblings('.label-material').addClass('active');
			$("#noOfDays").val(dateArray.length);
			$("#addAllocation").prop("disabled", true);
			$("#updateAllocation").show();
			$("#deleteAllocation").show();

		}
	}

	/*
	 * } else{ $("#errorMsg").text("Sorry. Requested data not available.");
	 * $('#modelError').modal(); setTimeout(function() {
	 * $('#modelError').modal('hide'); }, 3000); }
	 */

}

function ajaxCall(data, url, callback) {

	$.ajax({
		type : "POST",
		url : url,
		dataType : 'json',
		data : JSON.stringify(data),
		contentType : 'application/json; charset=utf-8'
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		$body.removeClass('loading');
	});
}

function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		/* $body.removeClass("loading"); */
	});
}