$( document ).ready(function() {

	$("#username").siblings('.label-material').addClass('active');
	$("#password").siblings('.label-material').addClass('active');
	
	var message = null;
	var error = null;
	
	error=$("#error").val(); 
	message=$("#message").val();
	 
	
	if (message != null) {
		
		$("#successMsg").text(message);
   	    $('#modelSuccess').modal();
   	    setTimeout(function() { $('#modelSuccess').modal('hide'); }, 3000);	
   	    $("#username").val("");
		$("#username").siblings('.label-material').removeClass('active');
		$("#password").val("");
		$("#password").siblings('.label-material').removeClass('active');
	}
	
	if (error != null) {
		 $("#errorMsg").text(error);
         $('#modelError').modal();
         setTimeout(function() { $('#modelError').modal('hide'); }, 3000);
        $("#username").val("");
 		$("#username").siblings('.label-material').removeClass('active');
 		$("#password").val("");
 		$("#password").siblings('.label-material').removeClass('active');
	}
});

