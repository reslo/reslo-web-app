var dataSet = [ [ "", "", "", "", ""] ];

var startDate = null;

$( document ).ready(function() {
	
	getAllFutureProjects();
	
	$("#allocationTabel").hide();
	$("#printReport").hide();
	$("#chartReport").hide();
	
	
	$('#projectName').change(function(e) {
		projectName =  $("#projectName").val();
		
		$('#crNo').val("");
		$("#allocationTabel").hide();
		$(".dataTables_wrapper").hide();
		$("#printReport").hide();
		$("#chartReport").hide();
		$("#userRoleChange").val("All Employee");
		
    });
	
	$('#previewReport').click(
			function(e) {
			
		if ($("#crNo").val() != "" && $("#crNo").val() != null)
         {
			
		  crNo =  $("#crNo").val();
		  userRole =  $("#userRoleChange").val();
		  
		  getBookingEmployees(projectName,crNo,userRole);	
		  $(".dataTables_wrapper").show();
		  $("#allocationTabel").show();
		  $("#allocationList").show();
		  
		  $("#printReport").show();
		  $("#chartReport").show();
		  
		 
        }
				
				
				return false;
	});
	

	$('#chartReport').click(
			function(e) {
				
				$('#gantt-chart').remove();
				$('#gantt').append('<div id="gantt-chart"></div>');
				$("#gantt-div").show();
				loadProjects(projectName,crNo,userRole);
				
				return false;
	});
	$('#cancelReport').click(
			function(e) {
			$(".dataTables_wrapper").hide();	
			$("#crNo").val("");	
			$("#userRoleChange").val("All Employee");
			$("#allocationTabel").hide();
			$("#printReport").hide();
			$("#chartReport").hide();
			$('#gantt-chart').remove();
			return false;
	});
	
	
	$('#crNo').typeahead({
		source : function(query, process) {
			filterName = 'crNo';
			return $.get('/Reslo/getProjectsByCrNo', {
				name : query,
				projectName : projectName
			}, function(data) {
				//data = $.parseJSON(data);
				return process(data);
			});
		}
	});

  
	
});

function formatDates(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

function loadGanttChart(data) {
	  //anychart.onDocumentReady(function() {
	
	  var date = new Date();
	  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	
	
	  start = formatDates(firstDay).split('/');
	  end = formatDates(lastDay).split('/');
	
	  // The data used in this sample can be obtained from the CDN
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  //anychart.data.loadJsonFile('test.json', function(data) {
		
	    // create data tree
	    var treeData = anychart.data.tree(data, 'as-table');
	    
	    anychart.format.outputTimezone(-330);

	    // create project gantt chart
	    chart = anychart.ganttProject();
	    
	    

	    // set data for the chart
	    chart.data(treeData);

	    // set start splitter position settings
	    chart.splitterPosition(515);

	    // get chart data grid link to set column settings
	    var dataGrid = chart.dataGrid();
	    
	    // set first column settings
	    dataGrid.column(0).cellTextSettings({
	      hAlign: 'center'
	    })
	    .width(30);

	    // set second column settings
	    dataGrid.column(1)
	      .width(140)
	      .title('Employee ID')
	      .cellTextSettingsOverrider(labelTextSettingsFormatter);

	    dataGrid.column(2)
	      
	      .width(140)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .title('Employee Name')
	      .format(crNoColumnTextFormatter);
	    
	    // set third column settings
	    dataGrid.column(3)
	      .title('Plan Start')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(thirdColumnTextFormatter);

	    // set fourth column settings
	    dataGrid.column(4)
	      .title('Actual End')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(fourthColumnTextFormatter);
	    

	    // set container id for the chart
	    chart.container('gantt-chart');

	    // initiate chart drawing
	    chart.draw();
	    
	   // chart.fitAll();
	    var start1 = parseInt(start[0]);
	    var start2 = parseInt(start[1]);
	    var start3 = parseInt(start[2]);
	    var end1 = parseInt(end[0]);
	    var end2 = parseInt(end[1]);
	    var end3 = parseInt(end[2]);
	    // zoom chart to specified date
	  chart.zoomTo(Date.UTC(start1, start2, start3), Date.UTC(end1, end2, end3));
	 // });
	//});
	  
} 

//add bold and italic text settings to all parent items
function labelTextSettingsFormatter(label, dataItem) {
  if (dataItem.numChildren()) {
    label.fontWeight('bold').fontStyle('italic');
  }
}

//do pretty formatting for dates in third column
function thirdColumnTextFormatter(item) {
  
  var field1 = item.get('actualStart');
  
  if(field1!= null)
	  {
	
	    var actualStart = new Date(field1);
	    
	    return formatDate(actualStart);
	  } else {

	    return '';
	  }
	  
 }
  
	  
  


function fourthColumnTextFormatter(item) {
	  
	  var field1 = item.get('actualEnd');
	  
	  
	  if (field1 != null) {
	    var actualEnd = new Date(field1);
	    
	    return formatDate(actualEnd);
	  } else {

	    return '';
	  }
	  
  }
	  
	 
	

//do pretty formatting for dates in fourth column
function nameColumnTextFormatter(item) {
  
 var field = item.get('id');
  
  pName.push(item.get('id'));
 
  return field;
}


function unique(pName) {
    
    $.each(pName, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

//do pretty formatting for dates in fourth column
function crNoColumnTextFormatter(item) {
  var field = item.get('crNo');
  return field;
}


//do pretty formatting for passed date unit
function formatDate(dateUnit) {
  if (dateUnit < 10) dateUnit = '0' + dateUnit;
  return dateUnit + '';
}

function formatDate(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}


function getBookingEmployees(projectName,crNo,userRole) {
	ajaxGetCall(
			{},"/Reslo/getBookingProjects?projectName="+projectName+"&crNo="+crNo+"&userRole="+userRole,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#allocationList').dataTable();
				oTable.fnClearTable();

				custodianBankSearchBean = data;

				$.each(custodianBankSearchBean,
								function(i) {
									var  empId = empName = BookingStartDate = BookingEndDate = Booking = 'N/A';
									count++;


									if (custodianBankSearchBean[i]['id'] != null) {
										empId = custodianBankSearchBean[i]['id'];
									}

									if (custodianBankSearchBean[i]['crNo'] != null) {
										empName = custodianBankSearchBean[i]['crNo'];
									}

								
									if (custodianBankSearchBean[i]['actualStart'] != 'N/A') {
										Start = formatDate(custodianBankSearchBean[i]['actualStart']);
										BookingStartDate = Start;
									}

									if (custodianBankSearchBean[i]['actualEnd'] != 'N/A') {
										End = formatDate(custodianBankSearchBean[i]['actualEnd']);
										BookingEndDate = End;
									}

									if (custodianBankSearchBean[i]['allocation'] != null) {
										Booking = custodianBankSearchBean[i]['allocation'];
									}
								
									oTable.fnAddData([ empId, empName, BookingStartDate, BookingEndDate, Booking]);

								});

			
			});
}

function call_event_handler(value) {
	
	   project = value;
	  
	   //console.log(value);
	   
	   //clear the place holder values
	   $("#crNo").siblings('.label-material').addClass('active');
	   
	   
	   if($("#crNo").val()!=null && $("#crNo").val()!='')
	   {
		   $("#crNo").val(project.crNo); 
	   }
	   
}



function  loadProjects(projectName,crNo,userRole) {           
	ajaxGetCall({}, "/Reslo/getBookingProjects?projectName="+projectName+"&crNo="+crNo+"&userRole="+userRole, function(data){
			loadGanttChart(data);
			
						
    	});
}

function downloadGetBookingProjectSummary(projectName,crNo,userRole) {

	window.location ="/Reslo/downloadGetBookingProjectSummary?projectName="+projectName+"&crNo="+crNo+"&userRole="+userRole;

}

function getAllFutureProjects() {
	ajaxGetCall(
			{},
			"/Reslo/getAllFutureProjectsByName",
			function(data) {
				
			//set data to the drop down
			 for (i = 0; i < data.length; i++) {
					  
			 var name = '<option>' + data[i]['project']['projectName'] + '</option>'
			 $("#projectName").append(name);
			}
			projectName =  $("#projectName").val();

			});	

				
}

function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		/* $body.removeClass("loading"); */
	});
}


