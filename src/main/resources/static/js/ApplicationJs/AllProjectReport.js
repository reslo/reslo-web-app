var dataSet = [ [ "", "", "", "", "", "", ""] ];

var startDate = null;

$( document ).ready(function() {
	
	$("#projectTabel").hide();
	$("#printReport").hide();
	$("#chartReport").hide();
	
	
	
	$('#previewReport').click(
			function(e) {
			
			startDate = formatDates(new Date($("#startDate").val()));
			
			
			if (startDate != "")
             {

				getAllProjects(startDate);
			  $(".dataTables_wrapper").show();
			  $("#projectTabel").show();
			  $("#printReport").show();
			  $("#chartReport").show();
		     }
				
				
				return false;
	});
	
	$('#cancelReport').click(
			function(e) {
			$(".dataTables_wrapper").hide();	
			$("#projectTabel").hide();
			$("#printReport").hide();
			$("#chartReport").hide();
			$("#gantt-div").hide();	
				
				return false;
	});
	
	$('#chartReport').click(
		function(e) {
			
			$('#gantt-chart').remove();
			$('#gantt').append('<div id="gantt-chart"></div>');
			$("#gantt-div").show();
		loadProjects(startDate);
				
		return false;
	});
	
	$('#printReport').click(
			function(e) {
				
			downloadAllProjects(startDate)

		 	return false;
	});
	
	

  
	
});

function formatDates(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

function loadGanttChart(data) {
	  //anychart.onDocumentReady(function() {
	
	  var date = new Date();
	  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	
	
	  start = formatDates(firstDay).split('/');
	  end = formatDates(lastDay).split('/');
	
	  // The data used in this sample can be obtained from the CDN
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  // https://cdn.anychart.com/samples-data/gantt-charts/planned-vs-actual-chart/data.json
	  //anychart.data.loadJsonFile('test.json', function(data) {
		
	    // create data tree
	    var treeData = anychart.data.tree(data, 'as-table');
	    
	    anychart.format.outputTimezone(-330);

	    // create project gantt chart
	    chart = anychart.ganttProject();
	    
	    

	    // set data for the chart
	    chart.data(treeData);

	    // set start splitter position settings
	    chart.splitterPosition(515);

	    // get chart data grid link to set column settings
	    var dataGrid = chart.dataGrid();
	    
	    // set first column settings
	    dataGrid.column(0).cellTextSettings({
	      hAlign: 'center'
	    })
	    .width(30);

	    // set second column settings
	    dataGrid.column(1)
	      .width(150)
	      .title('Project Name & CR Name')
	      .cellTextSettingsOverrider(labelTextSettingsFormatter);

	    dataGrid.column(2)
	      
	      .width(130)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .title('CR No')
	      .format(crNoColumnTextFormatter);
	    
	    // set third column settings
	    dataGrid.column(3)
	      .title('Plan Start')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(thirdColumnTextFormatter);

	    // set fourth column settings
	    dataGrid.column(4)
	      .title('Actual End')
	      .width(100)
	      .cellTextSettingsOverrider(labelTextSettingsFormatter)
	      .format(fourthColumnTextFormatter);
	    

	    // set container id for the chart
	    chart.container('gantt-chart');

	    // initiate chart drawing
	    chart.draw();
	    
	   // chart.fitAll();
	    var start1 = parseInt(start[0]);
	    var start2 = parseInt(start[1]);
	    var start3 = parseInt(start[2]);
	    var end1 = parseInt(end[0]);
	    var end2 = parseInt(end[1]);
	    var end3 = parseInt(end[2]);
	    // zoom chart to specified date
	  chart.zoomTo(Date.UTC(start1, start2, start3), Date.UTC(end1, end2, end3));
	 // });
	//});
	  
} 

//add bold and italic text settings to all parent items
function labelTextSettingsFormatter(label, dataItem) {
  if (dataItem.numChildren()) {
    label.fontWeight('bold').fontStyle('italic');
  }
}

//do pretty formatting for dates in third column
function thirdColumnTextFormatter(item) {
  var field = item.get('actualStart');
  var field1 = item.get('baselineStart');
  var field2 = item.get('actualStartTrue');
  var field3 = item.get('baselineStartTrue');
  

  if(field==field1)
	  {
	// format base line text
	  if (field2) {
	    var baselineStart = new Date(field2);
	    
	    return formatDate(baselineStart.getMonth() + 1) + '/' +
	      formatDate(baselineStart.getDate()) + '/' + baselineStart.getFullYear();
	  } else {

	    return '';
	  }
	  
	  }
  else
	  {
	// format base line text
	  if (field3) {
	    var baselineStart = new Date(field3);
	    
	    return formatDate(baselineStart.getMonth() + 1) + '/' +
	      formatDate(baselineStart.getDate()) + '/' + baselineStart.getFullYear();
	  } else {

	    return '';
	  }
	  }
  
}

function fourthColumnTextFormatter(item) {
	  var field0 = item.get('baselineStart');  
	  var field = item.get('actualEnd');
	  var field1 = item.get('baselineEnd');
	  var field2 = item.get('actualEndTrue');
	  var field3 = item.get('baselineEndTrue');
	  
	  if(field1==field0 || field==field1)
	  {
	// format base line text
	  if (field2) {
	    var baselineStart = new Date(field2);
	    
	    return formatDate(baselineStart.getMonth() + 1) + '/' +
	      formatDate(baselineStart.getDate()) + '/' + baselineStart.getFullYear();
	  } else {

	    return '';
	  }
	  
	  }
  else
	  {
	// format base line text
	  if (field3) {
	    var baselineStart = new Date(field3);
	    
	    return formatDate(baselineStart.getMonth() + 1) + '/' +
	      formatDate(baselineStart.getDate()) + '/' + baselineStart.getFullYear();
	  } else {

	    return '';
	  }
	  }
	  
	 
	}

//do pretty formatting for dates in fourth column
function nameColumnTextFormatter(item) {
  
 var field = item.get('id');
  
  pName.push(item.get('id'));
 
  return field;
}


function unique(pName) {
    
    $.each(pName, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

//do pretty formatting for dates in fourth column
function crNoColumnTextFormatter(item) {
  var field = item.get('crNo');
  return field;
}


//do pretty formatting for passed date unit
function formatDate(dateUnit) {
  if (dateUnit < 10) dateUnit = '0' + dateUnit;
  return dateUnit + '';
}



function getAllProjects(startDate) {
	ajaxGetCall(
			{startDate : startDate,},
			"/Reslo/getAllProjectsGanttChartData",
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#projectList').dataTable();
				oTable.fnClearTable();

				custodianBankSearchBean = data.chartList;

				$.each(custodianBankSearchBean,
								function(i) {
									var  projectName = crNo = projectStatus = startDate = endDate = actualStartDate = actualEndDate = projectProgress = 'N/A';
									count++;


									if (custodianBankSearchBean[i]['name'] != null) {
										projectName = custodianBankSearchBean[i]['name'];
									}
									
									if (custodianBankSearchBean[i]['crNo'] != null) {
										crNo = custodianBankSearchBean[i]['crNo'];
									}
									
									if (custodianBankSearchBean[i]['status'] != null) {
										status = custodianBankSearchBean[i]['status'];
									}
									
									if (custodianBankSearchBean[i]['actualStartTrue'] != null) {
										startDate = formatDates(custodianBankSearchBean[i]['actualStartTrue']);
									}
									
									if (custodianBankSearchBean[i]['actualEndTrue'] != null) {
										endDate = formatDates(custodianBankSearchBean[i]['actualEndTrue']);
									}
									
									if (custodianBankSearchBean[i]['baselineStartTrue'] != null) {
										actualStartDate = formatDates(custodianBankSearchBean[i]['baselineStartTrue']);
									}
									
									if (custodianBankSearchBean[i]['baselineEndTrue'] != null) {
										actualEndDate = formatDates(custodianBankSearchBean[i]['baselineEndTrue']);
									}
									
									if (custodianBankSearchBean[i]['progressValue'] != "null%") {
										projectProgress = custodianBankSearchBean[i]['progressValue'];
									}

								
									oTable.fnAddData([ projectName, crNo, status, startDate, endDate, actualStartDate, actualEndDate, projectProgress ]);

								});

			
			});
}

function downloadAllProjects(startDate) {
	
	window.location = "/Reslo/downloadAllProjectsData?startDate="+startDate;
			
}

function  loadProjects(startDate) {           
	ajaxGetCall({
		startDate : startDate,
		
	}, "/Reslo/getAllProjectsGanttChartData", function(data){
			loadGanttChart(data.chartList);
			
						
    	});
}


function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		/* $body.removeClass("loading"); */
	});
}


