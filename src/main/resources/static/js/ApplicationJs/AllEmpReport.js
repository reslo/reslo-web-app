var dataSet = [ [ "", "", "", "", "", "", "", "", ""] ];
var userRole = null;
var startDate = null;

$( document ).ready(function() {
	
	$("#employeeList").hide();
	$("#downloadReport").hide();
	
	
	$('#previewReport').click(
			function(e) {
			
			startDate = formatDate(new Date($("#startDate").val()));
			userRole = $("#userRoleChange").val();
			
			if (startDate != "" && userRole != "")
             {

			  loadAllEmployeeData(startDate,userRole);
			  $(".dataTables_wrapper").show();
			  $("#employeeList").show();
			  $("#downloadReport").show();
		     }
				
				
				return false;
	});
	
	$('#cancelReport').click(
			function(e) {
			$(".dataTables_wrapper").hide();	
			$("#employeeList").hide();
			$("#downloadReport").hide();
			$("#startDate").val("");	
			$("#userRoleChange").val("All Employee");	
				return false;
	});
	
	$('#downloadReport').click(
			function(e) {
				
			
				downloadAllEmployeeData(startDate,userRole);
				
				return false;
	});

  
	
});

function formatDate(date) {
	var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
			+ d.getDate(), year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [ year, month, day ].join('/');
}

function loadAllEmployeeData(startDate,userRole) {
	ajaxGetCall(
			{},
			"/Reslo/getAllEmployeeData?startDate="+startDate+"&userRole="+userRole,
			function(data) {

				// if(data.errorCode == 0){
				var count = 0;
				var appName = "N/A";
				var name = "N/A";
				var accBalance = null;

				// get the DataTable
				var oTable = $('#employeeList').dataTable();
				oTable.fnClearTable();

				custodianBankSearchBean = data;

				$.each(custodianBankSearchBean,
								function(i) {
									var employeeName = projectName = crNo = projectStatus = startDate = endDate = actualStartDate = actualEndDate = projectProgress = allocation = 'N/A';
									count++;

									if (custodianBankSearchBean[i]['employee']['empName'] != null) {
										employeeName = custodianBankSearchBean[i]['employee']['empName'];
									}

									if (custodianBankSearchBean[i]['project']['projectName'] != null) {
										projectName = custodianBankSearchBean[i]['project']['projectName'];
									}
									
									if (custodianBankSearchBean[i]['project']['crNo'] != null) {
										crNo = custodianBankSearchBean[i]['project']['crNo'];
									}
									
									if (custodianBankSearchBean[i]['project']['projectStatus'] != null) {
										status = custodianBankSearchBean[i]['project']['projectStatus'];
									}
									
									if (custodianBankSearchBean[i]['project']['startDate'] != null) {
										startDate = formatDate(custodianBankSearchBean[i]['project']['startDate']);
									}
									
									if (custodianBankSearchBean[i]['project']['endDate'] != null) {
										endDate = formatDate(custodianBankSearchBean[i]['project']['endDate']);
									}
									
									if (custodianBankSearchBean[i]['project']['actualStartDate'] != null) {
										actualStartDate = formatDate(custodianBankSearchBean[i]['project']['actualStartDate']);
									}
									
									if (custodianBankSearchBean[i]['project']['actualEndDate'] != null) {
										actualEndDate = formatDate(custodianBankSearchBean[i]['project']['actualEndDate']);
									}
									
									if (custodianBankSearchBean[i]['project']['projectProgress'] != null) {
										projectProgress = custodianBankSearchBean[i]['project']['projectProgress'];
									}

									if (custodianBankSearchBean[i]['allocation'] != 'N/A') {
										allocation = custodianBankSearchBean[i]['allocation'];
									}

									
									oTable.fnAddData([ employeeName, projectName, crNo, status, startDate, endDate, actualStartDate, actualEndDate, projectProgress, allocation ]);

								});

			
			});
}


function downloadAllEmployeeData(startDate,userRole) {

 window.location = "/Reslo/downloadAllEmployeeData?startDate="+startDate+"&userRole="+userRole;

}

function ajaxGetCall(data, url, callback) {

	$.ajax({
		type : "GET",
		url : url,
		data : data
	}).done(function(msg) {
		callback(msg);
	}).fail(function(err) {
		console.log("error : " + err);
		/* $body.removeClass("loading"); */
	});
}


