package lk.dialog.ist.reslo.error;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Service;

import lk.dialog.ist.reslo.services.Config.UserStatus;


@Service("authenticationFailureHandler")
public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	
	private static final Log LOG = LogFactory.getLog(AuthenticationFailureHandler.class);

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		
		super.onAuthenticationFailure(request, response, exception);
		String msg = exception.getLocalizedMessage();
		String asd = UserStatus.BAD_CREDENTIALS.getMsg();
		if(msg.equals(asd))
		{
			msg = UserStatus.INVALID_CREDENTIALS.getMsg();
		}
		getRedirectStrategy().sendRedirect(request, response, "/?error="+msg);		
		
	}
	
	
}
