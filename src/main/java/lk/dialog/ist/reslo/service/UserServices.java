package lk.dialog.ist.reslo.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import lk.dialog.ist.reslo.response.UserDetailResponce;

@Service
public class UserServices {

@Value(value = "${reslo.admin.rest.sabanda.service.url}")
private String restServiceUrl;

public JSONArray sendPost(String name) throws ParseException  {
	JSONParser parser = new JSONParser(); 
	JSONArray json=null;
	try{	
		URL obj = new URL(restServiceUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();	
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.addRequestProperty("Authorization", "Bearer MTUwODM5OTkxNTkyNg==");
		con.setConnectTimeout(5);                       
		String urlParameters = "filterToken="+name;
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		json = (JSONArray) parser.parse(response.toString());	
	}catch(Exception e){
		e.printStackTrace();
	}
	return json;

}

	
}
