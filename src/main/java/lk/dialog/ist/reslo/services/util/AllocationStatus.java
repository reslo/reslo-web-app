package lk.dialog.ist.reslo.services.util;

public enum AllocationStatus {
	BOOKED,
	ALLOCATED,
	DELETED,
}
