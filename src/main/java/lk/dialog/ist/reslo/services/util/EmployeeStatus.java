package lk.dialog.ist.reslo.services.util;

public enum EmployeeStatus {
	DELETED,
	ACTIVE,
}
