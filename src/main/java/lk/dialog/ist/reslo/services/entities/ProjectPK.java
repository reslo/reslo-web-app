/**
 * 
 */
package lk.dialog.ist.reslo.services.entities;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author amel_33251
 *
 */
public class ProjectPK implements Serializable{

	private static final long serialVersionUID = -65257091879282067L;
	
	protected String projectName;
	protected String crNo;
	
	public ProjectPK() {
		
	}
	
	public ProjectPK(String merchantId,String gameId) {
		this.projectName = merchantId;
		this.crNo = gameId;
	}

	
	
	
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @return the crNo
	 */
	public String getCrNo() {
		return crNo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(getProjectName(), getCrNo());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectPK other = (ProjectPK) obj;
		if (projectName != other.projectName)
			return false;
		if (crNo != other.crNo)
			return false;
		return true;
	}

	
}
