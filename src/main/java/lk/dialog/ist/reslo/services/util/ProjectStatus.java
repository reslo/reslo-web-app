package lk.dialog.ist.reslo.services.util;

public enum ProjectStatus {
	CLOSED,
	INPROGRESS,
	ONHOLD,
	TERMINATED,
}
